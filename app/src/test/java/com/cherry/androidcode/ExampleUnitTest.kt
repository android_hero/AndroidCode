package com.cherry.androidcode

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import com.cherry.androidcode.ApiUrl.SEARCH_URL
import com.cherry.androidcode.entity.CityBean
import com.cherry.androidcode.entity.HttpResult
import com.cherry.androidcode.entity.SearchBean
import com.cherry.androidcode.main1.MainActivity
import com.cherry.androidcode.okgo.BaseObserver
import com.cherry.androidcode.okgo.JsonConvert
import com.lzy.okgo.OkGo
import com.lzy.okrx2.adapter.ObservableBody
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(value = AndroidJUnit4::class)
@LargeTest
class ExampleUnitTest {

    @Rule
    val activityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun testSearchBean() {
        OkGo.post<HttpResult<SearchBean>>(SEARCH_URL)
                .params("k", "gradle")
                .converter(object : JsonConvert<HttpResult<SearchBean>>() {})
                .adapt(ObservableBody<HttpResult<SearchBean>>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : BaseObserver<SearchBean>() {
                    override fun onSuccess(data: SearchBean) {
                        println(data.toString())
                    }
                })
    }

    @Test
    fun testListBean() {
        OkGo.get<HttpResult<Map<String, CityBean>>>(SEARCH_URL)
                .converter(object : JsonConvert<HttpResult<Map<String, CityBean>>>() {})
                .adapt(ObservableBody<HttpResult<Map<String, CityBean>>>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : BaseObserver<Map<String, CityBean>>() {
                    override fun onSuccess(data: Map<String, CityBean>) {
                        data.forEach {
                            it.value.toString()
                        }.also(::println)
                    }
                })
    }
}
