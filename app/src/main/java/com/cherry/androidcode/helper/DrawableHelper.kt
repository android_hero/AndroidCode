package com.cherry.androidcode.helper

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.view.View
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat
import com.cherry.androidcode.MainApp
import com.cherry.androidcode.R
import com.cherry.androidcode.util.dpToPx
import com.cherry.androidcode.util.resToColor
import com.cherry.androidcode.widget.divider.DividerDrawable
import com.cherry.androidcode.widget.divider.DividerLayout
import com.cherry.androidcode.widget.divider.DividerUtils
import top.defaults.drawabletoolbox.DrawableBuilder
import top.defaults.drawabletoolbox.StateListDrawableBuilder


/**
 * @author 董棉生(dongmiansheng@parkingwang.com)
 * @since 2019/6/7
 */

object DrawableHelper {

    fun tabDrawable(): Drawable {
        val normalState = DrawableBuilder()
                .baseDrawable(R.mipmap.ic_tab_home_normal.resToDrawable())
                .build()
        val selectedState = DrawableBuilder()
                .baseDrawable(R.mipmap.ic_tab_home_selected.resToDrawable())
                .build()
        return StateListDrawableBuilder().normal(normalState)
                .selected(selectedState)
                .build()
    }

    fun pointIndicatorDrawable(): Drawable {
        val normalState = DrawableBuilder()
                .solidColor(Color.LTGRAY)
                .cornerRadius(5f.dpToPx())
                .build()
        val selectedState = DrawableBuilder()
                .cornerRadius(5f.dpToPx())
                .solidColor(Color.RED)
                .build()
        return StateListDrawableBuilder().normal(normalState)
                .selected(selectedState)
                .build()
    }

    fun normalDivider(view: View) {
        baseDivider(view) {
            it.align = DividerLayout.ALIGN_PARENT_BOTTOM
        }
    }

    fun baseDivider(view: View, setDivider: (DividerLayout) -> Unit) {
        val dividerDrawable = DividerDrawable().setStrokeWidth(1).setColor(R.color.divider.resToColor()).apply {
            setDivider(this.layout)
        }
        DividerUtils.addDividersTo(view, dividerDrawable)
    }


    /**
     * Resources.getSystem() 可以在任何地方进行使用，但是有一个局限，只能获取系统本身的资源。
    系统资源存放地址可在 platforms/android-xx/data/res/ 目录下查看。
    使用方法类似于
    int res =  Resources.getSystem().getIdentifier("resName", "resType", "resPackage");
     */
    fun getResources() = MainApp.CONTEXT.resources


    fun Int.resToDrawable() = ResourcesCompat.getDrawable(getResources(), this, null)!!

    fun wrap(@DrawableRes icon: Int, @ColorInt tintColor: Int): Drawable? {
        var drawable = ResourcesCompat.getDrawable(getResources(), icon, MainApp.CONTEXT.theme)
//        val tint = ResourcesCompat.getColorStateList(getResources(), tintColorResource, MainApp.CONTEXT.theme)
        val tint = ColorStateList.valueOf(tintColor)
        if (drawable != null) {
            drawable = DrawableCompat.wrap(drawable.mutate())
            DrawableCompat.setTintList(drawable!!, tint)
        }
        return drawable
    }
}