package com.cherry.androidcode.helper

import android.content.Context.MODE_PRIVATE
import android.webkit.WebSettings
import android.webkit.WebView


/**
 * @author DongMS
 * @date 2019/6/20
 */
object WebViewHelper {

    fun setup(webView: WebView) {
        //声明WebSettings子类
        val webSettings = webView.settings

        //如果访问的页面中要与Javascript交互，则webview必须设置支持Javascript
        // 若加载的 html 里有JS 在执行动画等操作，会造成资源浪费（CPU、电量）
        // 在 onStop 和 onResume 里分别把 setJavaScriptEnabled() 给设置成 false 和 true 即可
        webSettings.javaScriptEnabled = true

        //设置自适应屏幕，两者合用
        webSettings.useWideViewPort = true //将图片调整到适合webview的大小
        webSettings.loadWithOverviewMode = true // 缩放至屏幕的大小

        //缩放操作
        webSettings.setSupportZoom(true) //支持缩放，默认为true。是下面那个的前提。
        webSettings.builtInZoomControls = true //设置内置的缩放控件。若为false，则该WebView不可缩放
        webSettings.displayZoomControls = false //隐藏原生的缩放控件

        //其他细节操作
        webSettings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK //关闭webview中缓存
        webSettings.allowFileAccess = true //设置可以访问文件
        webSettings.javaScriptCanOpenWindowsAutomatically = true //支持通过JS打开新窗口
        webSettings.loadsImagesAutomatically = true //支持自动加载图片
        webSettings.defaultTextEncodingName = "utf-8"//设置编码格式

        //设置定位的数据库路径
        val dir = webView.context.getDir("database", MODE_PRIVATE).path
        webSettings.setGeolocationDatabasePath(dir)
        //启用地理定位
        webSettings.setGeolocationEnabled(true)
        //开启DomStorage缓存
        webSettings.domStorageEnabled = true

    }


    fun clear(webView: WebView) {
        //只会webview访问历史记录里的所有记录除了当前访问记录
        webView.clearHistory()
        //由于内核缓存是全局的因此这个方法不仅仅针对webview而是针对整个应用程序.
        webView.clearCache(true)
        //这个api仅仅清除自动完成填充的表单数据，并不会清除WebView存储到本地的数据
        webView.clearFormData()
    }

}