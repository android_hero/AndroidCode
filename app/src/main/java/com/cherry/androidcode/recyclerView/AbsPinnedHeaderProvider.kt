package com.cherry.androidcode.recyclerView

import com.chad.library.adapter.base.provider.BaseItemProvider

/**
 * @author DongMS
 * @since 2019/9/4
 */
abstract class AbsPinnedHeaderProvider<T, VH : QuickViewHolder> : BaseItemProvider<T>() {

    override val itemViewType: Int = AbsPinnedHeaderListAdapter.TYPE_HEADER

}