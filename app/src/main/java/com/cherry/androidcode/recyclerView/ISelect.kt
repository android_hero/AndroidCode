package com.cherry.androidcode.recyclerView

/**
 * @author DongMS
 * @date 2019/6/26
 */
interface ISelect {
    var isSelected: Boolean
}