package com.cherry.androidcode.recyclerView

import android.content.Intent
import android.view.View
import androidx.core.os.bundleOf
import com.chad.library.adapter.base.BaseQuickAdapter
import com.cherry.androidcode.R
import com.cherry.androidcode.activity.AndroidQActivity
import com.cherry.androidcode.activity.HiltDemoActivity
import com.cherry.androidcode.activity.UploadActivity
import com.cherry.androidcode.demo.*
import com.cherry.androidcode.jetpack.JetpackActivity
import com.cherry.androidcode.jetpack.databinding.DataBindingActivity
import com.cherry.androidcode.jetpack.lifecycle.LifeCycleActivity
import com.cherry.androidcode.jetpack.viewmodel.ViewModelLiveDataActivity
import com.cherry.androidcode.mvp1.BaseView
import com.cherry.androidcode.third.ThirdActivity
import com.cherry.androidcode.util.resToColor
import com.yanyusong.y_divideritemdecoration.Y_Divider
import com.yanyusong.y_divideritemdecoration.Y_DividerBuilder
import com.yanyusong.y_divideritemdecoration.Y_DividerItemDecoration

/**
 * @author Cherry(dongmiansheng@gmail.com)
 * @since 2019/6/30
 */
class DemoFragment : ListViewFragment<DemoBean, BaseView, DemoPresenter>(), BaseView {

    override val enRefresh = false

    override val enLoadMore = false

    override val dividerDecoration by lazy {
        object : Y_DividerItemDecoration(activity) {
            override fun getDivider(itemPosition: Int): Y_Divider {
                return Y_DividerBuilder()
                        .setTopSideLine(itemPosition != 0, R.color.divider.resToColor(), 1f, 16f, 16f)
                        .create()
            }
        }
    }

    override val adapter: BaseQuickAdapter<DemoBean, QuickViewHolder> = object : BaseQuickAdapter<DemoBean, QuickViewHolder>(R.layout.item_test_simple) {
        override fun convert(helper: QuickViewHolder, item: DemoBean) {
            helper.setText(R.id.tv_title, item.titile)
        }
    }

    override fun initView(view: View) {
        super.initView(view)

        val listData = mvpPresenter?.loadData?.invoke()
        adapter.setNewInstance(listData!!)
        adapter.setOnItemClickListener { _, _, position ->
            adapter.getItem(position)?.clazz.let {
                startActivity(Intent(activity, it))
            }
        }
    }

    //注意，如果一个界面没有请求，不要调用这个方法，rootView此时是空的
    override fun reload() {
        //nothing
    }

    override fun loadNext() {
        //nothing
    }

    override fun createView() = this

    override fun createPresenter(): DemoPresenter {

        return DemoPresenter {
            when (arguments?.get("type")) {
                MAIN_FRAGMENT2_TAG ->
                    mutableListOf(DemoBean("CoordinatorLayout使用", CoordinatorLayoutDemoActivity::class.java),
                            DemoBean("Behavior使用", BehaviorDemoActivity::class.java),
                            DemoBean("RecyclerView", RecyclerViewDemoActivity::class.java),
                            DemoBean("ConstraintLayout和flexbox", ConstraintFlexboxDemoActivity::class.java),
                            DemoBean("Retrofit和Okhttp", RetrofitOkhttpDemoActivity::class.java),
                            DemoBean("Jetpack", JetpackActivity::class.java),
                            DemoBean("三方-支付、分享、登录", ThirdActivity::class.java),
                            DemoBean("AndroidQ适配", AndroidQActivity::class.java),
                            DemoBean("文件上传下载", UploadActivity::class.java)
                    )
                JETPACK_MAIN_TAG ->
                    mutableListOf(
                            DemoBean("LifeCycle使用", LifeCycleActivity::class.java),
                            DemoBean("ViewModel和LiveData使用", ViewModelLiveDataActivity::class.java),
                            DemoBean("DataBinding使用", DataBindingActivity::class.java),
                            DemoBean("Hilt使用", HiltDemoActivity::class.java)
                    )
                else -> mutableListOf()
            }
        }

    }

    companion object {
        const val MAIN_FRAGMENT2_TAG = "main_fragment2_tag"
        const val JETPACK_MAIN_TAG = "jetpack_main_tag"

        fun newInstance(type: String) = DemoFragment().apply {
            arguments = bundleOf(Pair("type", type))
        }

    }

}