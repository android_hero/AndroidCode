package com.cherry.androidcode.recyclerView

import android.view.View
import android.view.ViewGroup
import com.chad.library.adapter.base.loadmore.BaseLoadMoreView
import com.chad.library.adapter.base.util.getItemView
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.cherry.androidcode.R

/**
 * 自定义的加载更多视图
 */
class CustomLoadMoreView : BaseLoadMoreView() {

    // 布局中 “当前一页加载完成”的View
    override fun getLoadComplete(holder: BaseViewHolder): View = holder.getView(R.id.load_complete)

    // 布局中 “全部加载结束，没有数据”的View
    override fun getLoadEndView(holder: BaseViewHolder): View = holder.getView(R.id.load_end)

    override fun getLoadFailView(holder: BaseViewHolder): View = holder.getView(R.id.load_more_fail)

    override fun getLoadingView(holder: BaseViewHolder): View = holder.getView(R.id.loading)

    override fun getRootView(parent: ViewGroup): View = parent.getItemView(R.layout.layout_list_footer_load_more)
}
