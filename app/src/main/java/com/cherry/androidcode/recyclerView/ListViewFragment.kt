package com.cherry.androidcode.recyclerView

import android.view.View
import androidx.annotation.CallSuper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.module.BaseLoadMoreModule
import com.cherry.androidcode.AppConstant
import com.cherry.androidcode.R
import com.cherry.androidcode.mvp1.BasePresenter
import com.cherry.androidcode.mvp1.BaseView
import com.cherry.androidcode.mvp1.MVPFragment
import com.cherry.androidcode.util.findViewById
import com.cherry.androidcode.widget.MultipleStatusView

/**
 * @author DongMS
 * @date 2019/6/24
 */
abstract class ListViewFragment<T, V : BaseView, P : BasePresenter<V>> : MVPFragment<V, P>() {

    //默认实现底部加载更多，可以重写
    protected open val enLoadMore = true

    //默认实现顶部刷新，可以重写
    protected open val enRefresh = true

    protected var page = AppConstant.PAGE_START_INDEX

    protected open val layoutManager: RecyclerView.LayoutManager by lazy {
        LinearLayoutManager(activity)
    }

    protected abstract val dividerDecoration: RecyclerView.ItemDecoration?

    protected abstract val adapter: BaseQuickAdapter<T, QuickViewHolder>

    protected lateinit var recyclerView: RecyclerView

    protected lateinit var swipeRefreshLayout: SwipeRefreshLayout

    protected lateinit var multipleStatusView: MultipleStatusView

    protected var loadMoreModule: BaseLoadMoreModule? = null

    override val contentLayout = R.layout.layout_list_view_refresh_load

    @CallSuper
    override fun initView(view: View) {
        recyclerView = findViewById(R.id.list)
        swipeRefreshLayout = findViewById(R.id.refresh)
        multipleStatusView = findViewById(R.id.multiple_status_view)
        initRecyclerView()
        recyclerView.layoutManager = layoutManager
        if (dividerDecoration != null) {
            recyclerView.addItemDecoration(dividerDecoration!!)
        }
        adapter.isUseEmpty = false
        recyclerView.adapter = adapter

        multipleStatusView.setOnRetryClickListener {
            resetRefresh()
        }

        if (enLoadMore) {
            loadMoreModule = adapter.loadMoreModule
            loadMoreModule?.loadMoreView = CustomLoadMoreView()
            loadMoreModule?.loadMoreEnd(false)
            loadMoreModule?.setOnLoadMoreListener {
                page++
                loadNext()
            }
        }
        if (enRefresh) {
            swipeRefreshLayout.isRefreshing = true
            swipeRefreshLayout.setOnRefreshListener {
                innerRefresh()
            }
        } else {
            swipeRefreshLayout.isEnabled = false
        }
        //这里的作用是防止下拉刷新的时候还可以上拉加载
        loadMoreModule?.isEnableLoadMore = false
    }

    fun resetRefresh() {
        if (enRefresh) {
            swipeRefreshLayout.isRefreshing = true
        }
        innerRefresh()
    }

    private fun innerRefresh() {
        page = AppConstant.PAGE_START_INDEX
        //这里的作用是防止下拉刷新的时候还可以上拉加载
        loadMoreModule?.isEnableLoadMore = false
        reload()
    }

    protected open fun initRecyclerView() {
    }

    override fun onFragmentFirstVisible() {
        reload()
    }

    //如果有每次刷新的需求就使用这个方法
//    override fun onFragmentVisibleChange(isVisible: Boolean) {
//        if (isVisible){
//            reload()
//        }
//    }

    protected open fun updateList(list: MutableList<T>, reload: Boolean) {
        if (reload) {
            refresh(list)
        } else {
            loadMore(list)
        }
    }

    private fun refresh(list: MutableList<T>) {
        loadMoreModule?.isEnableLoadMore = true
        swipeRefreshLayout.isRefreshing = false
        if (list.isEmpty()) {
            multipleStatusView.showEmpty()
            adapter.setNewInstance(mutableListOf())
        } else {
            adapter.setNewInstance(list)
            //一页数据不够，结束加载更多
            if (enLoadMore && list.size < AppConstant.LOAD_PER_PAGE) {
                loadMoreModule?.loadMoreEnd()
            }
        }
        multipleStatusView.showContent()
    }

    private fun loadMore(list: List<T>) {
        if (list.isEmpty()) {
            loadMoreModule?.loadMoreEnd()
        } else {
            adapter.addData(list)
            if (list.size < AppConstant.LOAD_PER_PAGE) {
                loadMoreModule?.loadMoreEnd()
            } else {
                loadMoreModule?.loadMoreComplete()
            }
        }
    }

    protected open fun updateFail(error: String, reload: Boolean) {
        if (reload) {
            loadMoreModule?.isEnableLoadMore = true
            swipeRefreshLayout.isRefreshing = false
            multipleStatusView.showError()
        } else {
            loadMoreModule?.loadMoreFail()
        }
    }

    abstract fun reload()

    abstract fun loadNext()

}