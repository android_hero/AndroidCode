package com.cherry.androidcode.recyclerView

import com.cherry.androidcode.entity.SelectGankMeiZiBean
import com.cherry.androidcode.mvp1.BasePresenter
import com.cherry.androidcode.mvp1.BaseView

/**
 * @author DongMS
 * @since 2019/7/15
 */
interface TestMeiZiRecyclerViewContract {

    interface View : BaseView {
        fun updateMeiZiSuccess(list: MutableList<SelectGankMeiZiBean.Result>, isRefresh: Boolean)
        fun updateMeiZiFailure(errorMsg: String, isRefresh: Boolean)
    }

    interface Presenter : BasePresenter<View> {
        fun reload()
        fun loadMore(page: Int)
    }

}