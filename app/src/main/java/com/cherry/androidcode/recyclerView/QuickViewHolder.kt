package com.cherry.androidcode.recyclerView

import android.view.View
import android.widget.Checkable
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.annotation.IdRes
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.cherry.androidcode.R
import com.cherry.androidcode.image.GlideApp
import com.cherry.androidcode.widget.SmoothCheckBox

/**
 * @author DongMS
 * @date 2019/6/24
 */
open class QuickViewHolder(view: View) : BaseViewHolder(view) {


    fun setTextSize(@IdRes viewId: Int, textSize: Float): BaseViewHolder {
        getView<TextView>(viewId).textSize = textSize
        return this
    }

    fun setCheckedAnim(viewId: Int, checked: Boolean): BaseViewHolder {
        val view = getView<View>(viewId)
        if (view is SmoothCheckBox) {
            view.setChecked(checked, true)
        } else if (view is Checkable) {
            view.isChecked = checked
        }
        return this
    }

    fun setSelected(@IdRes viewId: Int, isSelected: Boolean): BaseViewHolder {
        getView<View>(viewId).isSelected = isSelected
        return this
    }

    @JvmOverloads
    fun setImageWithGlide(@IdRes viewId: Int, url: String?, @DrawableRes placeholder: Int = R.mipmap.ic_launcher): BaseViewHolder {
        val view = getView<ImageView>(viewId)
        GlideApp.with(view).load(url).placeholder(placeholder).fitCenter().into(view)
        return this
    }

}