package com.cherry.androidcode.recyclerView

import com.chad.library.adapter.base.BaseQuickAdapter
import com.cherry.androidcode.R
import com.cherry.androidcode.entity.SearchBean
import com.cherry.androidcode.util.resToColor
import com.yanyusong.y_divideritemdecoration.Y_Divider
import com.yanyusong.y_divideritemdecoration.Y_DividerBuilder
import com.yanyusong.y_divideritemdecoration.Y_DividerItemDecoration

/**
 * @author DongMS
 * @date 2019/6/24
 */
class TestSwipeMenuFragment : ListViewFragment<SearchBean.Data, TestSimpleRecyclerViewContract.View, TestSimpleRecyclerViewPresenter>(), TestSimpleRecyclerViewContract.View {

    override val dividerDecoration by lazy {
        object : Y_DividerItemDecoration(activity) {
            override fun getDivider(itemPosition: Int): Y_Divider {
                return Y_DividerBuilder()
                        .setTopSideLine(itemPosition != 0, R.color.divider.resToColor(), 1f, 16f, 16f)
                        .create()
            }
        }
    }

    override val enRefresh = false

    override val enLoadMore = false

    override val adapter: BaseQuickAdapter<SearchBean.Data, QuickViewHolder> = TestSimpleAdapter(R.layout.item_test_swipe_menu)

    override fun reload() {
        mvpPresenter?.reload("gradle")
    }

    //nothing
    override fun loadNext() {
    }

    override fun updateListSuccess(bean: MutableList<SearchBean.Data>, isRefresh: Boolean) {
        updateList(bean, isRefresh)
    }

    override fun updateListFail(errorMsg: String, isRefresh: Boolean) {
        updateFail(errorMsg, isRefresh)
    }

    override fun createPresenter() = TestSimpleRecyclerViewPresenter()

    override fun createView() = this

}