package com.cherry.androidcode.recyclerView

import com.chad.library.adapter.base.BaseQuickAdapter
import com.cherry.androidcode.R
import com.cherry.androidcode.entity.SearchBean
import com.cherry.androidcode.util.resToColor
import com.yanyusong.y_divideritemdecoration.Y_Divider
import com.yanyusong.y_divideritemdecoration.Y_DividerBuilder
import com.yanyusong.y_divideritemdecoration.Y_DividerItemDecoration

/**
 * @author DongMS
 * @date 2019/6/24
 */
class TestRefreshLoadFragment : ListViewFragment<SearchBean.Data, TestSimpleRecyclerViewContract.View, TestSimpleRecyclerViewPresenter>(), TestSimpleRecyclerViewContract.View {

    override val dividerDecoration by lazy {
        object : Y_DividerItemDecoration(activity) {
            override fun getDivider(itemPosition: Int): Y_Divider {
                return Y_DividerBuilder()
                        .setTopSideLine(itemPosition != 0, R.color.divider.resToColor(), 1f, 0f, 0f)
                        .create()
            }
        }
    }

    override val adapter: BaseQuickAdapter<SearchBean.Data, QuickViewHolder> = TestSimpleAdapter(R.layout.item_test_simple)

    override fun reload() {
        mvpPresenter?.reload("glide")
//       mvpPresenter?.reload("相机")//不够一页
    }


    override fun loadNext() {
        mvpPresenter?.loadMore(page, "glide")
    }

    override fun updateListSuccess(bean: MutableList<SearchBean.Data>, isRefresh: Boolean) {
        updateList(bean, isRefresh)
    }

    override fun updateListFail(errorMsg: String, isRefresh: Boolean) {
        updateFail(errorMsg, isRefresh)
    }

    override fun createPresenter() = TestSimpleRecyclerViewPresenter()

    override fun createView() = this

}