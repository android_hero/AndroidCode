package com.cherry.androidcode.recyclerView

import android.annotation.SuppressLint
import com.cherry.androidcode.ApiUrl
import com.cherry.androidcode.entity.SelectGankMeiZiBean
import com.cherry.androidcode.mvp1.MVPBasePresenter
import com.cherry.androidcode.okgo.JsonConvert
import com.lzy.okgo.OkGo
import com.lzy.okrx2.adapter.ObservableBody
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * @author DongMS
 * @since 2019/7/15
 */
class TestMeiZiRecyclerViewPresenter : MVPBasePresenter<TestMeiZiRecyclerViewContract.View>(), TestMeiZiRecyclerViewContract.Presenter {


    override fun reload() {
        loadData(0, true)
    }

    override fun loadMore(page: Int) {
        loadData(page, false)
    }

    @SuppressLint("CheckResult")
    private fun loadData(page: Int, isRefresh: Boolean) {
        OkGo.get<SelectGankMeiZiBean>("${ApiUrl.GANK_MEIZI_URL}$page")
                .converter(object : JsonConvert<SelectGankMeiZiBean>(clazz = SelectGankMeiZiBean::class.java) {})
                .adapt(ObservableBody<SelectGankMeiZiBean>())
                .subscribeOn(Schedulers.io())
                .filter { !it.error }
                .map { it.results }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view?.updateMeiZiSuccess(it, isRefresh)
                }, {
                    view?.updateMeiZiFailure(it?.message ?: "", isRefresh)
                })
    }
}