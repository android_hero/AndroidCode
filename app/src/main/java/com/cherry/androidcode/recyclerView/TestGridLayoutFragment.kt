package com.cherry.androidcode.recyclerView

import androidx.recyclerview.widget.GridLayoutManager
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.module.LoadMoreModule
import com.cherry.androidcode.R
import com.cherry.androidcode.entity.SelectGankMeiZiBean
import com.cherry.androidcode.helper.ImageViewerHelper
import com.cherry.androidcode.helper.ImageViewerHelper.TransitionViewsRef.KEY_MAIN
import com.cherry.androidcode.util.resToColor
import com.yanyusong.y_divideritemdecoration.Y_Divider
import com.yanyusong.y_divideritemdecoration.Y_DividerBuilder
import com.yanyusong.y_divideritemdecoration.Y_DividerItemDecoration

/**
 * @author DongMS
 * @date 2019/6/26
 */
class TestGridLayoutFragment : ListViewFragment<SelectGankMeiZiBean.Result, TestMeiZiRecyclerViewContract.View, TestMeiZiRecyclerViewPresenter>(), TestMeiZiRecyclerViewContract.View {

    override val layoutManager by lazy {
        GridLayoutManager(activity, 2)
    }

    override val dividerDecoration by lazy {
        object : Y_DividerItemDecoration(activity) {
            override fun getDivider(itemPosition: Int): Y_Divider {
                //note 这里的尺寸参数是dp，不是px
                val paddingColor = R.color.transparent.resToColor()
                return Y_DividerBuilder()
                        .setTopSideLine(itemPosition == 0 || itemPosition == 1, paddingColor, 10f, 0f, 0f)
                        //不包括加载更多的view
                        .setBottomSideLine(itemPosition != adapter.itemCount - 1, paddingColor, 10f, 0f, 0f)
                        .setLeftSideLine(true, paddingColor, 10f, 0f, 0f)
                        .setRightSideLine(itemPosition % 2 != 0, paddingColor, 10f, 0f, 0f)
                        .create()
            }
        }
    }


    class TestGridLayoutAdatper : BaseQuickAdapter<SelectGankMeiZiBean.Result, QuickViewHolder>(R.layout.item_test_image_simple), LoadMoreModule {
        override fun convert(helper: QuickViewHolder, item: SelectGankMeiZiBean.Result) {
            helper.setImageWithGlide(R.id.iv, item.url)
                    .setText(R.id.tv_author, item.who)
                    .setText(R.id.tv_date, item.desc)
        }
    }


    override val adapter: BaseQuickAdapter<SelectGankMeiZiBean.Result, QuickViewHolder> = TestGridLayoutAdatper()

    override fun initRecyclerView() {
        adapter.addChildClickViewIds(R.id.iv)
        adapter.setOnItemChildClickListener { a, view, position ->
            if (view.id == R.id.iv) {
//                startActivity(Intent(activity, PreviewImageActivity::class.java).apply { putExtra("img", adapter.getItem(position).url) })
                ImageViewerHelper.provideImageViewerBuilder(activity!!, adapter.getItem(position).url).show()
            }
        }
    }

    override fun reload() {
        mvpPresenter?.reload()
    }

    override fun loadNext() {
        mvpPresenter?.loadMore(page)
    }

    override fun updateMeiZiSuccess(list: MutableList<SelectGankMeiZiBean.Result>, isRefresh: Boolean) {
        updateList(list, isRefresh)
    }

    override fun updateMeiZiFailure(errorMsg: String, isRefresh: Boolean) {
        updateFail(errorMsg, isRefresh)
    }

    override fun createPresenter() = TestMeiZiRecyclerViewPresenter()

    override fun createView() = this

    override fun onDestroy() {
        super.onDestroy()
        ImageViewerHelper.TransitionViewsRef.releaseTransitionViewRef(KEY_MAIN)
    }


}