package com.cherry.androidcode.recyclerView

import com.cherry.androidcode.demo.DemoBean
import com.cherry.androidcode.mvp1.BaseView
import com.cherry.androidcode.mvp1.MVPBasePresenter

/**
 * @author Cherry(dongmiansheng@gmail.com)
 * @since 2021/5/29
 */
class DemoPresenter(val loadData: (() -> MutableList<DemoBean>)) : MVPBasePresenter<BaseView>()