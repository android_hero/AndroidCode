package com.cherry.androidcode.recyclerView

import android.util.Log
import android.widget.Button
import com.cherry.androidcode.R
import com.cherry.androidcode.entity.SelectGankMeiZiBean
import com.cherry.androidcode.util.inflate
import com.cherry.androidcode.util.resToColor
import com.yanyusong.y_divideritemdecoration.Y_Divider
import com.yanyusong.y_divideritemdecoration.Y_DividerBuilder
import com.yanyusong.y_divideritemdecoration.Y_DividerItemDecoration

/**
 * @author DongMS
 * @date 2019/6/26
 */
class TestMultiSelectFragment : ListViewFragment<SelectGankMeiZiBean.Result, TestMeiZiRecyclerViewContract.View, TestMeiZiRecyclerViewPresenter>(), TestMeiZiRecyclerViewContract.View {

    override val dividerDecoration by lazy {
        object : Y_DividerItemDecoration(activity) {
            override fun getDivider(itemPosition: Int): Y_Divider {
                //note 这里的尺寸参数是dp，不是px
                val paddingColor = R.color.transparent.resToColor()
                return Y_DividerBuilder()
                        .setTopSideLine(itemPosition == 0, paddingColor, 16f, 0f, 0f)
                        //不包括加载更多的view
                        .setBottomSideLine(itemPosition != adapter.itemCount - 1, paddingColor, 16f, 0f, 0f)
                        .setLeftSideLine(true, paddingColor, 16f, 0f, 0f)
                        .setRightSideLine(true, paddingColor, 16f, 0f, 0f)
                        .create()
            }
        }
    }
    override val adapter = object : SelectQuickAdapter<SelectGankMeiZiBean.Result, QuickViewHolder>(R.layout.item_test_image_select) {

        override val checkedViewId = R.id.scb

        override val isMultiSelected = true

        override fun bindData(helper: QuickViewHolder, item: SelectGankMeiZiBean.Result) {
            helper.setImageWithGlide(R.id.iv, item.url)
                    .setText(R.id.tv_author, item.who)
                    .setText(R.id.tv_date, item.desc)
        }
    }

    override fun initRecyclerView() {
        val headerLayout = R.layout.layout_list_header_select_controller.inflate()
        val enableSelect: Button = headerLayout.findViewById(R.id.btn_enable_select)
        enableSelect.setOnClickListener {
            adapter.toggleSelect()
            enableSelect.text = if (adapter.isSelectState) "取消" else "选择"
        }
        headerLayout.findViewById<Button>(R.id.btn_all_select).setOnClickListener {
            adapter.selectAll()
        }
        headerLayout.findViewById<Button>(R.id.btn_none_select).setOnClickListener {
            adapter.unSelectAll()
        }
        headerLayout.findViewById<Button>(R.id.btn_confirm_select).setOnClickListener {
            Log.e("=========", "多选择${adapter.getMultiSelectItem()?.toString()}")
        }
        adapter.addHeaderView(headerLayout)

        adapter.setOnItemClickListener { adapter, view, position ->
            Log.e("========", "点击$position")
        }
    }


    override fun reload() {
        mvpPresenter?.reload()
    }

    override fun loadNext() {
        mvpPresenter?.loadMore(page)
    }

    override fun updateMeiZiSuccess(list: MutableList<SelectGankMeiZiBean.Result>, isRefresh: Boolean) {
        updateList(list, isRefresh)
    }

    override fun updateMeiZiFailure(errorMsg: String, isRefresh: Boolean) {
        updateFail(errorMsg, isRefresh)
    }

    override fun createPresenter() = TestMeiZiRecyclerViewPresenter()

    override fun createView() = this

}