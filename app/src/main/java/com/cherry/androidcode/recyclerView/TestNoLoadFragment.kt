package com.cherry.androidcode.recyclerView

import android.content.Intent
import com.chad.library.adapter.base.BaseQuickAdapter
import com.cherry.androidcode.R
import com.cherry.androidcode.activity.WebViewActivity
import com.cherry.androidcode.entity.SearchBean
import com.cherry.androidcode.util.resToColor
import com.yanyusong.y_divideritemdecoration.Y_Divider
import com.yanyusong.y_divideritemdecoration.Y_DividerBuilder
import com.yanyusong.y_divideritemdecoration.Y_DividerItemDecoration

/**
 * @author DongMS
 * @date 2019/6/24
 */
class TestNoLoadFragment : ListViewFragment<SearchBean.Data, TestSimpleRecyclerViewContract.View, TestSimpleRecyclerViewPresenter>(), TestSimpleRecyclerViewContract.View {
    override val dividerDecoration by lazy {
        object : Y_DividerItemDecoration(activity) {
            override fun getDivider(itemPosition: Int): Y_Divider {
                //note 这里的尺寸参数是dp，不是px
                val paddingColor = R.color.transparent.resToColor()
                return Y_DividerBuilder()
                        .setTopSideLine(true, paddingColor, 16f, 0f, 0f)
                        .setBottomSideLine(itemPosition == adapter.itemCount - 1, paddingColor, 16f, 0f, 0f)
                        .setLeftSideLine(true, paddingColor, 16f, 0f, 0f)
                        .setRightSideLine(true, paddingColor, 16f, 0f, 0f)
                        .create()
            }
        }
    }


    override val enLoadMore = false

    override val adapter: BaseQuickAdapter<SearchBean.Data, QuickViewHolder> = TestSimpleAdapter(R.layout.item_test_card_view_simple)


    override fun initRecyclerView() {
        adapter.setOnItemClickListener { _, _, position ->
            startActivity(Intent(activity, WebViewActivity::class.java).apply {
                putExtra("url", adapter.data[position].link)
            })
        }
    }

    override fun reload() {
        mvpPresenter?.reload("gradle")
    }

    //nothing
    override fun loadNext() {
    }

    override fun updateListSuccess(bean: MutableList<SearchBean.Data>, isRefresh: Boolean) {
        updateList(bean, isRefresh)
    }

    override fun updateListFail(errorMsg: String, isRefresh: Boolean) {
        updateFail(errorMsg, isRefresh)
    }

    override fun createPresenter() = TestSimpleRecyclerViewPresenter()

    override fun createView() = this

}