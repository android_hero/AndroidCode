package com.cherry.androidcode.recyclerView

import com.cherry.androidcode.entity.SearchBean
import com.cherry.androidcode.mvp1.BasePresenter
import com.cherry.androidcode.mvp1.BaseView

/**
 * @author DongMS
 * @since 2019/7/15
 */
interface TestSimpleRecyclerViewContract {

    interface View : BaseView {

        fun updateListSuccess(bean: MutableList<SearchBean.Data>, isRefresh: Boolean)

        fun updateListFail(errorMsg: String, isRefresh: Boolean)

    }

    interface Presenter : BasePresenter<View> {
        fun reload(keyWord: String)
        fun loadMore(page: Int, keyWord: String)
    }
}