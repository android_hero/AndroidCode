package com.cherry.androidcode.recyclerView

import android.text.Html
import androidx.annotation.LayoutRes
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.module.LoadMoreModule
import com.cherry.androidcode.R
import com.cherry.androidcode.entity.SearchBean

/**
 * @author DongMS
 * @date 2019/6/24
 */
class TestSimpleAdapter(@LayoutRes layoutRes: Int) : BaseQuickAdapter<SearchBean.Data, QuickViewHolder>(layoutRes), LoadMoreModule {

    override fun convert(helper: QuickViewHolder, item: SearchBean.Data) {
        helper.setText(R.id.tv_title, Html.fromHtml(item.title))
                .setText(R.id.tv_author, item.author)
                .setText(R.id.tv_date, item.niceDate)
    }
}