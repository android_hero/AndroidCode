package com.cherry.androidcode.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.cherry.androidcode.R;
import com.cherry.androidcode.util.ExtensionKt;

/**
 * @author DongMS
 * @date 2019/6/21
 */
public class ProgressWebView extends WebView {

    private ProgressBar mProgressBar;
    private boolean isAnimStart;
    private int mCurrentProgress;
    private int mProgressDrawableRes;
    private int mProgressHeight;

    public ProgressWebView(Context context) {
        this(context, null);
    }

    public ProgressWebView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProgressWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ProgressWebView);
        mProgressHeight = a.getDimensionPixelSize(R.styleable.ProgressWebView_progressHeight, ExtensionKt.dpToPx(3));
        mProgressDrawableRes = a.getResourceId(R.styleable.ProgressWebView_progressDrawable, R.drawable.progress_h_webview);
        a.recycle();
        addProgressBar();
    }

    private void addProgressBar() {
        mProgressBar = new ProgressBar(getContext(), null, android.R.attr.progressBarStyleHorizontal);
        mProgressBar.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, mProgressHeight, 0, 0));
        mProgressBar.setProgressDrawable(ExtensionKt.resToDrawable(mProgressDrawableRes));
        addView(mProgressBar);
    }

    public void start() {
        mProgressBar.setVisibility(View.VISIBLE);
        mProgressBar.setAlpha(1.0f);
    }

    public void setProgress(int newProgress) {
        mCurrentProgress = mProgressBar.getProgress();
        if (newProgress >= 100 && !isAnimStart) {
            // 防止调用多次动画
            isAnimStart = true;
            mProgressBar.setProgress(newProgress);
            // 开启属性动画让进度条平滑消失
            startDismissAnimation(mProgressBar.getProgress());
        } else {
            // 开启属性动画让进度条平滑递增
            startProgressAnimation(newProgress);
        }
    }

    /**
     * progressBar递增动画
     */
    private void startProgressAnimation(int newProgress) {
        ObjectAnimator animator = ObjectAnimator.ofInt(mProgressBar, "progress", mCurrentProgress, newProgress);
        animator.setDuration(300);
        animator.setInterpolator(new DecelerateInterpolator());
        animator.start();
    }

    /**
     * progressBar消失动画
     */
    private void startDismissAnimation(final int progress) {
        ObjectAnimator anim = ObjectAnimator.ofFloat(mProgressBar, "alpha", 1.0f, 0.0f);
        anim.setDuration(1500);  // 动画时长
        anim.setInterpolator(new DecelerateInterpolator());     // 减速
        // 关键, 添加动画进度监听器
        anim.addUpdateListener(valueAnimator -> {
            float fraction = valueAnimator.getAnimatedFraction();      // 0.0f ~ 1.0f
            int offset = 100 - progress;
            if (null != mProgressBar) {
                mProgressBar.setProgress((int) (progress + offset * fraction));
            }
        });

        anim.addListener(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationEnd(Animator animation) {
                // 动画结束
                mProgressBar.setProgress(0);
                mProgressBar.setVisibility(View.GONE);
                isAnimStart = false;
            }
        });
        anim.start();
    }
}
