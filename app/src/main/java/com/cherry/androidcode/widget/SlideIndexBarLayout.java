package com.cherry.androidcode.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * @author DongMS
 * @since 2019/9/6
 */
public class SlideIndexBarLayout extends FrameLayout {
    public SlideIndexBarLayout(@NonNull Context context) {
        this(context, null);
    }

    public SlideIndexBarLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SlideIndexBarLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context,attrs);
    }

    private void init(Context context, AttributeSet attrs) {

    }
}
