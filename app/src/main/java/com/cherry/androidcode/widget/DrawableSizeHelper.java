package com.cherry.androidcode.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.TextView;

import com.cherry.androidcode.R;


public class DrawableSizeHelper {
    private Drawable left;
    private Drawable top;
    private Drawable right;
    private Drawable bottom;

    public int drawableWidth;
    public int drawableHeight;

    public void readAttributes(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.DrawableButton);
        drawableWidth = typedArray.getDimensionPixelSize(R.styleable.DrawableButton_drawableWidth, 0);
        drawableHeight = typedArray.getDimensionPixelSize(R.styleable.DrawableButton_drawableHeight, 0);
        typedArray.recycle();
    }

    public void setDrawable(Drawable left, Drawable top, Drawable right, Drawable bottom) {
        this.left = left;
        this.top = top;
        this.right = right;
        this.bottom = bottom;
    }

    public boolean isNotSet() {
        return drawableWidth <= 0 && drawableHeight <= 0;
    }

    public void setCompoundDrawablesWithIntrinsicBounds(TextView widget) {
        if (left != null) {
            left.setBounds(0, 0, calculateWidth(left), calculateHeight(left));
        }
        if (right != null) {
            right.setBounds(0, 0, calculateWidth(right), calculateHeight(right));
        }
        if (top != null) {
            top.setBounds(0, 0, calculateWidth(top), calculateHeight(top));
        }
        if (bottom != null) {
            bottom.setBounds(0, 0, calculateWidth(bottom), calculateHeight(bottom));
        }
        widget.setCompoundDrawables(left, top, right, bottom);
    }

    public int calculateWidth(Drawable d) {
        if(drawableWidth == 0) {
            return d.getIntrinsicWidth() * drawableHeight / d.getIntrinsicHeight();
        }
        return drawableWidth;
    }

    public int calculateHeight(Drawable d) {
        if(drawableHeight == 0) {
            return  d.getIntrinsicHeight() * drawableWidth / d.getIntrinsicWidth();
        }
        return drawableHeight;
    }

    @TargetApi(17)
    public void setCompoundDrawablesRelativeWithIntrinsicBounds(TextView widget) {
        if (left != null) {
            left.setBounds(0, 0, drawableWidth, drawableHeight);
        }
        if (right != null) {
            right.setBounds(0, 0, drawableWidth, drawableHeight);
        }
        if (top != null) {
            top.setBounds(0, 0, drawableWidth, drawableHeight);
        }
        if (bottom != null) {
            bottom.setBounds(0, 0, drawableWidth, drawableHeight);
        }
        widget.setCompoundDrawablesRelativeWithIntrinsicBounds(left, top, right, bottom);
    }
}
