package com.cherry.androidcode.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.appcompat.widget.AppCompatCheckBox;

/**
 * @author Cherry(dongmiansheng @ gmail.com)
 * @since 2019/7/28
 */

public class NoClickCheckBox extends AppCompatCheckBox {
    public NoClickCheckBox(Context context) {
        super(context);
    }

    public NoClickCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NoClickCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }
}
