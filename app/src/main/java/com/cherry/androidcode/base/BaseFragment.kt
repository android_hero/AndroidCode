package com.cherry.androidcode.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.fragment.app.Fragment

/**
 * @author Cherry(dongmiansheng@gmail.com)
 * @since 2019/6/30
 */

abstract class BaseFragment : Fragment() {

    protected abstract val contentLayout: Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(contentLayout, container, false)
    }

    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        //todo

        initView(view)
    }

    open fun initView(view: View) {
    }
}