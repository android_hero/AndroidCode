package com.cherry.androidcode.base

import android.os.Bundle
import androidx.annotation.CallSuper
import com.cherry.androidcode.R
import com.cherry.androidcode.helper.DrawableHelper
import kotlinx.android.synthetic.main.activity_title_bar.*
import kotlinx.android.synthetic.main.layout_title_bar.*

/**
 * @author Cherry(dongmiansheng@gmail.com)
 * @since 2019/6/29
 */

abstract class BaseTitleBarActivity : BaseActivity() {

    abstract val titleText: Int

    abstract val contentLayoutRes: Int

    override val activityLayoutRes = R.layout.activity_title_bar
    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        content.layoutResource = contentLayoutRes
        content.inflate()
        iv_back.setOnClickListener { finish() }
        tv_title.setText(titleText)
        DrawableHelper.normalDivider(toolbar)
        initView()
    }

    protected open fun initView() {
    }
}