package com.cherry.androidcode.base

import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.appcompat.app.AppCompatActivity
import com.umeng.message.PushAgent

/**
 * @author Cherry(dongmiansheng@gmail.com)
 * @since 2019/6/29
 */

abstract class BaseActivity : AppCompatActivity() {

    protected abstract val activityLayoutRes: Int

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activityLayoutRes)
        //该方法是【友盟+】Push后台进行日活统计及多维度推送的必调用方法，请务必调用！
        PushAgent.getInstance(this).onAppStart()
    }

}