package com.cherry.androidcode.entity


import com.google.gson.annotations.SerializedName

data class SearchBean(
        @SerializedName("curPage")
        val curPage: Int,
        @SerializedName("datas")
        val datas: MutableList<Data>,
        @SerializedName("offset")
        val offset: Int,
        @SerializedName("over")
        val over: Boolean,
        @SerializedName("pageCount")
        val pageCount: Int,
        @SerializedName("size")
        val size: Int,
        @SerializedName("total")
        val total: Int
) {
    data class Data(
            @SerializedName("apkLink")
            val apkLink: String,
            @SerializedName("author")
            val author: String,
            @SerializedName("chapterId")
            val chapterId: Int,
            @SerializedName("chapterName")
            val chapterName: String,
            @SerializedName("collect")
            val collect: Boolean,
            @SerializedName("courseId")
            val courseId: Int,
            @SerializedName("desc")
            val desc: String,
            @SerializedName("envelopePic")
            val envelopePic: String,
            @SerializedName("fresh")
            val fresh: Boolean,
            @SerializedName("id")
            val id: Int,
            @SerializedName("link")
            val link: String,
            @SerializedName("niceDate")
            val niceDate: String,
            @SerializedName("origin")
            val origin: String,
            @SerializedName("prefix")
            val prefix: String,
            @SerializedName("projectLink")
            val projectLink: String,
            @SerializedName("publishTime")
            val publishTime: Long,
            @SerializedName("superChapterId")
            val superChapterId: Int,
            @SerializedName("superChapterName")
            val superChapterName: String,
            @SerializedName("tags")
            val tags: List<TagsBean>,
            @SerializedName("title")
            val title: String,
            @SerializedName("type")
            val type: Int,
            @SerializedName("userId")
            val userId: Int,
            @SerializedName("visible")
            val visible: Int,
            @SerializedName("zan")
            val zan: Int
    ) {
        class TagsBean(
                /**
                 * name : 问答
                 * url : /article/list/0?cid=440
                 */
                val name: String,
                val url: String
        ) {
            override fun toString(): String {
                return "TagsBean(name='$name', url='$url')"
            }
        }

        override fun toString(): String {
            return "Data(apkLink='$apkLink', author='$author', chapterId=$chapterId, chapterName='$chapterName', collect=$collect, courseId=$courseId, desc='$desc', envelopePic='$envelopePic', fresh=$fresh, id=$id, link='$link', niceDate='$niceDate', origin='$origin', prefix='$prefix', projectLink='$projectLink', publishTime=$publishTime, superChapterId=$superChapterId, superChapterName='$superChapterName', tags=$tags, title='$title', type=$type, userId=$userId, visible=$visible, zan=$zan)"
        }

    }

    override fun toString(): String {
        return "SearchBean(curPage=$curPage, datas=$datas, offset=$offset, over=$over, pageCount=$pageCount, size=$size, total=$total)"
    }
}