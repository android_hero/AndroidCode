package com.cherry.androidcode.entity

import com.github.iielse.imageviewer.adapter.ItemType
import com.github.iielse.imageviewer.core.Photo

/**
 * @author DongMS
 * @since 2020-09-23
 */
data class ImageBean(val url: String) : Photo {

    override fun id(): Long = url.hashCode().toLong()

    //这里没有图片的宽高像素，先做小图判断
    override fun itemType(): Int = ItemType.PHOTO
}