package com.cherry.androidcode.entity

/**
 * @author DongMS
 * @since 2019/9/5
 */
data class MultiVideoDetail(val source: String?, val videoList: VideoDetail.VideoList?)