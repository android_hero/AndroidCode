package com.cherry.androidcode.entity

/**
 * @author Cherry(dongmiansheng@gmail.com)
 * @since 2019/8/31
 */

data class VideoDetail(val source: String, val videoList: List<VideoList>) {


    data class VideoList(val name: String, val url: String)
}