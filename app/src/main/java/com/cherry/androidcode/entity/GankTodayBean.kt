package com.cherry.androidcode.entity


import com.google.gson.annotations.SerializedName

data class GankTodayBean(
    @SerializedName("category")
    val category: List<String>,
    @SerializedName("error")
    val error: Boolean,
    @SerializedName("results")
    val results: Results
) {
    data class Results(
            @SerializedName("Android")
        val android: List<Android>,
            @SerializedName("App")
        val app: List<App>,
            @SerializedName("iOS")
        val iOS: List<IOS>,
            @SerializedName("休息视频")
        val lazyVideo: List<LazyVideo>,
            @SerializedName("前端")
        val front: List<Front>,
            @SerializedName("拓展资源")
        val otherResource: List<OtherResource>,
            @SerializedName("瞎推荐")
        val recommend: List<Recommend>,
            @SerializedName("福利")
        val meizi: List<MeiZi>
    ) {
        data class IOS(
            @SerializedName("createdAt")
            val createdAt: String,
            @SerializedName("desc")
            val desc: String,
            @SerializedName("_id")
            val id: String,
            @SerializedName("images")
            val images: List<String>,
            @SerializedName("publishedAt")
            val publishedAt: String,
            @SerializedName("source")
            val source: String,
            @SerializedName("type")
            val type: String,
            @SerializedName("url")
            val url: String,
            @SerializedName("used")
            val used: Boolean,
            @SerializedName("who")
            val who: String
        )

        data class App(
            @SerializedName("createdAt")
            val createdAt: String,
            @SerializedName("desc")
            val desc: String,
            @SerializedName("_id")
            val id: String,
            @SerializedName("images")
            val images: List<String>,
            @SerializedName("publishedAt")
            val publishedAt: String,
            @SerializedName("source")
            val source: String,
            @SerializedName("type")
            val type: String,
            @SerializedName("url")
            val url: String,
            @SerializedName("used")
            val used: Boolean,
            @SerializedName("who")
            val who: String
        )

        data class MeiZi(
            @SerializedName("createdAt")
            val createdAt: String,
            @SerializedName("desc")
            val desc: String,
            @SerializedName("_id")
            val id: String,
            @SerializedName("publishedAt")
            val publishedAt: String,
            @SerializedName("source")
            val source: String,
            @SerializedName("type")
            val type: String,
            @SerializedName("url")
            val url: String,
            @SerializedName("used")
            val used: Boolean,
            @SerializedName("who")
            val who: String
        )

        data class Android(
            @SerializedName("createdAt")
            val createdAt: String,
            @SerializedName("desc")
            val desc: String,
            @SerializedName("_id")
            val id: String,
            @SerializedName("images")
            val images: List<String>,
            @SerializedName("publishedAt")
            val publishedAt: String,
            @SerializedName("source")
            val source: String,
            @SerializedName("type")
            val type: String,
            @SerializedName("url")
            val url: String,
            @SerializedName("used")
            val used: Boolean,
            @SerializedName("who")
            val who: String
        )

        data class OtherResource(
            @SerializedName("createdAt")
            val createdAt: String,
            @SerializedName("desc")
            val desc: String,
            @SerializedName("_id")
            val id: String,
            @SerializedName("images")
            val images: List<String>,
            @SerializedName("publishedAt")
            val publishedAt: String,
            @SerializedName("source")
            val source: String,
            @SerializedName("type")
            val type: String,
            @SerializedName("url")
            val url: String,
            @SerializedName("used")
            val used: Boolean,
            @SerializedName("who")
            val who: String
        )

        data class Recommend(
            @SerializedName("createdAt")
            val createdAt: String,
            @SerializedName("desc")
            val desc: String,
            @SerializedName("_id")
            val id: String,
            @SerializedName("images")
            val images: List<String>,
            @SerializedName("publishedAt")
            val publishedAt: String,
            @SerializedName("source")
            val source: String,
            @SerializedName("type")
            val type: String,
            @SerializedName("url")
            val url: String,
            @SerializedName("used")
            val used: Boolean,
            @SerializedName("who")
            val who: String
        )

        data class LazyVideo(
            @SerializedName("createdAt")
            val createdAt: String,
            @SerializedName("desc")
            val desc: String,
            @SerializedName("_id")
            val id: String,
            @SerializedName("publishedAt")
            val publishedAt: String,
            @SerializedName("source")
            val source: String,
            @SerializedName("type")
            val type: String,
            @SerializedName("url")
            val url: String,
            @SerializedName("used")
            val used: Boolean,
            @SerializedName("who")
            val who: String
        )

        data class Front(
            @SerializedName("createdAt")
            val createdAt: String,
            @SerializedName("desc")
            val desc: String,
            @SerializedName("_id")
            val id: String,
            @SerializedName("images")
            val images: List<String>,
            @SerializedName("publishedAt")
            val publishedAt: String,
            @SerializedName("source")
            val source: String,
            @SerializedName("type")
            val type: String,
            @SerializedName("url")
            val url: String,
            @SerializedName("used")
            val used: Boolean,
            @SerializedName("who")
            val who: String
        )
    }
}