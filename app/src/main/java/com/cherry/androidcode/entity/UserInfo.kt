package com.cherry.androidcode.entity

import com.chibatching.kotpref.KotprefModel
import com.chibatching.kotpref.enumpref.enumValuePref
import com.chibatching.kotpref.gsonpref.gsonPref
import java.util.*

/**
 * 2.SharePreference封装库，定义模型，一般这里都是app的配置，需要继承KotprefModel
 * @author DongMS
 * @date 2019/7/3
 */
object UserInfo : KotprefModel() {

    //提交都是用apply，如果要改成commit提交
    // 全部都改：override val commitAllPropertiesByDefault: Boolean = true
    //针对某一项：var age: Int by intPref(default = 18, commitByDefault = true)
    //因为要读写，所以必须是var

    var gameLevel by enumValuePref(GameLevel.NORMAL)

    //默认值就是""
    var name by stringPref()
    var code by nullableStringPref()
    //不写默认值0
    var age by intPref(default = -1)
    var highScore by longPref()
    var rate by floatPref()
    //默认false
    var flag by booleanPref()

    //Set不常用啊。。。
    val prizes by stringSetPref {
        val set = TreeSet<String>()
        set.add("Beginner")
        return@stringSetPref set
    }

    var city by gsonPref(CityBean())

    var stringList by gsonPref(listOf<String>())

    enum class GameLevel {
        EASY,
        NORMAL,
        HARD;
    }
}

