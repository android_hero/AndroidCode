package com.cherry.androidcode.entity

import com.cherry.androidcode.recyclerView.ISelect
import com.google.gson.annotations.SerializedName

/**
 * @author DongMS
 * @date 2019/6/26
 */
data class SelectGankMeiZiBean(
        @SerializedName("error")
        val error: Boolean,
        @SerializedName("results")
        val results: MutableList<Result>
) {
    data class Result(@SerializedName("createdAt")
                 val createdAt: String,
                 @SerializedName("desc")
                 val desc: String,
                 @SerializedName("_id")
                 val id: String,
                 @SerializedName("publishedAt")
                 val publishedAt: String,
                 @SerializedName("source")
                 val source: String,
                 @SerializedName("type")
                 val type: String,
                 @SerializedName("url")
                 val url: String,
                 @SerializedName("used")
                 val used: Boolean,
                 @SerializedName("who")
                 val who: String
    ) : ISelect {
        override var isSelected = false
        override fun toString(): String {
            return "Result(createdAt='$createdAt', desc='$desc', id='$id', publishedAt='$publishedAt', source='$source', type='$type', url='$url', used=$used, who='$who', isSelected=$isSelected)"
        }
    }
}