package com.cherry.androidcode.entity

/**
 * @author Cherry(dongmiansheng @ gmail.com)
 * @since 2019/8/31
 */

data class VideoSearchList(
        val videoName: String,
        val videoLink: String,
        val videoCover: String?,
        val actor: String?,
        val area: String?,
        val time: String?,
        val otherInfo:String?=null
){

    override fun toString(): String {
        return "VideoSearchList(videoName='$videoName', videoLink='$videoLink', videoCover=$videoCover, actor=$actor, area=$area, time=$time, otherInfo=$otherInfo)"
    }
}
