package com.cherry.androidcode.entity


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * kt关于实现Parcelable:
 * Kotlin版本为1.1.4或者更高
 * build.gradle中添加支持
 * apply plugin: 'kotlin-android-extensions'
 * androidExtensions {
experimental = true
}
 * 实体类上添加注解@Parcelize
 * 但是Lint检查会报错，所以加上@SuppressLint("ParcelCreator")忽略这个警告错误
 */
@Parcelize
 class CityBean(
        @SerializedName("code")
        val code: Int = 200,
        @SerializedName("name")
        val name: String = ""
) : Parcelable {

    override fun toString(): String {
        return "CityBean(code=$code, name='$name')"
    }
}