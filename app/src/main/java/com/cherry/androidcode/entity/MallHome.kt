package com.cherry.androidcode.entity


import com.google.gson.annotations.SerializedName

data class MallHome(

        //顶部广告
        @SerializedName("ad_banner_data")
        val adBannerData: MutableList<AdBannerData>?,
        //中部广告
        @SerializedName("ad_data")
        val adData: MutableList<AdData>?,
        //底部广告
        @SerializedName("banner_data")
        val bannerData: MutableList<BannerData>?,
        //顶部分类
        @SerializedName("class_data")
        val classData: MutableList<ClassData>?,
        @SerializedName("hot_product_data")
        val hotProductData: MutableList<HotProductData>?,
        @SerializedName("product_data")
        val productData: MutableList<ProductData>?,
        @SerializedName("store_data")
        val storeData: MutableList<StoreData>?
) {

    data class AdBannerData(
            @SerializedName("id")
            val id: String,
            @SerializedName("img_url")
            val imgUrl: String,
            @SerializedName("put_platform")
            val putPlatform: String,
            @SerializedName("target_id")
            val targetId: String,
            @SerializedName("target_type")
            val targetType: String,
            @SerializedName("target_url")
            val targetUrl: String,
            @SerializedName("title")
            val title: String,
            @SerializedName("type_id")
            val typeId: String
    )

    data class ClassData(
            @SerializedName("id")
            val id: String,
            @SerializedName("img_url")
            val imgUrl: String,
            @SerializedName("put_platform")
            val putPlatform: String,
            @SerializedName("target_id")
            val targetId: String,
            @SerializedName("target_type")
            val targetType: String,
            @SerializedName("target_url")
            val targetUrl: String,
            @SerializedName("third_type")
            val thirdType: String,
            @SerializedName("title")
            val title: String,
            @SerializedName("type")
            val type: String,
            @SerializedName("type_id")
            val typeId: String
    )

    data class HotProductData(
            @SerializedName("create_type")
            val createType: String,
            @SerializedName("desc")
            val desc: String,
            @SerializedName("detail")
            val detail: String,
            @SerializedName("fraction_price")
            val fractionPrice: String,
            @SerializedName("fractionname")
            val fractionname: String,
            @SerializedName("group_number")
            val groupNumber: Int,
            @SerializedName("group_price")
            val groupPrice: String,
            @SerializedName("id")
            val id: String,
            @SerializedName("image")
            val image: String,
            @SerializedName("is_fraction")
            val isFraction: Int,
            @SerializedName("is_group")
            val isGroup: String,
            @SerializedName("is_online")
            val isOnline: String,
            @SerializedName("is_show")
            val isShow: String,
            @SerializedName("market_price")
            val marketPrice: String,
            @SerializedName("name")
            val name: String,
            @SerializedName("popularity")
            val popularity: String,
            @SerializedName("put_platform")
            val putPlatform: String,
            @SerializedName("redpacket_price")
            val redpacketPrice: String,
            @SerializedName("show_fraction")
            val showFraction: Int,
            @SerializedName("store_id")
            val storeId: String,
            @SerializedName("third_type")
            val thirdType: String,
            @SerializedName("user_data")
            val userData: List<UserData>
    )

    data class StoreData(
            @SerializedName("id")
            val id: String,
            @SerializedName("logo")
            val logo: String,
            @SerializedName("popularity")
            val popularity: String,
            @SerializedName("put_platform")
            val putPlatform: String,
            @SerializedName("store_name")
            val storeName: String
    )

    data class AdData(
            @SerializedName("id")
            val id: String,
            @SerializedName("img_url")
            val imgUrl: String,
            @SerializedName("put_platform")
            val putPlatform: String,
            @SerializedName("target_id")
            val targetId: String,
            @SerializedName("target_type")
            val targetType: String,
            @SerializedName("target_url")
            val targetUrl: String,
            @SerializedName("third_type")
            val thirdType: String,
            @SerializedName("title")
            val title: String,
            @SerializedName("title_img_url")
            val titleImgUrl: String,
            @SerializedName("type")
            val type: String,
            @SerializedName("type_id")
            val typeId: String
    )

    data class BannerData(
            @SerializedName("id")
            val id: String,
            @SerializedName("img_url")
            val imgUrl: String,
            @SerializedName("put_platform")
            val putPlatform: String,
            @SerializedName("target_id")
            val targetId: String,
            @SerializedName("target_type")
            val targetType: String,
            @SerializedName("target_url")
            val targetUrl: String,
            @SerializedName("title")
            val title: String,
            @SerializedName("type_id")
            val typeId: String
    )

    data class ProductData(
            @SerializedName("desc")
            val desc: String,
            @SerializedName("detail")
            val detail: String,
            @SerializedName("fraction_price")
            val fractionPrice: String,
            @SerializedName("fractionname")
            val fractionname: String,
            @SerializedName("group_number")
            val groupNumber: Int,
            @SerializedName("group_price")
            val groupPrice: String,
            @SerializedName("id")
            val id: String,
            @SerializedName("image")
            val image: String,
            @SerializedName("is_fraction")
            val isFraction: Int,
            @SerializedName("is_group")
            val isGroup: String,
            @SerializedName("is_online")
            val isOnline: String,
            @SerializedName("is_show")
            val isShow: String,
            @SerializedName("market_price")
            val marketPrice: String,
            @SerializedName("name")
            val name: String,
            @SerializedName("popularity")
            val popularity: String,
            @SerializedName("redpacket_price")
            val redpacketPrice: String,
            @SerializedName("show_fraction")
            val showFraction: Int,
            @SerializedName("store_id")
            val storeId: String,
            @SerializedName("third_type")
            val thirdType: String,
            @SerializedName("user_data")
            val userData: List<UserData>
    )

    data class UserData(
            @SerializedName("avatar")
            val avatar: String
    )
}