package com.cherry.androidcode.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * @author DongMS
 * @date 2019/6/19
 */
data class HttpResult<T>(@SerializedName("errorCode") val status: Int,
                         @SerializedName("errorMsg") val msg: String,
                         val data: T) : Serializable {
    companion object {
        private const val serialVersionUID = 5213230387175987834L
    }

}