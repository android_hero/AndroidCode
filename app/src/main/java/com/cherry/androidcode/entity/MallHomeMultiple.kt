package com.cherry.androidcode.entity

/**
 * @author DongMS
 * @since 2019/7/16
 */
//这里注意所有的itemHolder数据都是数组，所以这样写包装更方便，但如果出现其中一个itemHolder不是数组，在组装数据时，再包装一层数组，取值为索引值0
data class MallHomeMultiple<T>(val list: MutableList<T>?, val multipleType: Int) {
    companion object {
        const val TOP_AD = 0
        const val CENTER_AD = 1
        const val BOTTOM_AD = 2
        const val CLASS_PRODUCT = 3
        const val HOT_PRODUCT = 4
        const val STORE = 5
        const val NEW_PRODUCT = 6
    }
}