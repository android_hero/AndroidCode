package com.cherry.androidcode.entity

import java.io.Serializable

data class SimpleResponse(private val code: Int, private val msg: String) : Serializable {

    fun toHttpResponse() = HttpResult(code, msg, Void.TYPE.newInstance())

    companion object {
        private const val serialVersionUID = -1477609349345966116L
    }
}
