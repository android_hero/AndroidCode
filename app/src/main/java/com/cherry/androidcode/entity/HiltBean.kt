package com.cherry.androidcode.entity

import android.app.Activity
import android.app.Application
import android.content.Context
import android.util.Log
import com.cherry.androidcode.module.BindElectricEngine
import com.cherry.androidcode.module.BindGasEngine
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

//无参构造
class Truck1 @Inject constructor() {
    fun deliver() {
        Log.w("----", "Truck1 is delivering cargo.")
    }
}

//有参构造
class Truck2 @Inject constructor(private val driver: Driver) {

    fun deliver() {
        Log.w("----", "Truck2 is delivering cargo.driver by $driver")
    }
}


class Driver @Inject constructor()


//预置Qualifier
//注意：这里作用域范围要小于或者等于@ApplicationContext、@ActivityContext，否则编译不通过
//这样就限定在范围内只存在一个实例
@Singleton
//@ApplicationContext 提供传入Context参数
class Test1 @Inject constructor(@ApplicationContext context: Context)

//预置Qualifier直接注入
class Test2 @Inject constructor(application: Application)

//预置Qualifier直接注入
class Test3 @Inject constructor(activity: Activity)


//接口注入
class Truck3 @Inject constructor() {

    @Inject
    lateinit var engine: Engine

    fun deliver() {
        engine.start()
        Log.w("----", "Truck3 is delivering cargo.")
        engine.shutdown()
    }
}

//注入同一个接口的不同实现类
class Truck4 @Inject constructor() {

    @BindGasEngine
    @Inject
    lateinit var gasEngine: Engine

    @BindElectricEngine
    @Inject
    lateinit var electricEngine: Engine


    fun deliver() {
        gasEngine.start()
        electricEngine.start()
        Log.w("----", "Truck4 is delivering cargo.")
        gasEngine.shutdown()
        electricEngine.shutdown()
    }
}


interface Engine {
    fun start()
    fun shutdown()
}

class GasEngine @Inject constructor() : Engine {
    override fun start() {
        Log.w("----", "Gas engine start.")
    }

    override fun shutdown() {
        Log.w("----", "Gas  engine shutdown.")
    }
}

class ElectricEngine @Inject constructor() : Engine {
    override fun start() {
        Log.w("----", "Electric  engine start.")
    }

    override fun shutdown() {
        Log.w("----", "Electric  engine shutdown.")
    }
}
