package com.ilaisa.laishang.wxapi

import android.annotation.SuppressLint
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.cherry.androidcode.AppConstant.WX_APP_ID
import com.cherry.androidcode.AppConstant.WX_APP_SECRET
import com.google.gson.Gson
import com.ilaisa.laishang.WxException
import com.ilaisa.laishang.WxUserInfo
import com.lzy.okgo.OkGo
import com.lzy.okgo.convert.StringConvert
import com.lzy.okrx2.adapter.ObservableBody
import com.tencent.mm.opensdk.constants.ConstantsAPI
import com.tencent.mm.opensdk.modelbase.BaseReq
import com.tencent.mm.opensdk.modelbase.BaseResp
import com.tencent.mm.opensdk.modelmsg.SendAuth
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler
import com.tencent.mm.opensdk.openapi.WXAPIFactory
import com.umeng.socialize.weixin.view.WXCallbackActivity
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject

/**
 * @author DongMS
 * @since 2020/4/9
 */
//不能改成AppCompatActivity
class WXEntryActivity :WXCallbackActivity()

//    : Activity(), IWXAPIEventHandler {
//
//    private val wxapi by lazy {
//        // 通过WXAPIFactory工厂，获取IWXAPI的实例，第三个参数是否检查签名
//        WXAPIFactory.createWXAPI(this, WX_APP_ID, true)
//    }
//
//    private val wxBroadcastReceiver by lazy {
//        object : BroadcastReceiver() {
//            override fun onReceive(context: Context?, intent: Intent?) {
//                // 将该app注册到微信
//                wxapi.registerApp(WX_APP_ID)
//            }
//        }
//    }
//
//    private val compositeDisposable: CompositeDisposable by lazy {
//        CompositeDisposable()
//    }
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        regToWx()
//        wxapi.handleIntent(intent, this)
//    }
//
//    override fun onNewIntent(intent: Intent?) {
//        super.onNewIntent(intent)
//        wxapi.handleIntent(intent, this)
//    }
//
//    private fun regToWx() {
//
//        // 将应用的appId注册到微信
//        wxapi.registerApp(WX_APP_ID)
//
//        //建议动态监听微信启动广播进行注册到微信
//        registerReceiver(wxBroadcastReceiver, IntentFilter(ConstantsAPI.ACTION_REFRESH_WXAPP))
//    }
//
//    // 微信发送请求到第三方应用时，会回调到该方法
//    override fun onReq(p0: BaseReq) {
//
//    }
//
//    //app发送消息给微信，处理返回消息的回调
//    override fun onResp(res: BaseResp) {
//        when (res.type) {
//            ConstantsAPI.COMMAND_SENDMESSAGE_TO_WX -> {
//                //分享，开发者将无法获知用户是否分享完成
//                finish()
//            }
//            ConstantsAPI.COMMAND_SENDAUTH -> {
//                //登录
//                if (res is SendAuth.Resp) {
//                    handlerLogin(res)
//                }
//            }
//            else -> {
//                //nothing
//            }
//        }
//    }
//
//
//    private fun handlerLogin(sendAuth: SendAuth.Resp) {
//        when (sendAuth.errCode) {
//            BaseResp.ErrCode.ERR_OK -> {
//                authLogin(sendAuth)
//            }
//            BaseResp.ErrCode.ERR_USER_CANCEL -> {
//                Toast.makeText(applicationContext, "用户取消授权登录", Toast.LENGTH_SHORT).show()
//                finish()
//            }
//            BaseResp.ErrCode.ERR_COMM -> {
//                Log.e("wechat", "微信授权失败原因：${sendAuth.errStr}")
//                Toast.makeText(applicationContext, "用户授权登录失败", Toast.LENGTH_SHORT).show()
//                finish()
//            }
//            BaseResp.ErrCode.ERR_AUTH_DENIED -> {
//                Toast.makeText(applicationContext, "用户拒绝授权登录", Toast.LENGTH_SHORT).show()
//                finish()
//            }
//            else -> {
//            }
//        }
//    }
//
//
//    /**
//     * Appsecret 是应用接口使用密钥，泄漏后将可能导致应用数据泄漏、应用的用户数据泄漏等高风险后果；存储在客户端，极有可能被恶意窃取（如反编译获取Appsecret）；
//    access_token 为用户授权第三方应用发起接口调用的凭证（相当于用户登录态），存储在客户端，可能出现恶意获取access_token 后导致的用户数据泄漏、用户微信相关接口功能被恶意发起等行为；
//    refresh_token 为用户授权第三方应用的长效凭证，仅用于刷新access_token，但泄漏后相当于access_token 泄漏，风险同上
//     *
//     *
//     * https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code
//     * 正确返回
//     * {
//    "access_token": "ACCESS_TOKEN",
//    "expires_in": 7200,
//    "refresh_token": "REFRESH_TOKEN",
//    "openid": "OPENID",
//    "scope": "SCOPE",
//    "unionid": "o6_bmasdasdsad6_2sgVt7hMZOPfL"
//    }
//
//    https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID
//    {
//    "openid": "OPENID",
//    "nickname": "NICKNAME",
//    "sex": 1,
//    "province": "PROVINCE",
//    "city": "CITY",
//    "country": "COUNTRY",
//    "headimgurl": "http://wx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4eMsv84eavHiaiceqxibJxCfHe/0",
//    "privilege": ["PRIVILEGE1", "PRIVILEGE2"],
//    "unionid": " o6_bmasdasdsad6_2sgVt7hMZOPfL"
//    }
//     */
//    @SuppressLint("CheckResult")
//    private fun authLogin(sendAuth: SendAuth.Resp) {
//        //1.通过 code 获取 access_token 和openid
//        //拼装url https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code
//        OkGo.get<String>("https://api.weixin.qq.com/sns/oauth2/access_token?appid=${WX_APP_ID}&secret=${WX_APP_SECRET}&code=${sendAuth.code}&grant_type=authorization_code")
//                .converter(object : StringConvert() {})
//                .adapt(ObservableBody<String>())
//                .flatMap {
//                    val jsonParser = JSONObject(it)
//                    val errorCode = jsonParser.optInt("errcode", 0)
//                    if (errorCode == 0) {
//                        //成功了
//                        //2.获取用户个人信息（UnionID 机制）
//                        //拼装GET https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID
//                        OkGo.get<String>("https://api.weixin.qq.com/sns/userinfo?access_token=${jsonParser.optString("access_token")}&openid=${jsonParser.optString("openid")}")
//                                .converter(object : StringConvert() {})
//                                .adapt(ObservableBody<String>())
//                    } else {
//                        Observable.error<String>(WxException(errorCode, jsonParser.optString("errmsg")))
//                    }
//                }
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(object : Observer<String> {
//                    override fun onComplete() {
//                        finish()
//                    }
//
//                    override fun onSubscribe(d: Disposable) {
//                        compositeDisposable.add(d)
//                    }
//
//                    override fun onNext(t: String) {
//                        val jsonParser = JSONObject(t)
//                        val errorCode = jsonParser.optInt("errcode", 0)
//                        if (errorCode == 0) {
//                            getWxUserInfo(t)
//                        } else {
//                            throw  WxException(errorCode, jsonParser.optString("errmsg"))
//                        }
//                    }
//
//                    override fun onError(e: Throwable) {
//                        when (e) {
//                            is WxException -> {
//                                Toast.makeText(applicationContext, e.errorMsg, Toast.LENGTH_SHORT).show()
//                            }
//                            else -> {
//                            }
//                        }
//                    }
//                })
//
//    }
//
//    private fun getWxUserInfo(s: String) {
//        val userInfo = Gson().fromJson(s, WxUserInfo::class.java)
////        unionid	用户统一标识。针对一个微信开放平台帐号下的应用，同一用户的 unionid 是唯一的。
//        //传openid和unionid给后台
//        Log.e("---微信用户信息", userInfo.toString())
//    }
//
//    override fun onDestroy() {
//        super.onDestroy()
//        unregisterReceiver(wxBroadcastReceiver)
//    }
//}
