package com.cherry.androidcode.demo

/**
 * @author Cherry(dongmiansheng@gmail.com)
 * @since 2019/6/30
 */

data class DemoBean(val titile: String, val clazz: Class<*>)