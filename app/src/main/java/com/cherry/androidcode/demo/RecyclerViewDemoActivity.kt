package com.cherry.androidcode.demo

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cherry.androidcode.R
import com.cherry.androidcode.base.BaseActivity
import com.cherry.androidcode.util.inflate
import com.cherry.androidcode.util.resToColor
import com.parkingwang.android.SwToast
import com.yanyusong.y_divideritemdecoration.Y_Divider
import com.yanyusong.y_divideritemdecoration.Y_DividerBuilder
import com.yanyusong.y_divideritemdecoration.Y_DividerItemDecoration
import kotlinx.android.synthetic.main.layout_recycler_view.*

/**
 * 缓存的是ViewHolder,一个itemView对应一个ViewHolder,ViewHolder的作用是减少findViewById的次数，与itemView的复用没有直接关系
 * 四级缓存，如果有缓存就不会执行onBindViewHolder
 * Scrap当前屏幕可见视图的渲染缓存，根据position缓存；
 * Cache 根据position缓存之前可见成为不可见的视图，例如重新往回滑动，默认缓存两个ViewHolder；
 * ViewCacheExtension:用户自定义的缓存，这样就不用调用onBindViewHolder重新绑定数据了
 * RecycledViewPool:“脏数据”，根据viewType缓存，会调用到onBindViewHolder
 */
class RecyclerViewDemoActivity : BaseActivity() {

    override val activityLayoutRes = R.layout.layout_recycler_view

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.addItemDecoration(object : Y_DividerItemDecoration(this) {
            override fun getDivider(itemPosition: Int): Y_Divider {
                //note 这里的尺寸参数是dp，不是px
                val paddingColor = R.color.transparent.resToColor()
                return Y_DividerBuilder()
                        .setTopSideLine(true, paddingColor, 16f, 0f, 0f)
                        .setLeftSideLine(true, paddingColor, 16f, 0f, 0f)
                        .setRightSideLine(true, paddingColor, 16f, 0f, 0f)
                        .create()
            }
        })
        val list = (1..100).map { "$it" }.toMutableList()
        val adapter = TestAdapter(list)
        recyclerView.adapter = adapter
        adapter.setOnItemClickListener { _, position, item ->
            SwToast.showShort("position->$position,item->$item")
        }

        //优化2：recyclerView中有横向滑动的子RecyclerView，可能会导致页面卡顿
        //设置此方法RecyclerView会prefetch，初次显示横向列表的可见个数，
        // 只适用于LinearLayoutManager，只有嵌套在内部的RecyclerView才会生效
//        linearLayoutManager.initialPrefetchItemCount = 4
        /**
         * if(hasFixedSize){
         *      layoutChildren()
         * }else{
         *      requestLayout()
         * }
         */
        //优化3：当adapter数据变化不会导致recyclerView的大小变化，可以提高效率
        recyclerView.setHasFixedSize(true)

        //优化4：如果存在多个recyclerView,共用同一个RecycledViewPool
        val recycledViewPool = RecyclerView.RecycledViewPool()
        recyclerView.setRecycledViewPool(recycledViewPool)
//        recyclerView1.setRecycledViewPool(recycledViewPool)
//        recyclerViewN.setRecycledViewPool(recycledViewPool)


//        recyclerView.setViewCacheExtension(object : RecyclerView.ViewCacheExtension() {
//            override fun getViewForPositionAndType(recycler: RecyclerView.Recycler, position: Int, type: Int): View? {
//                if (position < 6){
//                    return R.layout.item_test_card_view_simple.inflate(context = this@RecyclerViewDemoActivity)
//                }
//                return null
//            }
//        })
    }

    class TestAdapter(val list: MutableList<String>) : RecyclerView.Adapter<TestViewHolder>() {

        private var listener: ((View, Int, String) -> Unit)? = null

        fun setOnItemClickListener(listener: (View, Int, String) -> Unit) {
            this.listener = listener
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TestViewHolder {
            //这里需要看源码解析RecycledViewPool的默认容量
            Log.e(TAG, "RecycledViewPool缓存ViewHolder到一定容量后，之后整个列表的ViewHolder都从RecycledViewPool中根据ViewType取，不会频繁调用")

            //优化1：减少ItemView的监听事件，这里调用次数少，可以在设置
            val itemView = R.layout.item_test_card_view_simple.inflate(context = parent.context)
            val testViewHolder = TestViewHolder(itemView)
            itemView.setOnClickListener {
                listener?.invoke(it, testViewHolder.layoutPosition, list[testViewHolder.layoutPosition])
            }
            return testViewHolder
        }

        override fun onBindViewHolder(holder: TestViewHolder, position: Int) {
            Log.e(TAG, "$position-------只要position不同，就有脏数据，重新绑定数据")
            holder.bindToData(list[position])

        }

        override fun getItemCount() = list.size

    }

    class TestViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val tvTitle: TextView = itemView.findViewById(R.id.tv_title)

        fun bindToData(item: String) {
            tvTitle.text = item
        }
    }

    companion object {
        private const val TAG = "RecyclerViewDemo"
    }

}
