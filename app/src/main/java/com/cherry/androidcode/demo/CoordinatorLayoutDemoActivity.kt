package com.cherry.androidcode.demo

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.cherry.androidcode.R
import com.cherry.androidcode.base.BaseActivity
import com.cherry.androidcode.util.StatusBarUtil
import com.cherry.androidcode.util.getResource
import kotlinx.android.synthetic.main.activity_demo_coordinator_layout.*


/**
 *
 * AppBarLayout 是一个垂直的 LinearLayout，实现了 Material Design 中 App bar 的 Scrolling Gestures 特性。
 * AppBarLayout 的子 View 应该声明想要具有的“滚动行为”，这可以通过 layout_scrollFlags 属性或是 setScrollFlags() 方法来指定
 * AppBarLayout 只有作为 CoordinatorLayout 的直接子 View 时才能正常工作，
 * 为了让 AppBarLayout 能够知道何时滚动其子 View，我们还应该在 CoordinatorLayout 布局中提供一个可滚动 View，我们称之为 Scrolling View。
 * Scrolling View 和 AppBarLayout 之间的关联，通过将 Scrolling View 的 Behavior 设为 AppBarLayout.ScrollingViewBehavior 来建立。
 *
 * 设置沉浸式状态栏总结：
 * 1.布局：
 * CollapsingToolbarLayout->app:statusBarScrim="@android:color/transparent"
 * 实现沉浸式的控件(ImageView)设置上android:fitsSystemWindows="true"，如果没有设置，则子布局会位于状态栏下方，未延伸至状态栏
 * 2.StatusBarUtil.setStatusBarColor(this, Color.TRANSPARENT)
 * StatusBarUtil.addMarginTopEqualStatusBarHeight(toolbar)
 */

class CoordinatorLayoutDemoActivity : BaseActivity() {

    override val activityLayoutRes = R.layout.activity_demo_coordinator_layout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        StatusBarUtil.setStatusBarColor(this, Color.TRANSPARENT)
        StatusBarUtil.addMarginTopEqualStatusBarHeight(toolbar)
        setSupportActionBar(toolbar)
        collapsingToolbarLayout.setCollapsedTitleTextColor(Color.RED)
        collapsingToolbarLayout.setExpandedTitleColor(Color.GREEN)

        btn_dependency.setOnTouchListener { view, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_MOVE) {
                //RawX,RawY 相对于屏幕位置坐标,表示相对于屏幕左上角的x坐标值(注意:这个屏幕左上角是手机屏幕左上角,不管activity是否有titleBar或是否全屏幕)
                //X,Y 相对于容器的位置坐标,表示Widget相对于自身左上角的x坐标
                view.x = motionEvent.rawX
                view.y = motionEvent.rawY
            }
            true
        }
    }


    /**
     * Dependency是主动调用者(被观察者)，Child受Dependency影响的被动调用者（观察者）
     * 给Dependency的View设置，泛型就是Child
     * 因为是在XML中使用app:layout_behavior定义静态的这种行为,
     * 必须实现一个构造函数使布局的效果能够正常工作，内部类注意要静态内部类。
     * 否则 Could not inflate Behavior subclass error messages.
     *
     */
    class FollowBehavior(context: Context, attrs: AttributeSet) : CoordinatorLayout.Behavior<ImageView>(context, attrs) {

        /**
         * 判断child的布局是否依赖dependency
         */
        override fun layoutDependsOn(parent: CoordinatorLayout, child: ImageView, dependency: View): Boolean {
            //返回false表示child不依赖dependency，true表示依赖
            //测试
            return child.id == R.id.iv_child
        }

        /**
         * 当dependency发生改变时（位置、宽高等），执行这个函数
         */
        override fun onDependentViewChanged(parent: CoordinatorLayout, child: ImageView, dependency: View): Boolean {
            //返回true表示child的位置或者是宽高要发生改变，否则就返回false
            changePosition(child, dependency)
            return true
        }

        private fun changePosition(child: ImageView, dependency: View) {
            child.x = getResource().displayMetrics.widthPixels - dependency.x - dependency.width
            child.y = dependency.y
        }

    }

}