package com.cherry.androidcode.demo

import android.os.Bundle
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.cherry.androidcode.R
import com.cherry.androidcode.base.BaseActivity

/**
 * @author DongMS
 * @since 2019/11/12
 */
class ConstraintFlexboxDemoActivity : BaseActivity() {

//    override val activityLayoutRes = R.layout.activity_constraint_layout_demo

//    override val activityLayoutRes: Int = R.layout.activity_flex_box_demo

//    override val activityLayoutRes = R.layout.activity_constraint_layout_2_demo

    override val activityLayoutRes = R.layout.activity_constraint_layout_3_demo

    private lateinit var testState: ConstraintLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        testState = findViewById<ConstraintLayout>(R.id.cl_test_state).apply {
//            loadLayoutDescription(R.xml.constraint_state_test)
//            setState(R.id.loading, 0, 0)
//        }

    }

//    fun loadSuccess(view: View) {
//        testState.setState(R.id.success, 0, 0)
//    }
//
//    fun loadError(view: View) {
//        testState.setState(R.id.error, 0, 0)
//    }
}