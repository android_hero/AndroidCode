package com.cherry.androidcode.demo

import android.animation.Keyframe
import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.animation.AnticipateOvershootInterpolator
import androidx.constraintlayout.widget.ConstraintHelper
import androidx.constraintlayout.widget.ConstraintLayout

class TestAnimationHelper @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) :
    ConstraintHelper(context, attrs, defStyleAttr) {

    //onLayout()后,getViews()方法获取所引用的所有view
    override fun updatePostLayout(container: ConstraintLayout) {
        super.updatePostLayout(container)
        val views = getViews(container)
        views.forEach {
            startAnimation(it)
        }
    }

    private fun startAnimation(view: View) {
        val translationY = -100f


        view.translationY = translationY
        val translationYHolder = PropertyValuesHolder.ofFloat(
            View.TRANSLATION_Y,
            translationY,
            0f
        )

        val keyFrame1 =
            Keyframe.ofFloat(0f, -6f)
        val keyFrame2 =
            Keyframe.ofFloat(0.6f, 25f)
        val keyFrame3 =
            Keyframe.ofFloat(1f, 0f)
        val rotateHolder =
            PropertyValuesHolder.ofKeyframe(View.ROTATION, keyFrame1, keyFrame2, keyFrame3)

        ObjectAnimator.ofPropertyValuesHolder(
            view,
            translationYHolder,
            rotateHolder
        )
            .apply { interpolator = AnticipateOvershootInterpolator() }
            .setDuration(600L)
            .start()
    }

}