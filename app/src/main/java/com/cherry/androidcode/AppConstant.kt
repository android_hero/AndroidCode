package com.cherry.androidcode

/**
 * @author DongMS
 * @date 2019/6/24
 */
object AppConstant {
    const val PAGE_START_INDEX = 1
    const val LOAD_PER_PAGE = 20

    const val QILING6080_URL = "qiling6080_url"
    const val YIFANG_URL = "yifang_url"
    const val CAOMING_URL = "caoming_url"
    const val DY_YIFANG_URL = "dy_yifang_url"

    const val WX_APP_ID = ""
    const val WX_APP_SECRET = ""
}