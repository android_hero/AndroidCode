package com.ilaisa.laishang

import com.google.gson.annotations.SerializedName

/**
 * @author DongMS
 * @since 2020/4/10
 */
data class OrderPayResult(
        val status: Int,
        val info: String,
        @SerializedName("data")
        val orderPay: OrderPay?
) {

    data class OrderPay(val type: String,
                        val platform: String,
                        @SerializedName("pay_success")
                        val paySuccessUrl: String,
                        @SerializedName("pay_error")
                        val payErrorUrl: String,
                        val content: OrderPayReq?,
                        @SerializedName("order_no")
                        val orderNo: String)

}