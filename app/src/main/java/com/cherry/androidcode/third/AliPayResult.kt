package com.ilaisa.laishang

import android.text.TextUtils
import com.cherry.androidcode.util.getALiParams

/**
 * @author DongMS
 * @since 2020/4/10
 */
class AliPayResult(payResult: Map<String, String>?) {

    var resultStatus: String? = null
    var result: String? = null
    var memo: String? = null
    var paySuccess = false

    init {
        payResult?.let {
            for ((k, v) in payResult) {
                when (k) {
                    "resultStatus" -> resultStatus = v
                    "result" -> result = v
                    "memo" -> memo = v
                }
            }
        }
        //在resultStatus=9000，并且success="true"以及sign="xxx"校验通过的情况下，证明支付成功，其它情况归为失败。较低安全级别的场合，也可以只通过检查resultStatus以及success="true"来判定支付结果
        var isSuccess = false
        if (!TextUtils.isEmpty(result)) {
            isSuccess = result!!.split("&").find {
                it.startsWith("success")
            }?.getALiParams("success=")?.toBoolean() ?: false
        }
        paySuccess = "9000" == resultStatus && isSuccess
    }


    override fun toString(): String {
        return "AliPayResult(resultStatus=$resultStatus, result=$result, memo=$memo)"
    }

}