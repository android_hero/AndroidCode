package com.ilaisa.laishang
import com.google.gson.annotations.SerializedName


/**
 * @author DongMS
 * @since 2020/4/9
 */
data class WxUserInfo(
    @SerializedName("city")
    val city: String,
    @SerializedName("country")
    val country: String,
    @SerializedName("headimgurl")
    val headimgurl: String,
    @SerializedName("nickname")
    val nickname: String,
    @SerializedName("openid")
    val openid: String,
    @SerializedName("privilege")
    val privilege: List<String>,
    @SerializedName("province")
    val province: String,
    @SerializedName("sex")
    val sex: Int,
    @SerializedName("unionid")
    val unionid: String
){
    override fun toString(): String {
        return "WxUserInfo(city='$city', country='$country', headimgurl='$headimgurl', nickname='$nickname', openid='$openid', privilege=$privilege, province='$province', sex=$sex, unionid='$unionid')"
    }
}