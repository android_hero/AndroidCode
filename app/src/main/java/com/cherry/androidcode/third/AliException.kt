package com.ilaisa.laishang

/**
 * @author DongMS
 * @since 2020/4/9
 */
class AliException(val errorCode: String?, val errorMsg: String?) : Throwable()