package com.ilaisa.laishang

/**
 * @author DongMS
 * @since 2020/4/9
 */
data class WxException(val errorCode: Int, val errorMsg: String) : Throwable()