package com.ilaisa.laishang

import com.google.gson.annotations.SerializedName

/**
 * @author DongMS
 * @since 2020/4/10
 */
data class OrderPayReq(val orderStr: String,
                       @SerializedName("order_no")
                    val orderNo: String,
                       val appid: String,
                       val sign: String,
                       val partnerid: String,
                       val prepayid: String,
                       val noncestr: String,
                       val timestamp: String)