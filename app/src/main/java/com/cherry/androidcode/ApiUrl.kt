package com.cherry.androidcode

/**
 * @author DongMS
 * @date 2019/6/25
 */
object ApiUrl {
    //post :"k":"..."
    const val SEARCH_URL = "https://www.wanandroid.com/article/query/0/json"
    //get:list
    const val TEST_LIST_URL = "http://www.mocky.io/v2/5d103d2e3000006f004c9e80"
    //get:map
    const val TEST_MAP_URL = "http://www.mocky.io/v2/5d103f73300000a6034c9e87"
    //get
    const val GANK_TODAY_URL = "http://gank.io/api/today"
    //get
    const val GANK_MEIZI_URL = "http://gank.io/api/data/福利/${AppConstant.LOAD_PER_PAGE}/"
    //get mallHome
    const val MALL_HOME_URL = "http://www.mocky.io/v2/5d2da0122e00004e00c57f10"

    //金山词霸,http://fy.iciba.com/ajax.php?a=fy&f=auto&t=auto&w=hello%20world
    // 参数说明：
// a：固定值 fy
// f：原文内容类型，日语取 ja，中文取 zh，英语取 en，韩语取 ko，德语取 de，西班牙语取 es，法语取 fr，自动则取 auto
// t：译文内容类型，日语取 ja，中文取 zh，英语取 en，韩语取 ko，德语取 de，西班牙语取 es，法语取 fr，自动则取 auto
// w：查询内容
    const val GET_CIBA_URL = "http://fy.iciba.com/ajax.php"

    //http://fanyi.youdao.com/translate?doctype=json&jsonversion=&type=&keyfrom=&model=&mid=&imei=&vendor=&screen=&ssid=&network=&abtest=
    // 参数说明
// doctype：json 或 xml
// jsonversion：如果 doctype 值是 xml，则去除该值，若 doctype 值是 json，该值为空即可
// xmlVersion：如果 doctype 值是 json，则去除该值，若 doctype 值是 xml，该值为空即可
// type：语言自动检测时为 null，为 null 时可为空。英译中为 EN2ZH_CN，中译英为 ZH_CN2EN，日译中为 JA2ZH_CN，中译日为 ZH_CN2JA，韩译中为 KR2ZH_CN，中译韩为 ZH_CN2KR，中译法为 ZH_CN2FR，法译中为 FR2ZH_CN
// keyform：mdict. + 版本号 + .手机平台。可为空
// model：手机型号。可为空
// mid：平台版本。可为空
// imei：???。可为空
// vendor：应用下载平台。可为空
// screen：屏幕宽高。可为空
// ssid：用户名。可为空
// abtest：???。可为空
// 请求体：i
// 请求格式：x-www-form-urlencoded
    const val POST_YOUDAO_URL = "http://fanyi.youdao.com/translate"
    //视频网站地址
    const val VIDEO_BASE_URL = "https://gitee.com/android_hero/MyFile/raw/master/jav/config.json"
}