package com.cherry.androidcode

import android.content.Intent
import android.os.Bundle
import com.cherry.androidcode.base.BaseActivity
import com.cherry.androidcode.main1.MainActivity
import com.lzy.okgo.OkGo
import com.lzy.okgo.callback.StringCallback
import com.lzy.okgo.model.Response
import com.tencent.mmkv.MMKV
import org.json.JSONObject

/**
 * @author Cherry(dongmiansheng@gmail.com)
 * @since 2019/12/7
 */

class SplashActivity : BaseActivity() {

    override val activityLayoutRes = R.layout.activity_splash

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //先不处理了
        OkGo.get<String>(ApiUrl.VIDEO_BASE_URL)
            .headers(
                "User-Agent",
                "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Mobile Safari/537.36"
            )
            .execute(object : StringCallback() {
                override fun onSuccess(response: Response<String>?) {
                    response?.let {
                        val jsonParse = JSONObject(response.body())
                        MMKV.defaultMMKV().run {
                            encode(
                                AppConstant.QILING6080_URL,
                                jsonParse.optString("qiling6080_url")
                            )
                            encode(AppConstant.YIFANG_URL, jsonParse.optString("yifang_url"))
                            encode(AppConstant.CAOMING_URL, jsonParse.optString("caoming_url"))
                            encode(AppConstant.DY_YIFANG_URL, jsonParse.optString("dy_yifang_url"))

                        }
                        startActivity(Intent(this@SplashActivity, MainActivity::class.java))
                        finish()
                    }
                }
            })
    }

}