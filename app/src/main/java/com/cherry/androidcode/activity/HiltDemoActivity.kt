package com.cherry.androidcode.activity

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.cherry.androidcode.entity.*
import com.cherry.androidcode.jetpack.viewmodel.HiltViewModel
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Inject

//Hilt一共支持6个入口点，分别是：
//
//Application
//Activity
//Fragment
//View
//Service
//BroadcastReceiver

//@InstallIn 组件作用域 ApplicationComponent、ActivityComponent、ActivityRetainedComponent(ViewModel)、
// FragmentComponent、ViewComponent、ServiceComponent、ViewWithFragmentComponent

//Hilt会为每次的依赖注入行为都创建不同的实例,@Singleton只创建一个
@AndroidEntryPoint
class HiltDemoActivity : AppCompatActivity() {

    //修饰符不能是private
    @Inject
    lateinit var truck1: Truck1

    @Inject
    lateinit var truck2: Truck2

    @Inject
    lateinit var truck3: Truck3

    @Inject
    lateinit var truck4: Truck4

    @Inject
    lateinit var okHttpClient: OkHttpClient

    @Inject
    lateinit var retrofit: Retrofit

    @Inject
    lateinit var test1: Test1

    @Inject
    lateinit var test11: Test1

    @Inject
    lateinit var test2: Test2

    @Inject
    lateinit var test3: Test3


    val hiltViewModel by viewModels<HiltViewModel>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        truck1.deliver()

        truck2.deliver()

        truck3.deliver()

        truck4.deliver()

        Log.w("------", okHttpClient.connectTimeoutMillis().toString())
        Log.w("------", retrofit.baseUrl().host())

        Log.w("------", test1.toString())
        Log.w("------", test11.toString())
        Log.w("------", test2.toString())
        Log.w("------", test3.toString())
        Log.w("------", hiltViewModel.toString())
    }

}