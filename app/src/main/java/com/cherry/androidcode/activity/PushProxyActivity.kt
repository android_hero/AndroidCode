package com.cherry.androidcode.activity

import android.content.Intent
import android.util.Log
import com.umeng.message.UmengNotifyClickActivity
import org.android.agoo.common.AgooConstants


/**
 *
 * 该Activity需继承自UmengNotifyClickActivity，同时实现父类的onMessage方法，对该方法的intent参数进一步解析即可，该方法异步调用，不阻塞主线程
 *
 * @author DongMS
 * @since 2020-09-21
 */
class PushProxyActivity : UmengNotifyClickActivity() {

    override fun onMessage(intent: Intent) {
        super.onMessage(intent) //此方法必须调用，否则无法统计打开数
        val body = intent.getStringExtra(AgooConstants.MESSAGE_BODY)
//        {"display_type":"notification","extra":{"type":"2","target_id":"2000"},
//        "body":{"after_open":"go_app","ticker":"魅族前台测试999999999","title":"魅族前台测试999999999","play_sound":"true","play_lights":"false","play_vibrate":"false","text":"魅族前台测试99999999"},"msg_id":"umfpear160066808084910"}
        Log.w("push弹窗", body)
    }
}