package com.cherry.androidcode.activity

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.cherry.androidcode.databinding.ActivityUploadFileBinding
import com.cherry.androidcode.upload.UploadExampleViewModel
import com.dylanc.activityresult.launcher.GetMultipleContentsLauncher

class UploadActivity : AppCompatActivity() {

    private lateinit var activityUploadBinding: ActivityUploadFileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activityUploadBinding = DataBindingUtil.setContentView<ActivityUploadFileBinding>(
            this,
            com.cherry.androidcode.R.layout.activity_upload_file
        )

//        val uploadExampleViewModel = ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application))[UploadExampleViewModel::class.java]

        //fragment中使用 by activityViewModels()
        val uploadExampleViewModel by viewModels<UploadExampleViewModel>()

        uploadExampleViewModel.run {
            this.getMultipleContentsLauncher = GetMultipleContentsLauncher(this@UploadActivity)
            activityUploadBinding.uploadHelper = this
            responseLiveData.observe(this@UploadActivity, {
                activityUploadBinding.responseMsg = it
            })
            downloadLiveData.observe(this@UploadActivity, {
                activityUploadBinding.downloadFile = it
            })
            uploadLiveData.observe(this@UploadActivity) {
//                activityUploadBinding.imageUri = it.uri!![0]
                Log.w("----", "${it.uri ?: "null"}------${it.progress}")
            }
        }
    }
}