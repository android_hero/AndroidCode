package com.cherry.androidcode.activity

import android.util.Log
import com.cherry.androidcode.base.BaseTitleBarActivity
import com.cherry.androidcode.entity.CityBean
import com.cherry.androidcode.entity.UserInfo
import com.cherry.androidcode.helper.DrawableHelper
import com.cherry.androidcode.util.decodeEnum
import com.cherry.androidcode.util.decodeList
import com.cherry.androidcode.util.encode
import com.cherry.androidcode.widget.divider.DividerLayout
import com.tencent.mmkv.MMKV
import kotlinx.android.synthetic.main.activity_setting.*
import kotlin.random.Random


/**
 * @author DongMS
 * @date 2019/7/3
 */
class AppSettingActivity : BaseTitleBarActivity() {
    override val titleText = com.cherry.androidcode.R.string.app_name
    override val contentLayoutRes = com.cherry.androidcode.R.layout.activity_setting

    override fun initView() {
        super.initView()
        DrawableHelper.baseDivider(clear_config) {
            it.align = DividerLayout.ALIGN_PARENT_TOP
        }

//        testImportSharedPreferences()
//        writeReadBySpKt()

//        writeReadByMMKV()
    }

    /**
     * 旧项目SharePreference换成MMKV存储
     */
    private fun testImportSharedPreferences() {
        val defaultMMKV = MMKV.defaultMMKV()
        //就这样迁移了。。。
        defaultMMKV.importFromSharedPreferences(UserInfo.preferences)
        //然后删除原来的数据
        UserInfo.clear()

        //测试下
        Log.e("旧数据", UserInfo.name)
        Log.e("新数据", defaultMMKV.decodeString("name") ?: "")

        //为了兼容旧版本api还能有这种调用，而且不用commit或者apply
//        defaultMMKV.edit().putBoolean("",false)
    }

    private fun writeReadByMMKV() {
        //MMKV说明文档：https://github.com/Tencent/MMKV/wiki/android_setup_cn
        //不同业务需要区别存储，也可以单独创建自己的实例
//        MMKV* mmkv = MMKV.mmkvWithID("MyID");
        //业务需要多进程访问，那么在初始化的时候加上标志位 MMKV.MULTI_PROCESS_MODE
//        MMKV* mmkv = MMKV.mmkvWithID("InterProcessKV", MMKV.MULTI_PROCESS_MODE)

        val defaultMMKV = MMKV.defaultMMKV()

        save_string.setOnClickListener {
            defaultMMKV.encode("string", "Cherry")
            config_string.text = defaultMMKV.decodeString("string")
        }
        //decodeString 默认值null，也可以在重载方法中设置默认值
        config_string.text = defaultMMKV.decodeString("string")

        //int,float,double,long 默认值0,boolean默认false
        save_int.setOnClickListener {
            defaultMMKV.encode("int", 30)
            config_int.text = if (defaultMMKV.decodeInt("int", -1) == -1) "" else "年龄：${defaultMMKV.decodeInt("int")}"
        }
        config_int.text = if (defaultMMKV.decodeInt("int", -1) == -1) "" else "年龄：${defaultMMKV.decodeInt("int")}"

        val gameLevelList = arrayOf(UserInfo.GameLevel.EASY, UserInfo.GameLevel.NORMAL, UserInfo.GameLevel.HARD)
        save_enum.setOnClickListener {
            //todo 这里要有个弹框，具体下一步做
            val clickEnum = Random.nextInt(gameLevelList.size)
            defaultMMKV.encode("enum", gameLevelList[clickEnum])
            config_enum.text = defaultMMKV.decodeEnum<UserInfo.GameLevel>("enum")?.name
        }
        config_enum.text = defaultMMKV.decodeEnum("enum", UserInfo.GameLevel.NORMAL).name

        save_list.setOnClickListener {
            defaultMMKV.encode("list", listOf(CityBean(200, "请求成功"), CityBean(404, "页面找不到")))
            config_list.text = defaultMMKV.decodeList<CityBean>("list").toString()
        }
        config_list.text = defaultMMKV.decodeList<CityBean>("list").toString()

        //注意decodeParcelable空指针的情况
        save_bean.setOnClickListener {
            defaultMMKV.encode("bean", CityBean(200, "请求成功"))
            config_bean.text = defaultMMKV.decodeParcelable("bean", CityBean::class.java)?.name
        }
        config_bean.text = defaultMMKV.decodeParcelable("bean", CityBean::class.java)?.name

        clear_config.setOnClickListener {
            //            MMKV.defaultMMKV().clearMemoryCache()
            //            MMKV.defaultMMKV().removeValueForKey("string")
//            defaultMMKV.removeValuesForKeys(arrayOf("int", "long"))
            defaultMMKV.clearAll()
            finish()
        }
    }

    private fun writeReadBySpKt() {
        save_string.setOnClickListener {
            UserInfo.name = "Cherry"
            config_string.text = UserInfo.name
        }
        config_string.text = UserInfo.name

        save_int.setOnClickListener {
            UserInfo.age = 18
            config_int.text = if (UserInfo.age == -1) "" else "年龄：${UserInfo.age}"
        }
        config_int.text = if (UserInfo.age == -1) "" else "年龄：${UserInfo.age}"

        val gameLevelList = arrayOf(UserInfo.GameLevel.EASY, UserInfo.GameLevel.NORMAL, UserInfo.GameLevel.HARD)
        save_enum.setOnClickListener {
            //todo 这里要有个弹框，具体下一步做
            val clickEnum = Random.nextInt(gameLevelList.size)
            UserInfo.gameLevel = gameLevelList[clickEnum]
            config_enum.text = UserInfo.gameLevel.name
        }
        config_enum.text = UserInfo.gameLevel.name

        save_bean.setOnClickListener {
            UserInfo.city = CityBean(404, "页面找不到")
            config_bean.text = UserInfo.city.name
        }
        config_bean.text = UserInfo.city.name

        save_list.setOnClickListener {
            UserInfo.stringList = arrayListOf("a", "b", "c", "d")
            config_list.text = UserInfo.stringList.toString()
        }
        config_list.text = UserInfo.stringList.toString()

        clear_config.setOnClickListener {
            //如果清除某一项的话，只能手动设置默认值，如UserInfo.name = ""
            UserInfo.clear()
            finish()
        }
    }
}