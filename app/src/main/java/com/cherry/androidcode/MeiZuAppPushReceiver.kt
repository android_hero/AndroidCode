package com.cherry.androidcode

import org.android.agoo.mezu.MeizuPushReceiver

/**
 *
 * 自定义Recevier组件受魅族接入方式限制，必须在包名目录实现一个自定义Recevier，继承自MeizuPushReceiver
 * @author DongMS
 * @since 2020-09-21
 */
class MeiZuAppPushReceiver : MeizuPushReceiver()