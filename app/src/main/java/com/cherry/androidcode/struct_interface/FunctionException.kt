package com.cherry.androidcode.struct_interface

/**
 * @author DongMS
 * @since 2019/11/27
 */
class FunctionException : Exception {
    constructor(message: String?) : super(message)
}