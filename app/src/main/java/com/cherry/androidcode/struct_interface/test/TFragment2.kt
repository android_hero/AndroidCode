package com.cherry.androidcode.struct_interface.test

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import com.cherry.androidcode.struct_interface.FunctionManager
import com.cherry.androidcode.util.dpToPx

/**
 * @author DongMS
 * @since 2019/12/20
 */
class TFragment2 : BFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val linearLayout = LinearLayout(context)
        linearLayout.orientation = LinearLayout.VERTICAL

        val button1 = Button(context)
        button1.text = "点击向Activity通讯返回值"
        button1.setOnClickListener {
            val result = FunctionManager.invokeFuncForResult(METHOD1, String::class.java)
            Log.e("----无参数返回值", result)
        }
        val params1 = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        params1.setMargins(15f.dpToPx(), 15f.dpToPx(), 15f.dpToPx(), 15f.dpToPx())
        linearLayout.addView(button1, params1)

        val button2 = Button(context)
        button2.text = "点击向Activity通讯参数返回值"
        button2.setOnClickListener {
            val result = FunctionManager.invokeFuncForParamsResult(METHOD2, "我是参数", String::class.java)
            Log.e("-----有参数返回值", result)
        }
        val params2 = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        params2.setMargins(15f.dpToPx(), 15f.dpToPx(), 15f.dpToPx(), 15f.dpToPx())
        linearLayout.addView(button2, params2)

        return linearLayout
    }


    companion object {
        //不同fragment同名函数也没关系，已经区分调用了
        const val METHOD1 = "TFragment_method1"
        const val METHOD2 = "TFragment_method2"
    }

}