package com.cherry.androidcode.struct_interface.test

import android.content.Context
import androidx.fragment.app.Fragment
import com.cherry.androidcode.struct_interface.FunctionManager

/**
 * @author DongMS
 * @since 2019/12/20
 */
open class BFragment : Fragment() {

    var functionManager: FunctionManager? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context as? BActivity)?.setFunctionForFragment(tag)
    }

}