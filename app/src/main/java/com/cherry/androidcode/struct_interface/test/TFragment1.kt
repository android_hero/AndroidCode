package com.cherry.androidcode.struct_interface.test

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import com.cherry.androidcode.struct_interface.FunctionManager
import com.cherry.androidcode.util.dpToPx

/**
 * fragment向Activity通讯
 * 1.fragment定义接口
 * 2.Activity实现接口
 * 3.接口与fragment与activity绑定
 * 4.使用
 *
 * @author DongMS
 * @since 2019/12/19
 */
class TFragment1 : BFragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val linearLayout = LinearLayout(context)
        linearLayout.orientation = LinearLayout.VERTICAL

        val button1 = Button(context)
        button1.text = "点击向Activity通讯"
        button1.setOnClickListener {
            FunctionManager.invokeFunc(METHOD1)
        }
        val params1 = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        params1.setMargins(15f.dpToPx(), 15f.dpToPx(), 15f.dpToPx(), 15f.dpToPx())
        linearLayout.addView(button1, params1)

        val button2 = Button(context)
        button2.text = "点击向Activity通讯参数"
        button2.setOnClickListener {
            FunctionManager.invokeFuncForParams(METHOD2, "我是参数")
        }
        val params2 = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        params2.setMargins(15f.dpToPx(), 15f.dpToPx(), 15f.dpToPx(), 15f.dpToPx())
        linearLayout.addView(button2, params2)

        return linearLayout
    }


    companion object {
        const val METHOD1 = "TFragment_method1"
        const val METHOD2 = "TFragment_method2"
    }
}