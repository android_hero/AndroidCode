package com.cherry.androidcode.struct_interface

/**
 * @author DongMS
 * @since 2019/11/27
 */
abstract class FunctionNoParamNoResult(functionName: String) : Function(functionName) {

    abstract fun function()
}