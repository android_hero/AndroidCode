package com.cherry.androidcode.struct_interface.test

import android.os.Bundle
import com.cherry.androidcode.R
import com.cherry.androidcode.struct_interface.*
import com.parkingwang.android.SwToast

/**
 * @author DongMS
 * @since 2019/12/19
 */
class TActivity : BActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment_test)
    }

    override fun addInterfaceFunction(): FunctionManager? {
        return FunctionManager.addFunction(object : FunctionNoParamNoResult(TFragment1.METHOD1) {
            override fun function() {
                SwToast.showShort("无参无返回值的回调-----")
            }
        }).addFunction(object : FunctionWithParamNoResult<String>(TFragment1.METHOD2) {
            override fun function(param: String) {
                SwToast.showShort("有参=$param-------无返回值")
            }
        }).addFunction(object : FunctionNoParamWithResult<String>(TFragment2.METHOD1) {
            override fun function(): String {
                SwToast.showShort("无参----返回值的回调")
                return "TFragment_method1"
            }
        }).addFunction(object : FunctionWithParamWithResult<String, String>(TFragment2.METHOD2) {
            override fun function(param: String): String {
                SwToast.showShort("有参=$param-------有返回值")
                return "TFragment_method2"
            }
        })
    }

}