package com.cherry.androidcode.struct_interface.test

import androidx.appcompat.app.AppCompatActivity
import com.cherry.androidcode.struct_interface.FunctionManager

/**
 * @author DongMS
 * @since 2019/12/20
 */
abstract class BActivity : AppCompatActivity() {

    fun setFunctionForFragment(tag: String?) {
        val fragment = supportFragmentManager.findFragmentByTag(tag) as? BFragment
        fragment?.functionManager = addInterfaceFunction()
    }

    abstract fun addInterfaceFunction(): FunctionManager?
}