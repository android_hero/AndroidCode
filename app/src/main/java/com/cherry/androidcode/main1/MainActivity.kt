package com.cherry.androidcode.main1

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.SparseArray
import com.cherry.androidcode.BuildConfig
import com.cherry.androidcode.R
import com.cherry.androidcode.base.BaseActivity
import com.cherry.androidcode.helper.DrawableHelper
import com.cherry.androidcode.listener.SimpleTabSelectedListener
import com.cherry.androidcode.main.Fragment2
import com.cherry.androidcode.main.MainFragment
import com.cherry.androidcode.main.MeFragment
import com.cherry.androidcode.util.resToColor
import com.cherry.androidcode.video.VideoListFragment
import com.cherry.androidcode.widget.DrawableTextView
import com.google.android.material.tabs.TabLayout
import q.rorbin.badgeview.Badge
import q.rorbin.badgeview.QBadgeView


class MainActivity : BaseActivity() {

    private lateinit var tabLayout: TabLayout

    private val homeFragment = MainFragment.newInstance()
    private val videoFragment = VideoListFragment.newInstance()
    private val lazyLoadFragment = Fragment2.newInstance("懒加载")
    private val myFragment = MeFragment.newInstance()

    //如果有多个，就维护一个缓存集合
    private val badgeArray = SparseArray<Badge>()

    private val fragmentList by lazy {
        if (BuildConfig.isVideo) {
            listOf(videoFragment, homeFragment, lazyLoadFragment, myFragment)
        } else {
//            listOf(homeFragment, videoFragment, lazyLoadFragment, myFragment)
            listOf(lazyLoadFragment, videoFragment, homeFragment, myFragment)
        }
    }

    private var selectPosition = 0

    override val activityLayoutRes = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tabLayout = findViewById(R.id.tab_layout)
        initTabs()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.fragment_container, fragmentList[selectPosition])
        transaction.commitAllowingStateLoss()
    }

    private fun initTabs() {
        if (BuildConfig.isVideo) {
            tabLayout.addTab(newTab(DrawableHelper.tabDrawable(), "视频"), true)
            tabLayout.addTab(newTab(DrawableHelper.tabDrawable(), "主页"))
        } else {
            tabLayout.addTab(newTab(DrawableHelper.tabDrawable(), "主页"), true)
            tabLayout.addTab(newTab(DrawableHelper.tabDrawable(), "视频"))
        }

        tabLayout.addTab(newTab(DrawableHelper.tabDrawable(), "懒加载"))
        tabLayout.addTab(newTab(DrawableHelper.tabDrawable(), "我的"))
        tabLayout.addOnTabSelectedListener(object : SimpleTabSelectedListener() {
            override fun onTabSelected(tab: TabLayout.Tab) {
                val transaction = supportFragmentManager.beginTransaction()
                transaction.hide(fragmentList[selectPosition])
                selectPosition = tab.position
                if (supportFragmentManager.fragments.contains(fragmentList[selectPosition])) {
                    transaction.show(fragmentList[selectPosition])
                } else {
                    transaction.add(R.id.fragment_container, fragmentList[selectPosition])
                }
                transaction.commitAllowingStateLoss()
                //fixme 测试
                testBadge(tab)
            }
        })
    }

    private fun testBadge(tab: TabLayout.Tab) {
        if (tab.position == 0 || tab.position == 2) {
            var badgeView = badgeArray.get(tab.position)
            if (badgeView == null) {
                badgeView = QBadgeView(this).bindTarget(tab.customView)
                        .setShowShadow(false)
                        .setBadgeTextSize(10f, true)
                        .setBadgeTextColor(R.color.colorPrimary.resToColor())
                        .setBadgePadding(4f, true)//设置内边距
                        .setGravityOffset(5f, 0f, true)//设置外边距
                badgeArray.put(tab.position, badgeView)
            }
            badgeView.badgeNumber = (Math.random() * 100 + 1).toInt()
        }
    }

    private fun newTab(drawable: Drawable, text: String): TabLayout.Tab {
        val tab = tabLayout.newTab()
        tab.setCustomView(R.layout.widget_tab_view)//导航栏自定义
        tab.customView?.let {
            val textView = it.findViewById<DrawableTextView>(R.id.tab_text)
            textView.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null)
            textView.text = text
        }
        return tab
    }

}
