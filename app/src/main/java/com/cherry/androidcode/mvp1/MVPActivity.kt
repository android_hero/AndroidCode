package com.cherry.androidcode.mvp1

import android.app.ProgressDialog
import android.os.Bundle
import com.cherry.androidcode.base.BaseActivity

/**
 * 第一重代理->目标对象->实现目标接口(MvpCallback)---->Activity抽象类
 * 第二重代理->代理对象->MVPActivity
 *  MVP说明：
 * 子类V层泛型如果是接口，必须实现createView()
 * 子类P层泛型如果是接口，必须实现createPresenter()
 * @author DongMS
 * @since 2019/7/11
 */
abstract class MVPActivity<V : BaseView, P : BasePresenter<V>> : BaseActivity(), MVPCallback<V, P>, BaseView {

    override var mvpPresenter: P? = null

    override var mvpView: V? = null

    protected val progressDialog by lazy {
        ProgressDialog(this)
    }

    //第二重代理:持有目标对象引用
    //代理对象持有目标对象引用
    private var delegateImpl: MVPActivityDelegateImpl<V, P>? = null

    private fun getDelegateImpl(): MVPActivityDelegateImpl<V, P> {
        if (delegateImpl == null) {
            this.delegateImpl = MVPActivityDelegateImpl(this)
        }
        return delegateImpl!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getDelegateImpl().onCreate(savedInstanceState)
    }

    //加载条ui根据实际情况
    override fun showLoading() {
        if (!isFinishing && !progressDialog.isShowing) {
            progressDialog.show()
        }
    }

    override fun dismissLoading() {
        if (!isFinishing && progressDialog.isShowing) {
            progressDialog.dismiss()
        }
    }

    override fun error(error: Throwable, errorMsg: String) {
    }

    override fun onStart() {
        super.onStart()
        getDelegateImpl().onStart()
    }

    override fun onRestart() {
        super.onRestart()
        getDelegateImpl().onRestart()
    }

    override fun onResume() {
        super.onResume()
        getDelegateImpl().onResume()
    }

    override fun onPause() {
        super.onPause()
        getDelegateImpl().onPause()
    }

    override fun onStop() {
        super.onStop()
        getDelegateImpl().onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        getDelegateImpl().onDestroy()
    }
}