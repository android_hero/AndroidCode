package com.cherry.androidcode.mvp1

import android.os.Bundle

/**
 * @author DongMS
 * @since 2019/7/11
 */
interface MVPActivityDelegate<V : BaseView, P : BasePresenter<V>> {

    fun onCreate(savedInstanceState: Bundle?)

    fun onStart()

    fun onPause()

    fun onResume()

    fun onRestart()

    fun onStop()

    fun onDestroy()

}