package com.cherry.androidcode.mvp1

/**
 * 高度抽象UI层接口
 * @author DongMS
 * @since 2019/7/11
 */
interface BaseView {

    ///////加载条///////

    fun showLoading()

    fun dismissLoading()

    fun error(error: Throwable, errorMsg: String)

}