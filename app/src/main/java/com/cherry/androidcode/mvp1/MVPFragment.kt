package com.cherry.androidcode.mvp1

import android.content.Context
import android.os.Bundle
import androidx.annotation.CallSuper
import com.cherry.androidcode.base.LazyLoadFragment
import com.parkingwang.android.view.SwLoading


/**
 * MVP说明：
 * 子类V层泛型如果是接口，必须实现createView()
 * 子类P层泛型如果是接口，必须实现createPresenter()
 * @author DongMS
 * @since 2019/7/11
 */
abstract class MVPFragment<V : BaseView, P : BasePresenter<V>> : LazyLoadFragment(), MVPCallback<V, P>, BaseView {

    override var mvpPresenter: P? = null

    override var mvpView: V? = null

    //    protected val progressDialog by lazy {
//        ProgressDialog(context)
//    }

    protected val swLoading: SwLoading by lazy {
        SwLoading(context).setMessage("加载中...")
    }

    //第二重代理:持有目标对象引用
    //代理对象持有目标对象引用
    private var delegateImpl: MVPFragmentDelegateImpl<V, P>? = null

    private fun getDelegateImpl(): MVPFragmentDelegateImpl<V, P> {
        if (delegateImpl == null) {
            this.delegateImpl = MVPFragmentDelegateImpl(this)
        }
        return delegateImpl!!
    }

    //加载条ui根据实际情况
    override fun showLoading() {
        if (activity != null && !activity!!.isFinishing && !swLoading.isShowing) {
            swLoading.show()
        }

    }

    override fun dismissLoading() {
        if (activity != null && !activity!!.isFinishing && swLoading.isShowing) {
            swLoading.dismiss()
        }
    }

    override fun error(error: Throwable, errorMsg: String) {
    }

    @CallSuper
    override fun onAttach(context: Context) {
        super.onAttach(context)
        getDelegateImpl().onAttach(context)
    }

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getDelegateImpl().onCreate(savedInstanceState)
    }

    @CallSuper
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        getDelegateImpl().onActivityCreated(savedInstanceState)
    }

    @CallSuper
    override fun onViewCreated(view: android.view.View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getDelegateImpl().onViewCreated(view, savedInstanceState)
    }

    @CallSuper
    override fun onStart() {
        super.onStart()
        getDelegateImpl().onStart()
    }

    @CallSuper
    override fun onResume() {
        super.onResume()
        getDelegateImpl().onResume()
    }

    @CallSuper
    override fun onPause() {
        super.onPause()
        getDelegateImpl().onPause()
    }

    @CallSuper
    override fun onStop() {
        super.onStop()
        getDelegateImpl().onStop()
    }

    @CallSuper
    override fun onDestroyView() {
        super.onDestroyView()
        getDelegateImpl().onDestroyView()
    }

    @CallSuper
    override fun onDestroy() {
        super.onDestroy()
        getDelegateImpl().onDestroy()
    }

    @CallSuper
    override fun onDetach() {
        super.onDetach()
        getDelegateImpl().onDetach()
    }

}