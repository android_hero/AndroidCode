package com.cherry.androidcode.mvp1

/**
 * @author DongMS
 * @since 2019/7/11
 */
interface MVPCallback<V : BaseView, P : BasePresenter<V>> {

    var mvpPresenter: P?

    var mvpView: V?

    //创建Presenter
    fun createPresenter(): P?

    //创建View
    fun createView(): V?

}