package com.cherry.androidcode.mvp1

import android.content.Context
import android.os.Bundle

/**
 * @author DongMS
 * @since 2019/7/11
 */
class MVPFragmentDelegateImpl<V : BaseView, P : BasePresenter<V>>(mvpCallback: MVPCallback<V, P>?) : MVPFragmentDelegate<V, P> {

    private val proxyMvpCallback: ProxyMVPCallback<V, P>

    init {
        if (mvpCallback == null) {
            throw NullPointerException("MVPCallback not null!")
        }
        proxyMvpCallback = ProxyMVPCallback(mvpCallback)
    }

    override fun onAttach(context: Context?) {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        this.proxyMvpCallback.createPresenter()
        this.proxyMvpCallback.createView()
        this.proxyMvpCallback.attachView()
    }

    override fun onViewCreated(view: android.view.View, savedInstanceState: Bundle?) {
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
    }

    override fun onStart() {
    }

    override fun onResume() {
    }

    override fun onPause() {
    }

    override fun onStop() {
    }

    override fun onRestart() {
    }

    override fun onDestroyView() {
    }

    override fun onDestroy() {
        this.proxyMvpCallback.detachView()
    }

    override fun onDetach() {
    }
}