package com.cherry.androidcode.mvp1

import android.os.Bundle

/**
 * 第二重代理->目标对象->针对的是Activity生命周期
 * @author DongMS
 * @since 2019/7/11
 */
class MVPActivityDelegateImpl<V : BaseView, P : BasePresenter<V>>(mvpCallback: MVPCallback<V, P>?) : MVPActivityDelegate<V, P> {

    private val proxyMvpCallback: ProxyMVPCallback<V, P>

    init {
        if (mvpCallback == null) {
            throw NullPointerException("MVPCallback not null!")
        }
        proxyMvpCallback = ProxyMVPCallback(mvpCallback)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        //绑定处理
        this.proxyMvpCallback.createPresenter()
        this.proxyMvpCallback.createView()
        this.proxyMvpCallback.attachView()
    }

    override fun onStart() {
    }

    override fun onPause() {
    }

    override fun onResume() {
    }

    override fun onRestart() {
    }

    override fun onStop() {
    }

    override fun onDestroy() {
        this.proxyMvpCallback.detachView()
    }
}