package com.cherry.androidcode.mvp1

import android.content.Context
import android.os.Bundle

/**
 * @author DongMS
 * @since 2019/7/11
 */
interface MVPFragmentDelegate<V : BaseView, P : BasePresenter<V>> {

    fun onAttach(context: Context?)

    fun onCreate(savedInstanceState: Bundle?)

    fun onViewCreated(view: android.view.View, savedInstanceState: Bundle?)

    fun onActivityCreated(savedInstanceState: Bundle?)

    fun onStart()

    fun onResume()

    fun onPause()

    fun onStop()

    fun onRestart()

    fun onDestroyView()

    fun onDestroy()

    fun onDetach()
}