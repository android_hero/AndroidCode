package com.cherry.androidcode.mvp1

/**
 * 高度抽象UI层接口
 * @author DongMS
 * @since 2019/7/11
 */
interface BasePresenter<V:BaseView> {

    fun attachView(view: V)

    fun detachView()
}