package com.cherry.androidcode.mvp1

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable


/**
 * @author DongMS
 * @since 2019/7/11
 */
open class MVPBasePresenter<V : BaseView> : BasePresenter<V> {

    var view: V? = null

    protected var compositeDisposable: CompositeDisposable? = null

    fun unSubscribe() {
        if (compositeDisposable != null && !compositeDisposable!!.isDisposed) {
            compositeDisposable!!.dispose()
        }
    }

    fun addSubscribe(disposable: Disposable) {
        if (compositeDisposable == null) {
            compositeDisposable = CompositeDisposable()
        }
        compositeDisposable!!.add(disposable)
    }

    override fun attachView(view: V) {
        this.view = view
    }

    override fun detachView() {
        unSubscribe()
        this.view = null
    }
}