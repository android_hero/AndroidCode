package com.cherry.androidcode.image

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/**
 * glide4自定义模块
 * 支持glide4链式编程,不用到清单中注册
 * @author DongMS
 * @date 2019/6/26
 */
@GlideModule
class GlobalGlideConfig : AppGlideModule()
