package com.cherry.androidcode.listener

import com.google.android.material.tabs.TabLayout

/**
 * @author 董棉生(dongmiansheng@parkingwang.com)
 * @since 2019/6/7
 */

open class SimpleTabSelectedListener : TabLayout.OnTabSelectedListener {
    override fun onTabReselected(tab: TabLayout.Tab) {
    }

    override fun onTabUnselected(tab: TabLayout.Tab) {
    }

    override fun onTabSelected(tab: TabLayout.Tab) {
    }
}