package com.cherry.androidcode.jetpack.room

import androidx.room.*

/**
 * 每个实体必须将至少 1 个字段定义为主键。即使只有 1 个字段，您仍然需要为该字段添加 @PrimaryKey 注释。
 * 此外，如果您想让 Room 为实体分配自动 ID，则可以设置 @PrimaryKey 的 autoGenerate 属性。
 * 如果实体具有复合主键，您可以使用 @Entity 注释的 primaryKeys 属性
 */
//默认类名就是表名(全小写),SQLite 中的表名称不区分大小写
@Entity
//    (primaryKeys = [uid])
//    (tableName = "user")

//字段加索引及唯一
//(indices = [Index(value = ["age"],unique = true)])
data class TestUser(

    @PrimaryKey
//        (autoGenerate = true)
    val uid: String,
    @ColumnInfo(name = "name") val testName: String?,
    //与表字段保持一致
    @ColumnInfo val age: Int,

    //默认情况下，Room 会为实体中定义的每个字段创建一个列,不想保留的字段
    @Ignore var check: Boolean = false

)

open class Parent {

    var selected: Boolean? = null

}

//实体继承父类，但父类的字段不需要用到
@Entity(ignoredColumns = ["selected"])
data class Child(val name: String) : Parent()

