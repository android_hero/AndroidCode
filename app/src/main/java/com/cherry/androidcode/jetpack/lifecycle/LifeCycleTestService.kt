package com.cherry.androidcode.jetpack.lifecycle

import androidx.lifecycle.LifecycleService

/**
 * @author Cherry(dongmiansheng@gmail.com)
 * @since 2021/5/29
 */

class LifeCycleTestService : LifecycleService() {

    override fun onCreate() {
        super.onCreate()
        lifecycle.addObserver(LifeCycleServiceObserverTest())
    }
}