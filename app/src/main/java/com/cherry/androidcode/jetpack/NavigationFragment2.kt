package com.cherry.androidcode.jetpack

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.cherry.androidcode.R
import com.cherry.androidcode.databinding.FragmentNavigation2Binding

/**
 * @author DongMS
 * @since 2020/4/7
 */
class NavigationFragment2 : Fragment() {

    /**
     * 最后一步实现，也可以使用 val binding =  FragmentNavigation2Binding.inflate(inflater,container,false)
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        //实例化并赋值
        val binding: FragmentNavigation2Binding = DataBindingUtil.inflate(inflater, R.layout.fragment_navigation2, container, false)
        //给xml中变量赋值
        binding.activity = activity as? AppCompatActivity
        binding.model = NavigationModel2("第2个界面${arguments?.getString("a")}", "***", context!!)
        return binding.root
    }
}