package com.cherry.androidcode.jetpack

import android.content.Context
import android.util.Log
import androidx.databinding.ObservableField
import com.parkingwang.android.SwToast

/**
 * @author DongMS
 * @since 2020/4/8
 */
class NavigationModel2(nameS: String, pwdS: String, private val context: Context) {

    /**
     *
    方法	作用
    ObservableField(T value)	构造函数，设置可观察的域
    T get()	获取可观察的域的内容，可以使用UI控件监测它的值
    set(T value)	设置可观察的域，设置成功之后，会通知UI控件进行更新

    除了使用ObservableField之外，Data Binding为我们提供了基本类型的ObservableXXX(如ObservableInt)和存放容器的ObservableXXX(如ObservableList<T>)等，
    同样，如果你想让你自定义的类变成可观察状态，需要实现Observable接口

     *
     */
    //不能使用private,否则databing xml中不能直接使用name,pwd,不能使用val，否则set使用不了，居然编译过去了。。。
    var name = ObservableField<String>(nameS)
    var pwd = ObservableField<String>(pwdS)
    var number = ObservableField("")

    fun onNameChange(s: CharSequence) {
        Log.e("实时name", s.toString())
        name.set(s.toString())
    }

    /**
     * kt中::表示把一个方法当做一个参数，传递到另一个方法中进行使用，要确定参数个数、类型、返回值都和其形参一致
     * 还有一种情况，调用当前 Class 的内部方法时，为了防止作用域混淆 ， :: 调用的函数如果是类的成员函数或者是扩展函数，必须使用限定符,比如this
     */
    fun onPwdChange(s: CharSequence, start: Int, before: Int, count: Int) {
        Log.e("实时pwd", s.toString())
        pwd.set(s.toString())
    }

    fun testLogin() {
        if (name.get().equals("admin") && pwd.get().equals("admin")) {
            SwToast.showShort("账号密码正确")
        } else {
            SwToast.showShort("账号密码不正确")
        }
    }

}