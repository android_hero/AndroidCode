package com.cherry.androidcode.jetpack.viewmodel

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.map
import androidx.lifecycle.switchMap
import com.cherry.androidcode.R
import com.cherry.androidcode.base.BaseTitleBarActivity
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_view_model_live_data.*
import java.util.concurrent.TimeUnit
import kotlin.random.Random

class ViewModelLiveDataActivity : BaseTitleBarActivity() {

    override val titleText = R.string.view_model_live_data

    override val contentLayoutRes = R.layout.activity_view_model_live_data

    private lateinit var simpleViewModel: SimpleViewModel

    private lateinit var liveDataViewModel: LiveDataViewModel

    private val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        simpleViewModel()

        liveDataViewModel()

        supportFragmentManager.beginTransaction()
            .add(R.id.fl_fragment_1, LiveDataFragment1())
            .add(R.id.fl_fragment_2, LiveDataFragment2())
            .commitNow()

    }

    private fun liveDataViewModel() {
        liveDataViewModel =
            ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application)).get(
                LiveDataViewModel::class.java
            )
        liveDataViewModel.liveDataInt.observe(this, Observer<Int> {
//            观察者
            tv_live_data_view_model.text = it.toString()
        })

        startTimer()

        //Transformations.map
        liveDataViewModel.liveDataMap.apply {
            observe(this@ViewModelLiveDataActivity) {
                tv_live_data_map_before.text = it
            }
        }.map {
            "Transformations.map转换后"
        }.observe(this) {
            tv_live_data_map_after.text = it
        }

        liveDataViewModel.liveDataMap.value = "Transformations.map转换前"

        //Transformations.switchMap(),手动控制监听其中一个的数据变化，并能根据需要随时切换监听
        liveDataViewModel.switchMap.switchMap {
            if (it) {
                liveDataViewModel.switchMap1
            } else {
                liveDataViewModel.switchMap2
            }
        }.observe(this) {
            tv_live_data_switch_map.text = it
        }

    }

    fun tf_switchMap(view: View) {
        liveDataViewModel.let {
            it.switchMap.value = it.switchMap.value?.not()
            it.switchMap1.value = "switchMap1_${Random.nextInt(1, 100)}"
            it.switchMap2.value = "switchMap2_${Random.nextInt(1, 100)}"
        }
    }

    private fun simpleViewModel() {

        simpleViewModel = ViewModelProvider(this).get(SimpleViewModel::class.java)
//        simpleViewModel = ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application)).get(SimpleViewModel::class.java)
        tv_simple_view_model.text = simpleViewModel.number.toString()
    }

    @SuppressLint("CheckResult")
    private fun startTimer() {

        Observable.interval(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .subscribe({ i ->
                //被观察者
                //子线程需要使用postValue
                liveDataViewModel.liveDataInt.postValue(i.toInt())
            }, {}, {}, { dispose -> compositeDisposable.add(dispose) })
    }

    fun plusNumber(view: View) {
        simpleViewModel.number += 1
        tv_simple_view_model.text = simpleViewModel.number.toString()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

}