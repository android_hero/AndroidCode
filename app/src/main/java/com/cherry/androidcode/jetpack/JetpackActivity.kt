package com.cherry.androidcode.jetpack

import android.os.Bundle
import com.cherry.androidcode.R
import com.cherry.androidcode.base.BaseTitleBarActivity
import com.cherry.androidcode.recyclerView.DemoFragment

/**
 * @author Cherry(dongmiansheng@gmail.com)
 * @since 2021/5/29
 */

class JetpackActivity : BaseTitleBarActivity() {
    override val titleText = R.string.jetpack

    override val contentLayoutRes = R.layout.layout_fragment_container


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportFragmentManager.beginTransaction().add(R.id.fl_fragment_container, DemoFragment.newInstance(DemoFragment.JETPACK_MAIN_TAG)).commitNow()
    }

}