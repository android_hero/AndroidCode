package com.cherry.androidcode.jetpack

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

/**
 * @author DongMS
 * @since 2020-08-19
 */
class TestViewModel(var number: Int = 0) : ViewModel() {
    val likeNumber by lazy {
        val mutableLiveData = MutableLiveData<Int>()
        mutableLiveData.value = 0
        mutableLiveData
    }

    fun addLike(n: Int) {
        likeNumber.value = likeNumber.value!! + n
    }
}