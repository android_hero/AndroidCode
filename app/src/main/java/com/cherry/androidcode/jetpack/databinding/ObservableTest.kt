package com.cherry.androidcode.jetpack.databinding

import androidx.databinding.ObservableArrayMap
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt

/**
 * 通过数据绑定，数据对象可在其数据发生更改时通知其他对象，即监听器。可观察类有三种不同类型：对象、字段和集合。
当其中一个可观察数据对象绑定到界面并且该数据对象的属性发生更改时，界面会自动更新。
 *
 * ObservableBoolean
ObservableByte
ObservableChar
ObservableShort
ObservableInt
ObservableLong
ObservableFloat
ObservableDouble
ObservableParcelable
 */
class ObservableTest {

    val firstName = ObservableField<String>()
    val age = ObservableInt()

    //可观察集合ObservableArrayMap
    val name = ObservableArrayMap<String, Any>().apply {
        put("aaa", "Google")
        put("bbb", 17)
    }

}