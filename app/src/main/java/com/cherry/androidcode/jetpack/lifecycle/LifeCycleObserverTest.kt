package com.cherry.androidcode.jetpack.lifecycle

import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent

/**
 *
 * LifeCycle 在Activity基本使用
 * @author Cherry(dongmiansheng@gmail.com)
 * @since 2021/5/29
 */

class LifeCycleObserverTest : LifecycleObserver {

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun testCreate() {
        Log.w("LifeCycleTest", "testCreate")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun testResume() {
        Log.w("LifeCycleTest", "testResume")
    }
    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun testPause() {
        Log.w("LifeCycleTest", "testPause")
    }
    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun testDestroy() {
        Log.w("LifeCycleTest", "testDestroy")
    }

}