package com.cherry.androidcode.jetpack

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.cherry.androidcode.R

/**
 * @author DongMS
 * @since 2020/4/7
 */
class NavigationActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)
    }

}
