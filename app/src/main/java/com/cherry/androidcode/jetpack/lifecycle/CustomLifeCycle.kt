package com.cherry.androidcode.jetpack.lifecycle

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry

class CustomLifeCycle : LifecycleOwner {

    private val lifecycleRegistry = LifecycleRegistry(this)

    fun onStart(){
        lifecycleRegistry.currentState = Lifecycle.State.STARTED
    }

    fun onCreate(){
        lifecycleRegistry.currentState = Lifecycle.State.CREATED
    }

    override fun getLifecycle() = lifecycleRegistry
}