package com.cherry.androidcode.jetpack.viewmodel

import android.view.View
import android.widget.SeekBar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.cherry.androidcode.R
import com.cherry.androidcode.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_live_data.*

class LiveDataFragment1 : BaseFragment() {

    override val contentLayout = R.layout.fragment_live_data

    override fun initView(view: View) {
        super.initView(view)
        tv_label.text = "LiveDataFragment1"

        val liveDataViewModel = ViewModelProvider(
            requireActivity(),
            ViewModelProvider.AndroidViewModelFactory(requireActivity().application)
        )[LiveDataViewModel::class.java]

        liveDataViewModel.liveDataProgress.observe(this, Observer {
            seek_bar.progress = it
        })

        seek_bar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                //主线程直接set
                liveDataViewModel.liveDataProgress.value = progress
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }
        })
    }
}