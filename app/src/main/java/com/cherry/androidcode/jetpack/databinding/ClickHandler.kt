package com.cherry.androidcode.jetpack.databinding

import android.util.Log
import android.view.View
import com.parkingwang.android.SwToast

class ClickHandler {

    //方法引用,表达式中的方法签名必须与监听器对象中的方法签名完全一致
    fun clickReference(view: View) {
        SwToast.showShort("点击方法引用")
    }

    //监听器方式可以传参
    fun clickListener(s: String, i: Int) {
        SwToast.showShort("点击监听器绑定$s---$i")
    }

    fun clickListener1(view: View) {
        SwToast.showShort("点击监听器绑定")
    }

    fun checkChange(s: String, isChecked: Boolean) {
        SwToast.showShort("选中与否$s-----$isChecked")
    }

    //返回值
    //由于 null 对象而无法对表达式求值，则数据绑定将返回该类型的默认值。例如，引用类型返回 null，int 返回 0，boolean 返回 false，等等
    fun longClick(s: String): Boolean {
        SwToast.showShort("长按$s")
        return false
    }

}