package com.cherry.androidcode.jetpack.databinding

import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingMethod
import androidx.databinding.BindingMethods
import androidx.databinding.DataBindingUtil
import com.cherry.androidcode.R
import com.cherry.androidcode.databinding.ActivityDataBindingBinding
import com.cherry.androidcode.listener.SimpleTextWatcher

/**
 * @author Cherry(dongmiansheng@gmail.com)
 * @since 2021/6/5
 */

//一些属性具有名称不符的 setter 方法
//注释与类一起使用，可以包含多个 BindingMethod 注释，每个注释对应一个重命名的方法。绑定方法是可添加到应用中任何类的注释。
//android:tint 属性与 setImageTintList(ColorStateList) 方法相关联，而不与 setTint() 方法相关联
//@BindingMethods(value = [
//    BindingMethod(
//        type = ImageView::class,
//        attribute = "android:tint",
//        method = "setImageTintList")])
//大多数情况下，您无需在 Android 框架类中重命名 setter。特性已使用命名惯例实现，可自动查找匹配的方法
class DataBindingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //ActivityDataBindingBinding 命名生成布局名字+Binding
        val activityDataBindingBinding = DataBindingUtil.setContentView<ActivityDataBindingBinding>(
            this,
            R.layout.activity_data_binding
        )
        //另一种获取方式
//        val activityDataBindingBinding =  ActivityDataBindingBinding.inflate(layoutInflater)

        //Fragment、ListView 或 RecyclerView 适配器中使用数据绑定项
//        val listItemBinding = ListItemBinding.inflate(layoutInflater, viewGroup, false)
//        val listItemBinding = DataBindingUtil.inflate(layoutInflater, R.layout.list_item, viewGroup, false)


        val idol1 =
            Idol("", "小红", 5, listOf("aa", "cc"), mapOf("123" to "abc", "789" to "yml")).apply {
                activityDataBindingBinding.idol = this
            }

        activityDataBindingBinding.clickH = ClickHandler()

        activityDataBindingBinding.userList =
            listOf(idol1, Idol("", "小明", 18, emptyList(), emptyMap()))

        activityDataBindingBinding.network =
            "https://i0.hdslb.com/bfs/feed-admin/af31b91c9ce658868e73da61fee4575c988ab48a.jpg"

        activityDataBindingBinding.inputListener1 = object : SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                Log.w("---", s.toString())
            }
        }

    }

}