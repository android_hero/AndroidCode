package com.cherry.androidcode.jetpack.lifecycle

import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent

/**
 * @author Cherry(dongmiansheng@gmail.com)
 * @since 2021/5/29
 */

class LifeCycleAppObserverTest : LifecycleObserver {

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun testCreate() {
        Log.w("AppObserverTest", "testCreate")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun testStart() {
        Log.w("AppObserverTest", "testStart")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun testResume() {
        Log.w("AppObserverTest", "testResume")
    }
    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun testPause() {
        Log.w("AppObserverTest", "testPause")
    }
    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun testDestroy() {
        Log.w("AppObserverTest", "testDestroy")
    }
}