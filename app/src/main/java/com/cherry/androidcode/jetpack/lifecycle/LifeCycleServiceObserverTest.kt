package com.cherry.androidcode.jetpack.lifecycle

import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent

/**
 *
 * LifeCycle 在Activity基本使用
 * @author Cherry(dongmiansheng@gmail.com)
 * @since 2021/5/29
 */

class LifeCycleServiceObserverTest : LifecycleObserver {

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun testCreate() {
        Log.w("LifeCycleTestService", "testCreate")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun testStart() {
        Log.w("LifeCycleTestService", "testStart")
    }

    //服务这些生命周期不会被调用
//    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
//    fun testResume() {
//        Log.w("LifeCycleTestService", "testResume")
//    }
//    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
//    fun testPause() {
//        Log.w("LifeCycleTestService", "testPause")
//    }
    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun testDestroy() {
        Log.w("LifeCycleTestService", "testDestroy")
    }

}