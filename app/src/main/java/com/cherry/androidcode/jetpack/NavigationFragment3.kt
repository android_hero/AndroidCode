package com.cherry.androidcode.jetpack

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs

/**
 * liveData优点
 * 数据变更的时候更新UI
没有内存泄漏
不会因为停止Activity崩溃
无需手动处理生命周期
共享资源
子类
MutableLiveData
Transformations
map   ----数据变换
switchMap -- 上一步的liveData通知下一步的liveData
MediatorLiveData  addSource      ----多个liveData合并
需要注意的是：当 Fragment 不 再处于 active 状态时，如果 LiveDataA 和 LiveDataB 的数据都发生了变化，那么当 Fragment 重新恢复 active 状态时，MediatorLiveData 将获取最后添加的 LiveData 的数据发送给 Fragment，这里即 LiveDataB


 常用方法：

方法名	作用
observe(@NonNull LifecycleOwner owner, @NonNull Observer<? super T> observer)	最常用的方法，需要提供Observer处理数据变更后的处理。LifecycleOwner则是我们能够正确处理声明周期的关键！
setValue(T value)	设置数据
getValue():T	获取数据
postValue(T value)	在主线程中更新数据

viewModel
ViewModel同样具有生命周期意识的处理跟UI相关的数据，并且，当设备的一些配置信息改变（例如屏幕旋转）它的数据不会消失
同一个Activity的Fragment之间可以使用ViewModel实现共享数据
 *
 *
 *
 * @author DongMS
 * @since 2020/4/7
 */
class NavigationFragment3 : Fragment() {

    private lateinit var textView: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val linearLayout = LinearLayout(context)
        linearLayout.orientation = LinearLayout.VERTICAL
        textView = TextView(context)
        textView.setTextColor(Color.RED)
        linearLayout.addView(textView)
        return linearLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val safeArgs: NavigationFragment3Args by navArgs()
        textView.text = "跳转到第3个界面${safeArgs.aa}-------${safeArgs.bb}"
    }


}