package com.cherry.androidcode.jetpack.viewmodel

import androidx.lifecycle.ViewModel

class SimpleViewModel : ViewModel() {
    var number: Int = 0
}