package com.cherry.androidcode.jetpack

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.cherry.androidcode.R

/**
 * @author DongMS
 * @since 2020/4/7
 */
class NavigationFragment1 : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val linearLayout = LinearLayout(context)
        linearLayout.orientation = LinearLayout.VERTICAL
        val textView = TextView(context)
        textView.text = "跳转到第2个界面"
        textView.setTextColor(Color.RED)
        linearLayout.addView(textView)
        textView.setOnClickListener {
            // 设置动画参数
//            val navOption = navOptions {
//                anim {
//                    enter = R.anim.slide_in_right
//                    exit = R.anim.slide_out_left
//                    popEnter = R.anim.slide_in_left
//                    popExit = R.anim.slide_out_right
//                }
//            }
            val bundle = Bundle()
            bundle.putString("a", "aa")
//            Navigation.createNavigateOnClickListener(R.id.navigationFragment2, bundle)
            findNavController().navigate(R.id.navigationFragment2, bundle)
        }
        val textView1 = TextView(context)
        textView1.text = "跳转到第3个界面"
        textView1.setTextColor(Color.GREEN)
        textView1.setOnClickListener {
            val action = NavigationFragment1Directions.actionNav3().setAa("hello").setBb(1024)
            findNavController().navigate(action)
        }

        linearLayout.addView(textView1)

//        testViewModel(linearLayout)
        val textView4 = TextView(context)
        textView4.setTextColor(Color.BLACK)

        val testViewModel = ViewModelProviders.of(this).get(TestViewModel::class.java)
        testViewModel.likeNumber.observe(viewLifecycleOwner, Observer {
            textView4.text = testViewModel.likeNumber.value.toString()
        })

        val textView2 = TextView(context)
        textView2.text = "testViewModel +"
        textView2.setTextColor(Color.YELLOW)
        textView2.setOnClickListener {
            testViewModel.addLike(1)
        }
        val textView3 = TextView(context)
        textView3.text = "testViewModel -"
        textView3.setTextColor(Color.MAGENTA)
        textView3.setOnClickListener {
            testViewModel.addLike(-1)
        }

        linearLayout.addView(textView2)
        linearLayout.addView(textView3)
        linearLayout.addView(textView4)

        return linearLayout
    }

    private fun testViewModel(linearLayout: LinearLayout) {
        //屏幕旋转和国际化数据不会销毁
        val testViewModel = ViewModelProviders.of(this).get(TestViewModel::class.java)
        val textView4 = TextView(context)
        textView4.text = testViewModel.number.toString()
        textView4.setTextColor(Color.BLACK)

        val textView2 = TextView(context)
        textView2.text = "testViewModel +"
        textView2.setTextColor(Color.YELLOW)
        textView2.setOnClickListener {
            testViewModel.number++
            textView4.text = testViewModel.number.toString()
        }
        val textView3 = TextView(context)
        textView3.text = "testViewModel -"
        textView3.setTextColor(Color.MAGENTA)
        textView3.setOnClickListener {
            testViewModel.number--
            textView4.text = testViewModel.number.toString()
        }

        linearLayout.addView(textView2)
        linearLayout.addView(textView3)
        linearLayout.addView(textView4)
    }

}