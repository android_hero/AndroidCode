package com.cherry.androidcode.jetpack.databinding

/**
 * @author Cherry(dongmiansheng@gmail.com)
 * @since 2021/6/5
 */

object StarUtil {

    fun convert(star: Int): String {
        return "$star 星"
    }

}