package com.cherry.androidcode.jetpack.room

import androidx.room.*

@Dao
interface TestUserDao {

    @Insert
    fun insert(vararg user: TestUser)

    @Delete
    fun delete(vararg user: TestUser)

    @Query("select * from testuser")
    fun queryAll(): List<TestUser>

    //in 相当于=    id就是函数的形参名，需要加上:
    @Query("select * from testuser where uid in (:id)")
    fun queryById(id: String): TestUser

    @Query("select * from testuser where name like (:n) limit 1")
    fun queryByLikeName(n: String): TestUser

    @Update
    fun update(user: TestUser)

}