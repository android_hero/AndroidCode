package com.cherry.androidcode.jetpack.lifecycle

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.cherry.androidcode.R
import com.cherry.androidcode.base.BaseTitleBarActivity

/**
 * @author Cherry(dongmiansheng@gmail.com)
 * @since 2021/5/29
 */

class LifeCycleActivity : BaseTitleBarActivity() {

    override val titleText = R.string.life_cycle

    private lateinit var customLifeCycle: CustomLifeCycle

    override val contentLayoutRes = R.layout.activity_life_cycle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycle.addObserver(LifeCycleObserverTest())
        customLifeCycle = CustomLifeCycle().apply {
                onCreate()
        }
//        lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)
    }

    override fun onStart() {
        super.onStart()
        customLifeCycle.onStart()
    }

    fun startService(view: View) {
        startService(Intent(this, LifeCycleTestService::class.java))
    }

    fun stopService(view: View) {
        stopService(Intent(this, LifeCycleTestService::class.java))
    }

}