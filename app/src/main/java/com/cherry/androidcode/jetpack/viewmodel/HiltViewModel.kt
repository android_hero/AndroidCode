package com.cherry.androidcode.jetpack.viewmodel

import androidx.lifecycle.ViewModel
import com.cherry.androidcode.jetpack.repository.HiltRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class HiltViewModel @Inject constructor(val hiltRepository: HiltRepository) : ViewModel()