package com.cherry.androidcode.jetpack.databinding

import android.view.View
import com.parkingwang.android.SwToast

/**
 * @author Cherry(dongmiansheng@gmail.com)
 * @since 2021/6/5
 */

object ClickObjectHandler {

    //去掉此注解，xml中就要显示INSTANCE
    @JvmStatic
    fun testClick(view: View) {
        SwToast.showShort("测试点击")
    }

    @JvmStatic
    fun testClick(s: String, i: Int) {
        SwToast.showShort("测试点击$s------$i")
    }

}