package com.cherry.androidcode.jetpack.databinding

import android.util.SparseArray

/**
 * @author Cherry(dongmiansheng@gmail.com)
 * @since 2021/6/5
 */

data class Idol(
    val image: String, val name: String, val star: Int,
    val list: List<String>,
    val map: Map<String, String>
)