package com.cherry.androidcode.jetpack.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class LiveDataViewModel : ViewModel() {

    val liveDataInt: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>().apply { value = 0 }
    }

    val liveDataProgress: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>().apply { value = 0 }
    }

    //Transformations.map(),在LiveData对象分发给观察者之前对其中存储的值进行更改
    val liveDataMap: MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }

    val switchMap by lazy {
        MutableLiveData(false)
    }

    val switchMap1 by lazy {
        MutableLiveData<String>()
    }
    val switchMap2 by lazy {
        MutableLiveData<String>()
    }

}