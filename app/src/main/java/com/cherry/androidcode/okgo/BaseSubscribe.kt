package com.cherry.androidcode.okgo

import com.cherry.androidcode.entity.HttpResult
import com.cherry.androidcode.mvp1.MVPBasePresenter
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import java.lang.ref.WeakReference

/**
 * @author 董棉生(dongmiansheng@parkingwang.com)
 * @since 2019/6/23
 */

abstract class BaseObserver<T>(presenter: MVPBasePresenter<*>) : Observer<HttpResult<T>> {

    private val presenterReference: WeakReference<MVPBasePresenter<*>> = WeakReference(presenter)

    override fun onComplete() {
        presenterReference.get()?.view?.dismissLoading()
    }

    override fun onSubscribe(d: Disposable) {
        presenterReference.get()?.addSubscribe(d)
    }

    override fun onNext(t: HttpResult<T>) {
        onSuccess(t.data)
    }

    abstract fun onSuccess(data: T)

    override fun onError(e: Throwable) {

    }
}