package com.cherry.androidcode.video

import com.cherry.androidcode.entity.VideoSearchList
import com.cherry.androidcode.mvp1.BasePresenter
import com.cherry.androidcode.mvp1.BaseView

/**
 * @author Cherry(dongmiansheng @ gmail.com)
 * @since 2019/8/25
 */

interface VideoChildListContract {

    interface View : BaseView {
        fun onLoadData(list: MutableList<VideoSearchList>, isRefresh: Boolean)
        fun onLoadDataError(msg: String, isRefresh: Boolean)
    }

    interface Presenter : BasePresenter<View> {
        fun searchData(keyword: String, page: Int, isRefresh: Boolean)
    }

}
