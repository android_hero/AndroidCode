package com.cherry.androidcode.video

import android.annotation.SuppressLint
import android.view.View
import android.view.inputmethod.EditorInfo
import com.cherry.androidcode.R
import com.cherry.androidcode.base.LazyLoadFragment
import com.cherry.androidcode.helper.DrawableHelper
import kotlinx.android.synthetic.main.fragment_video_list.*

/**
 * @author Cherry(dongmiansheng@gmail.com)
 * @since 2019/8/25
 */

class VideoListFragment : LazyLoadFragment() {

    override val contentLayout = R.layout.fragment_video_list

    override fun initView(view: View) {
        DrawableHelper.normalDivider(searchView)
        searchView.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH && searchView.length() > 0) {
//                searchView.setDeleteIconVisible(false, false)
                doSearch(searchView.text.toString())
            }
            false
        }

    }

    @SuppressLint("CheckResult")
    private fun doSearch(searchKey: String) {
        (childFragmentManager.findFragmentById(R.id.video_child) as VideoChildListFragment).keyword = searchKey
    }

    companion object {
        fun newInstance() = VideoListFragment()
    }
}