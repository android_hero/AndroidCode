package com.cherry.androidcode.video

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import com.cherry.androidcode.R
import com.cherry.androidcode.image.GlideApp
import com.shuyu.gsyvideoplayer.utils.GSYVideoType
import com.shuyu.gsyvideoplayer.video.StandardGSYVideoPlayer
import com.shuyu.gsyvideoplayer.video.base.GSYBaseVideoPlayer
import com.shuyu.gsyvideoplayer.video.base.GSYVideoPlayer
import kotlinx.android.synthetic.main.sample_video.view.*

/**
 * @author Cherry(dongmiansheng@gmail.com)
 * @since 2019/9/1
 */

class CVideoPlayer : StandardGSYVideoPlayer {

    private var mType = ScaleType.DEFAULT

    var tempPosition = 0L

    constructor(context: Context?, fullFlag: Boolean?) : super(context, fullFlag)
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)

    override fun getLayoutId() = R.layout.sample_video

    override fun init(context: Context?) {
        super.init(context)
        moreScale.setOnClickListener {
            if (!mHadPlay) {
                return@setOnClickListener
            }
            //todo 这里可换成popupwindow选择
            mType = when (mType) {
                ScaleType.DEFAULT -> ScaleType.STRETCH
                ScaleType.STRETCH -> ScaleType.FULL_SCREEN
                ScaleType.FULL_SCREEN -> ScaleType.SIXTEEN2NINE
                ScaleType.SIXTEEN2NINE -> ScaleType.FOUR2THREE
                ScaleType.FOUR2THREE -> ScaleType.DEFAULT
            }
            resolveTypeUI()
        }
        reset.setOnClickListener {
            if (currentPositionWhenPlaying != 0) {
                tempPosition = currentPositionWhenPlaying - 3000L
            }
            seekOnStart = tempPosition
            startPlayLogic()
        }
    }

    // 全屏时将对应处理参数逻辑赋给全屏播放器
    override fun startWindowFullscreen(context: Context?, actionBar: Boolean, statusBar: Boolean): GSYBaseVideoPlayer {
        val sampleVideo = super.startWindowFullscreen(context, actionBar, statusBar) as CVideoPlayer
        sampleVideo.mType = mType
        sampleVideo.resolveTypeUI()
        //这个播放器的demo配置切换到全屏播放器
        //这只是单纯的作为全屏播放显示，如果需要做大小屏幕切换，请记得在这里耶设置上视频全屏的需要的自定义配置
        //比如已旋转角度之类的等等
        //可参考super中的实现
        return sampleVideo
    }


    //退出全屏时将对应处理参数逻辑返回给非播放器
    override fun resolveNormalVideoShow(oldF: View?, vp: ViewGroup?, gsyVideoPlayer: GSYVideoPlayer?) {
        super.resolveNormalVideoShow(oldF, vp, gsyVideoPlayer)
        if (gsyVideoPlayer != null) {
            val sampleVideo = gsyVideoPlayer as CVideoPlayer
            mType = sampleVideo.mType
            resolveTypeUI()
        }
    }

    /**
     * 显示比例
     * 注意，GSYVideoType.setShowType是全局静态生效，除非重启APP。
     */
    @SuppressLint("SetTextI18n")
    private fun resolveTypeUI() {
        if (!mHadPlay) {
            return
        }
        when (mType) {
            ScaleType.SIXTEEN2NINE -> {
                moreScale.text = "16:9"
                GSYVideoType.setShowType(GSYVideoType.SCREEN_TYPE_16_9)
            }
            ScaleType.FOUR2THREE -> {
                moreScale.text = "4:3"
                GSYVideoType.setShowType(GSYVideoType.SCREEN_TYPE_4_3)
            }
            ScaleType.FULL_SCREEN -> {
                moreScale.text = "全屏"
                GSYVideoType.setShowType(GSYVideoType.SCREEN_TYPE_FULL)
            }
            ScaleType.STRETCH -> {
                moreScale.text = "拉伸全屏"
                GSYVideoType.setShowType(GSYVideoType.SCREEN_MATCH_FULL)
            }
            ScaleType.DEFAULT -> {
                moreScale.text = "默认比例"
                GSYVideoType.setShowType(GSYVideoType.SCREEN_TYPE_DEFAULT)
            }
        }
        changeTextureViewShowType()
        mTextureView?.requestLayout()
    }

    private enum class ScaleType {
        DEFAULT,
        STRETCH,
        FULL_SCREEN,
        FOUR2THREE,
        SIXTEEN2NINE
    }

}