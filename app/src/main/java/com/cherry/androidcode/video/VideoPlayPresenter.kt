package com.cherry.androidcode.video

import android.annotation.SuppressLint
import android.util.Log
import com.cherry.androidcode.AppConstant
import com.cherry.androidcode.entity.MultiVideoDetail
import com.cherry.androidcode.entity.VideoDetail
import com.cherry.androidcode.mvp1.MVPBasePresenter
import com.cherry.androidcode.okgo.JsonConvert
import com.lzy.okgo.OkGo
import com.lzy.okrx2.adapter.ObservableBody
import com.tencent.mmkv.MMKV
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.Response
import org.jsoup.Jsoup

/**
 * @author Cherry(dongmiansheng@gmail.com)
 * @since 2019/8/31
 */

class VideoPlayPresenter : MVPBasePresenter<VideoPlayContract.View>(), VideoPlayContract.Presenter {

    @SuppressLint("CheckResult")
    override fun loadData(htmlUrl: String) {
        view?.showLoading()
//        yifangDetailData(htmlUrl)
//        OkGo.get<List<VideoDetail>>(MMKV.defaultMMKV().decodeString(AppConstant.QILING6080_URL) + htmlUrl)
//                .headers("User-Agent", "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.81 Mobile Safari/537.36")
//                .headers("Accept", "text/html,application/xhtml")
//                .converter(object : JsonConvert<List<VideoDetail>>() {
//
//                    override fun convertResponse(response: Response): List<VideoDetail>? {
//                        val document = Jsoup.parse(response.body()?.string())
//                    }
//                })
    }

    @SuppressLint("CheckResult")
    private fun yifangDetailData(htmlUrl: String) {
        OkGo.get<List<VideoDetail>>(MMKV.defaultMMKV().decodeString(AppConstant.YIFANG_URL) + htmlUrl)
                .headers("User-Agent", "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.81 Mobile Safari/537.36")
                .headers("Accept", "text/html,application/xhtml")
                .converter(object : JsonConvert<List<VideoDetail>>() {

                    override fun convertResponse(response: Response): List<VideoDetail>? {
                        val document = Jsoup.parse(response.body()?.string())
                        val playListElements = document.body().select("div.playlist")
                        return playListElements.map { player ->
                            val source = player.select("div.stit>div.from").text()
                            val videoListElement = player.select("ul.ulli>div.stab_list li a")
                            val videoList = videoListElement.map { videoItem ->
                                val title = videoItem.attr("title")
                                val url = videoItem.attr("href")
                                VideoDetail.VideoList(title, url)
                            }
                            VideoDetail(source, videoList)
                        }
                    }

                })
                .adapt(ObservableBody<List<VideoDetail>>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { list ->
                    Log.e("----map--thread", Thread.currentThread().name)
                    val arrayList = arrayListOf<MultiVideoDetail>()
                    list.forEach { item ->
                        arrayList.add(MultiVideoDetail(item.source, null))
                        item.videoList.forEach {
                            arrayList.add(MultiVideoDetail(null, it))
                        }
                    }
                    arrayList
                }
                .subscribe({
                    Log.e("list------data", it.joinToString())
                    view?.onLoadData(it)
                }, {
                    it.printStackTrace()
                }, {
                    view?.dismissLoading()
                })
    }

    @SuppressLint("CheckResult")
    override fun loadVideoUrl(videoUrl: String) {
        OkGo.get<String>(MMKV.defaultMMKV().decodeString(AppConstant.YIFANG_URL) + videoUrl)
                .headers("User-Agent", "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.81 Mobile Safari/537.36")
                .headers("Accept", "text/html,application/xhtml")
                .converter {
                    val document = Jsoup.parse(it.body()?.string())
                    val playerElement = document.body().select("div.wrap1 div.box1 p script").first()
                    playerElement.html().run {
                        substring(this.indexOf("now=\"") + 5, this.indexOf("\";"))
                    }
                }
                .adapt(ObservableBody<String>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Log.e("-----视频地址", it)
                    view?.onLoadVideoUrl(it)
                }, {
                    it.printStackTrace()
                }, {

                })
    }
}