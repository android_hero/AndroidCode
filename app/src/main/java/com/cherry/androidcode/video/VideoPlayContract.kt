package com.cherry.androidcode.video

import com.cherry.androidcode.entity.MultiVideoDetail
import com.cherry.androidcode.mvp1.BasePresenter
import com.cherry.androidcode.mvp1.BaseView

/**
 * @author Cherry(dongmiansheng@gmail.com)
 * @since 2019/8/31
 */

interface VideoPlayContract {

    interface View : BaseView {
        fun onLoadData(list: List<MultiVideoDetail>)
        fun onLoadVideoUrl(videoUrl: String)
    }

    interface Presenter : BasePresenter<View> {
        fun loadData(htmlUrl: String)
        fun loadVideoUrl(videoUrl: String)
    }

}