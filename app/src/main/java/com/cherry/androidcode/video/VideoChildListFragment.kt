package com.cherry.androidcode.video

import android.content.Intent
import androidx.recyclerview.widget.GridLayoutManager
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.module.LoadMoreModule
import com.cherry.androidcode.R
import com.cherry.androidcode.entity.VideoSearchList
import com.cherry.androidcode.recyclerView.ListViewFragment
import com.cherry.androidcode.recyclerView.QuickViewHolder
import com.cherry.androidcode.util.resToColor
import com.parkingwang.android.view.SwStateToast
import com.yanyusong.y_divideritemdecoration.Y_Divider
import com.yanyusong.y_divideritemdecoration.Y_DividerBuilder
import com.yanyusong.y_divideritemdecoration.Y_DividerItemDecoration

class VideoChildListFragment : ListViewFragment<VideoSearchList, VideoChildListContract.View, VideoChildListPresenter>(), VideoChildListContract.View {

    var keyword: String = ""
        set(value) {
            field = value
            resetRefresh()
        }

    override val enRefresh = false

    override val layoutManager by lazy {
        GridLayoutManager(context, 2)
    }

    override val dividerDecoration by lazy {
        object : Y_DividerItemDecoration(activity) {
            override fun getDivider(itemPosition: Int): Y_Divider {
                //note 这里的尺寸参数是dp，不是px
                val paddingColor = R.color.transparent.resToColor()
                return Y_DividerBuilder()
                        .setTopSideLine(itemPosition == 0 || itemPosition == 1, paddingColor, 10f, 0f, 0f)
                        //不包括加载更多的view
                        .setBottomSideLine(itemPosition != adapter.itemCount - 1, paddingColor, 10f, 0f, 0f)
                        .setLeftSideLine(true, paddingColor, 10f, 0f, 0f)
                        .setRightSideLine(itemPosition % 2 != 0, paddingColor, 10f, 0f, 0f)
                        .create()
            }
        }
    }

    class VideoAdapter : BaseQuickAdapter<VideoSearchList, QuickViewHolder>(R.layout.item_test_image_simple), LoadMoreModule {
        override fun convert(helper: QuickViewHolder, item: VideoSearchList) {
            helper.setImageWithGlide(R.id.iv, item.videoCover)
                    .setText(R.id.tv_author, item.videoName)
                    .setText(R.id.tv_date, item.area)
        }
    }

    override val adapter: BaseQuickAdapter<VideoSearchList, QuickViewHolder> = VideoAdapter()

    override fun initRecyclerView() {
        page = 1
        adapter.setOnItemClickListener { _, _, position ->
            startActivity(Intent(activity, VideoPlayActivity::class.java).apply {
                val item = adapter.getItem(position)
                putExtra("videoName", item?.videoName)
                putExtra("videoLink", item?.videoLink)
                putExtra("videoCover", item?.videoCover)
            })
        }
    }

    override fun reload() {
        keyword.takeIf {
            it.isNotEmpty()
        }?.let {
            showLoading()
            mvpPresenter?.searchData(keyword, page, true)
        }
    }

    override fun loadNext() {
        mvpPresenter?.searchData(keyword, page, false)
    }

    override fun createPresenter(): VideoChildListPresenter = VideoChildListPresenter()

    override fun createView(): VideoChildListContract.View = this

    override fun onLoadData(list: MutableList<VideoSearchList>, isRefresh: Boolean) {
        dismissLoading()
        updateList(list, isRefresh)
    }

    override fun onLoadDataError(msg: String, isRefresh: Boolean) {
        dismissLoading()
        SwStateToast.failed().showLong(msg)
    }

}
