package com.cherry.androidcode.code.mvp1;

import android.os.Bundle;


//第二重代理->目标对象->针对的是Activity生命周期
public class MvpActivityDelegateImpl_7<V extends MvpView_7, P extends MvpPresenter_7<V>> implements MvpActivityDelegate_7<V, P> {

    private ProxyMvpCallback<V, P> proxyMvpCallback;
    private MvpCallback_7<V, P> mvpCallback;

    public MvpActivityDelegateImpl_7(MvpCallback_7<V, P> mvpCallback){
        this.mvpCallback = mvpCallback;
        if (mvpCallback == null){
            throw new NullPointerException("不能够为空");
        }
        this.proxyMvpCallback = new ProxyMvpCallback<>(mvpCallback);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //绑定处理
        this.proxyMvpCallback.createPresenter();
        this.proxyMvpCallback.createView();
        this.proxyMvpCallback.attachView();
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onRestart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {
        this.proxyMvpCallback.detachView();
    }

}
