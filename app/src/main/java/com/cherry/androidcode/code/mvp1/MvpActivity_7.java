package com.cherry.androidcode.code.mvp1;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;


//第一重代理->目标对象->实现目标接口(MvpCallback)---->Activity抽象类
//第二重代理->代理对象->MvpActivity_7
public abstract class MvpActivity_7<V extends MvpView_7, P extends MvpPresenter_7<V>>
        extends AppCompatActivity implements MvpCallback_7<V, P> {

    private P presenter;
    private V view;

    //第二重代理:持有目标对象引用
    //代理对象持有目标对象引用
    private MvpActivityDelegateImpl_7<V, P> delegateImpl;

    public MvpActivityDelegateImpl_7<V, P> getDelegateImpl() {
        if (delegateImpl == null) {
            this.delegateImpl = new MvpActivityDelegateImpl_7<V, P>(this);
        }
        return delegateImpl;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDelegateImpl().onCreate(savedInstanceState);
    }

    //fixme 这里最好自动创建，不用每次手动创建
//    @Override
//    public P createPresenter() {
//        Type type = getClass().getGenericSuperclass();
//        if (type instanceof ParameterizedType) {
//            Type[] typeArguments = ((ParameterizedType) type).getActualTypeArguments();
//            Class<P> clzz = (Class<P>) typeArguments[1];
//            try {
//                return clzz.newInstance();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        return null;
//    }
//
//    @Override
//    public V createView() {
//        Type type = getClass().getGenericSuperclass();
//        if (type instanceof ParameterizedType) {
//            Type[] typeArguments = ((ParameterizedType) type).getActualTypeArguments();
//            Class<V> clzz = (Class<V>) typeArguments[0];
//            try {
//                return clzz.newInstance();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        return null;
//    }

    @Override
    public P getPresenter() {
        return presenter;
    }

    @Override
    public void setPresenter(P presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setMvpView(V view) {
        this.view = view;
    }

    @Override
    public V getMvpView() {
        return this.view;
    }

    @Override
    protected void onStart() {
        super.onStart();
        getDelegateImpl().onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getDelegateImpl().onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getDelegateImpl().onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        getDelegateImpl().onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        getDelegateImpl().onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getDelegateImpl().onDestroy();
    }
}
