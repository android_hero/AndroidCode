package com.cherry.androidcode.code.mvp1.test;

import com.cherry.androidcode.code.mvp1.MvpBasePresenter_7;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * @author DongMS
 * @since 2019/7/11
 */
public class TestMvpPresenter extends MvpBasePresenter_7<TestMvpContract.View> implements TestMvpContract.Presenter {

    @Override
    public void login() {
        Flowable.just("登陆成功")
                .delay(3, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onSubscribe(Subscription s) {
                        s.request(Long.MAX_VALUE);
                    }

                    @Override
                    public void onNext(String s) {
                        getView().login(s);
                    }

                    @Override
                    public void onError(Throwable t) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}
