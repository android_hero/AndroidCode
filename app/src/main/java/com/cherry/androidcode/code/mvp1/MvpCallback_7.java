package com.cherry.androidcode.code.mvp1;


//第一重代理->目标接口->抽象解绑和绑定(MvpCallback)
public interface MvpCallback_7<V extends MvpView_7, P extends MvpPresenter_7<V>> {

    //创建Presenter
    P createPresenter();

    //创建View
    V createView();

    void setPresenter(P presenter);

    P getPresenter();

    void setMvpView(V view);

    V getMvpView();
}
