package com.cherry.androidcode.code.mvp1.test;

import com.cherry.androidcode.code.mvp1.MvpPresenter_7;
import com.cherry.androidcode.code.mvp1.MvpView_7;

/**
 * @author DongMS
 * @since 2019/7/11
 */
public interface TestMvpContract {

    interface View extends MvpView_7 {
        void login(String msg);
    }

    interface Presenter extends MvpPresenter_7<View> {
        void login();
    }

}
