package com.cherry.androidcode.code.mvp1.test;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.cherry.androidcode.code.mvp1.MvpActivity_7;

/**
 * @author DongMS
 * @since 2019/7/11
 */
public class TestMvpActivity extends MvpActivity_7<TestMvpContract.View, TestMvpPresenter> implements TestMvpContract.View {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPresenter().login();
    }

    @Override
    public void login(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public TestMvpPresenter createPresenter() {
        return new TestMvpPresenter();
    }

    @Override
    public TestMvpContract.View createView() {
        return this;
    }
}
