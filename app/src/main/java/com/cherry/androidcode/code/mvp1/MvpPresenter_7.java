package com.cherry.androidcode.code.mvp1;


//高度抽象UI层接口
public interface MvpPresenter_7<V extends MvpView_7> {

    void attachView(V view);

    void detachView();

}
