package com.cherry.androidcode.code.mvp1;

import android.os.Bundle;


//第二重代理->目标接口->针对Activity生命周期进行代理
public interface MvpActivityDelegate_7<V extends MvpView_7, P extends MvpPresenter_7<V>> {

    public void onCreate(Bundle savedInstanceState);

    public void onStart();

    public void onPause();

    public void onResume();

    public void onRestart();

    public void onStop();

    public void onDestroy();

}
