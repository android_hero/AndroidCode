package com.cherry.androidcode.code.others;

import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * @author DongMS
 * @since 2019/7/16
 */
public class CodeBlock {


    /**
     * 针对recyclerView LayoutManager是LinearManager，垂直滚动的距离计算
     * 当然，如果recyclerView没有插入/删除/移动 Item
     * 可以直接使用
     * RecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
     *             private int totalDy = 0;
     *             @Override
     *             public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
     *                 totalDy += dy;
     *             }
     *}
     * @param recyclerView
     * @return
     */
    private int getRecyclerViewScroll(RecyclerView recyclerView) {
        if (recyclerView == null) {
            return 0;
        }
        RecyclerView.LayoutManager lm = recyclerView.getLayoutManager();
        if (lm instanceof LinearLayoutManager) {
            LinearLayoutManager layoutManager = (LinearLayoutManager) lm;
            int position = layoutManager.findFirstVisibleItemPosition();
            View firstVisibleChildView = lm.findViewByPosition(position);
            if (firstVisibleChildView != null) {
                int itemHeight = firstVisibleChildView.getHeight();
                return (position) * itemHeight - firstVisibleChildView.getTop();
            }
        }
        return 0;
    }


}
