package com.cherry.androidcode.code.mvp1;


//第一重代理->代理对象->ProxyMvpCallback(实现目标接口)
//直接封装MVP实现
public class ProxyMvpCallback<V extends MvpView_7, P extends MvpPresenter_7<V>> implements MvpCallback_7<V, P> {

    //持有目标对象引用->Activity
    //mvpCallback->本质就是Actiivty
    private MvpCallback_7<V, P> mvpCallback;

    public ProxyMvpCallback(MvpCallback_7<V, P> mvpCallback){
        this.mvpCallback = mvpCallback;
    }

    @Override
    public P createPresenter() {
        P presenter = mvpCallback.getPresenter();
        if (presenter == null) {
            presenter = mvpCallback.createPresenter();
        }
        if (presenter == null) {
            throw new NullPointerException("Presenter is not null!");
        }

        // 绑定
        mvpCallback.setPresenter(presenter);
        return getPresenter();
    }

    @Override
    public V createView() {
        V view = mvpCallback.getMvpView();
        if (view == null) {
            view = mvpCallback.createView();
        }
        if (view == null) {
            throw new NullPointerException("Presenter is not null!");
        }

        // 绑定
        mvpCallback.setMvpView(view);
        return getMvpView();
    }

    @Override
    public void setMvpView(V view) {
        this.mvpCallback.setMvpView(view);
    }

    /**
     * 获取presenter
     *
     * @return
     */
    @Override
    public P getPresenter() {
        P presenter = mvpCallback.getPresenter();
        if (presenter == null) {
            // 抛异常
            throw new NullPointerException("Presenter is not null!");
        }
        return presenter;
    }

    @Override
    public void setPresenter(P presenter) {
        mvpCallback.setPresenter(presenter);
    }

    @Override
    public V getMvpView() {
        return mvpCallback.getMvpView();
    }

    public void attachView(){
        getPresenter().attachView(getMvpView());
    }

    public void detachView(){
        getPresenter().detachView();
    }

}
