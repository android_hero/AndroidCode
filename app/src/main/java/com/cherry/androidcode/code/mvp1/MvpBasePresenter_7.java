package com.cherry.androidcode.code.mvp1;

public class MvpBasePresenter_7<V extends MvpView_7> implements MvpPresenter_7<V> {

    private V view;

    public V getView() {
        return view;
    }

    public void attachView(V view){
        this.view = view;
    }

    public void detachView(){
        this.view = null;
    }

}
