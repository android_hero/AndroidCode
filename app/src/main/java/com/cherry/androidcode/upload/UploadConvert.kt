package com.cherry.androidcode.upload

import android.content.Context
import android.net.Uri
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

/**
 * 上传文件参数转换
 * Multipart
 */

typealias progressCallback = (progress: Int, currentSize: Long, totalSize: Long) -> Unit

object UploadConvert {

    //--------------------通过MultiPart上传

    fun convertTextMap(textMap: Map<String, String>): Map<String, RequestBody> =
        textMap.mapValues {
            //注释也可以
//            RequestBody.create(MediaType.parse("text/plain"), it.value)
            RequestBody.create(null, it.value)
        }


    fun convertTextPart(textMap: Map<String, String>) =
        //注释也可以
        textMap.map {
            MultipartBody.Part.createFormData(it.key, it.value)
        }

//        MultipartBody.Builder().apply {
//            textMap.map {
//                addFormDataPart(it.key, it.value)
//            }
//        }.build().parts()

    fun convertFileParamsByUri(context: Context, key: String, fileList: List<Uri>) =
        fileList.map {
            it.asPart1(context, key)
        }

    fun convertFileParamsByFile(key: String, fileList: List<File>) =
        fileList.map {
            val upFile = UpFile1(key, it)
            val file: File = upFile.file
            require(file.exists()) { "File '" + file.absolutePath + "' does not exist" }
            require(file.isFile) { "File '" + file.absolutePath + "' is not a file" }

            val requestBody: RequestBody = FileRequestBody1(
                upFile.file, upFile.skipSize,
                BuildUtil1.getMediaType(upFile.filename)
            )
            MultipartBody.Part.createFormData(upFile.key, upFile.filename, requestBody)
        }

    //--------------------通过RequestBody上传

    fun convertRequestBodyByFile(
        textMap: Map<String, String>,
        key: String,
        fileList: List<File>,
        callback: progressCallback? = null
    ) =
        MultipartBody.Builder().apply {
            setType(MultipartBody.FORM)
            textMap.forEach {
                addFormDataPart(it.key, it.value)
            }
            fileList.forEach {
                val upFile = UpFile1(key, it)
                val file: File = upFile.file
                require(file.exists()) { "File '" + file.absolutePath + "' does not exist" }
                require(file.isFile) { "File '" + file.absolutePath + "' is not a file" }

                val fileRequestBody =
                    RequestBody.create(BuildUtil1.getMediaType(upFile.filename), it)
                addFormDataPart(key, it.name, fileRequestBody)
            }
        }.build().let {
            if (callback == null) {
                it
            } else {
                ProgressRequestBody1(it, callback)
            }
        }

    fun convertRequestBodyByUri(
        context: Context,
        textMap: Map<String, String>,
        key: String,
        fileList: List<Uri>,
        callback: progressCallback? = null
    ) =
        MultipartBody.Builder().apply {
            setType(MultipartBody.FORM)
            textMap.forEach {
                addFormDataPart(it.key, it.value)
            }
            fileList.forEach {
                val fileRequestBody = it.asRequestBody1(context)
                addFormDataPart(key, it.displayName(context), fileRequestBody)
            }

        }.build().let {
            if (callback == null) {
                it
            } else {
                ProgressRequestBody1(it, callback)
            }
        }

}