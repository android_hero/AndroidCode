package com.cherry.androidcode.upload;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;

import java.net.URLConnection;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.MultipartBody.Part;
import okhttp3.Request;
import okhttp3.RequestBody;
import rxhttp.wrapper.annotations.NonNull;
import rxhttp.wrapper.annotations.Nullable;
import rxhttp.wrapper.entity.KeyValuePair;
import rxhttp.wrapper.param.IRequest;
import rxhttp.wrapper.utils.BuildUtil;

/**
 * User: ljx
 * Date: 2017/12/1
 * Time: 18:36
 */
public class BuildUtil1 {

    //For compatibility with okHTTP 3.x version, only written in Java
    public static MediaType getMediaType(@Nullable String filename) {
        if (filename == null) return null;
        int index = filename.lastIndexOf(".") + 1;
        String fileSuffix = filename.substring(index);
        String contentType = URLConnection.guessContentTypeFromName(fileSuffix);
        return contentType != null ? MediaType.parse(contentType) : MediaType.parse("application/octet-stream");
    }

    //For compatibility with okHTTP 3.x version, only written in Java
    public static MediaType getMediaTypeByUri(Context context, Uri uri) {
        if (uri.getScheme().equals(ContentResolver.SCHEME_FILE)) {
            return getMediaType((uri.getLastPathSegment()));
        } else {
            String contentType = context.getContentResolver().getType(uri);
            return contentType != null ? MediaType.parse(contentType) : MediaType.parse("application/octet-stream");
        }
    }
}
