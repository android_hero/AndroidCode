package com.cherry.androidcode.retrofit.api

import com.cherry.androidcode.entity.HttpResult
import com.cherry.androidcode.entity.MallHome
import retrofit2.http.GET
import retrofit2.http.Url

interface MockyApi {

    @GET
    suspend fun mallHome(@Url url: String): HttpResult<MallHome>

}