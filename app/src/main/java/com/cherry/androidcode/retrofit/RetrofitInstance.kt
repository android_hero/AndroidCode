package com.cherry.androidcode.retrofit

import com.parkingwang.okhttp3.LogInterceptor.LogInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitInstance {

    fun defaultInstance(baseurl: String = "http://ww.baidu.com/", log: Boolean = true): Retrofit =
        Retrofit.Builder()
            .baseUrl(baseurl)
            .client(
                OkHttpClient.Builder().let {
                    if (log){
                        it.addInterceptor(LogInterceptor())
                    }
                    it.build()
                }
            )
            .addConverterFactory(GsonConverterFactory.create())
            .build()


}