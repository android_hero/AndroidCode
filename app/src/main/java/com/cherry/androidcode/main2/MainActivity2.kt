package com.cherry.androidcode.main2

import com.cherry.androidcode.R
import com.cherry.androidcode.base.BaseActivity

/**
 * @author DongMS
 * @since 2019/9/6
 */
class MainActivity2 : BaseActivity() {

    override val activityLayoutRes = R.layout.activity_main2

}