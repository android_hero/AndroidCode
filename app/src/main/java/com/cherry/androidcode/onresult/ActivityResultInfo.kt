package com.cherry.androidcode.onresult

import android.content.Intent

/**
 * @author 董棉生(dongmiansheng@parkingwang.com)
 * @since 18-9-27
 */

data class ActivityResultInfo(val resultCode: Int, val data: Intent?)