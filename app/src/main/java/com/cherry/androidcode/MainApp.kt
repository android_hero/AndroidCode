package com.cherry.androidcode

import android.app.Application
import android.app.Notification
import android.content.Context
import android.util.Log
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.multidex.MultiDex
import com.cherry.androidcode.jetpack.lifecycle.LifeCycleAppObserverTest
import com.cherry.androidcode.util.GlobalGson
import com.chibatching.kotpref.Kotpref
import com.chibatching.kotpref.gsonpref.gson
import com.parkingwang.android.SwUtils
import com.tencent.mmkv.MMKV
import com.umeng.commonsdk.UMConfigure
import com.umeng.message.IUmengRegisterCallback
import com.umeng.message.PushAgent
import com.umeng.message.UmengMessageHandler
import com.umeng.message.UmengNotificationClickHandler
import com.umeng.message.entity.UMessage
import dagger.hilt.android.HiltAndroidApp
import org.android.agoo.mezu.MeizuRegister
import org.android.agoo.xiaomi.MiPushRegistar
import rxhttp.RxHttpPlugins
import kotlin.properties.Delegates

/**
 * @author dongmiansheng@parkingwang.com
 * @since 2019/6/7
 */
@HiltAndroidApp
class MainApp : Application() {

    override fun onCreate() {
        super.onCreate()
        CONTEXT = this
        MultiDex.install(this)

        SwUtils.init(this)

        RxHttpPlugins.init(RxHttpPlugins.getOkHttpClient()).setDebug(true)

        //针对整个应用程序的监听，与Activity数量无关
        //create只会调用一次，destroy永远不会被调用
        ProcessLifecycleOwner.get().lifecycle.addObserver(LifeCycleAppObserverTest())

        //1.SharePreference封装库，先初始化
        Kotpref.init(this)
        //要支持Gson加这句
        Kotpref.gson = GlobalGson

        //MMKV初始化
        Log.e("MMKV存放文件地址", MMKV.initialize(this))
        //一些 Android 设备（API level 19）在安装/更新 APK 时可能出错, 导致 libmmkv.so 找不到。
        // 然后就会遇到 java.lang.UnsatisfiedLinkError 之类的 crash
        //要申请读写存储卡权限
//        val path = "${Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)}/testMMKV"
//        val file = File(path)
//        if (!file.exists()){
//            file.mkdirs()
//        }
//        Log.e("MMKV存放文件地址", MMKV.initialize(path) {
//            ReLinker.loadLibrary(CONTEXT, it)
//        })

        // 在此处调用基础组件包提供的初始化函数 相应信息可在应用管理 -> 应用信息 中找到 http://message.umeng.com/list/apps
// 参数一：当前上下文context；
// 参数二：应用申请的Appkey（需替换）；
// 参数三：渠道名称；
// 参数四：设备类型，必须参数，传参数为UMConfigure.DEVICE_TYPE_PHONE则表示手机；传参数为UMConfigure.DEVICE_TYPE_BOX则表示盒子；默认为手机；
// 参数五：Push推送业务的secret 填充Umeng Message Secret对应信息（需替换）
        UMConfigure.init(
            this,
            "5f2a7f553fa0c96d2bde54b1",
            "Umeng",
            UMConfigure.DEVICE_TYPE_PHONE,
            "0145afee4fb95a21de47608a5787133c"
        )

        //获取消息推送代理示例（这里有个坑，必须放Application里面）
        val mPushAgent = PushAgent.getInstance(this)
        //注册推送服务，每次调用register方法都会回调该接口
        mPushAgent.register(object : IUmengRegisterCallback {
            override fun onSuccess(deviceToken: String) {
                //注册成功会返回deviceToken deviceToken是推送消息的唯一标志。消息推送生成的用于标识设备的id，长度为44位，不能定制和修改。
                Log.i("友盟", "注册成功：deviceToken：-------->  $deviceToken")
            }

            override fun onFailure(s: String, s1: String) {
                Log.e("友盟", "注册失败：-------->  s:$s,s1:$s1")
            }
        })
        //有关集成问题的toast提示：如果您的app测试完毕，需要正式上线时，可以通过UMConfigure.setLogEnabled(false)接口关闭sdk的相关提示，这时就不会有toast相关提示
        UMConfigure.setLogEnabled(BuildConfig.DEBUG)
        //自定义参数将通过extra字段发送到客户端
        //1.消息到达时获取自定义参数,设置这个回调后app在前台时居然没有通知但是有回调！！！
//        mPushAgent.messageHandler = object : UmengMessageHandler() {
//            override fun getNotification(context: Context, msg: UMessage): Notification {
//                printMsgExtra("push_extra", msg)
//                return super.getNotification(context, msg)
//            }
//        }
        //2.消息点击时获取自定义参数，对应api的几种点击模式
        mPushAgent.notificationClickHandler = object : UmengNotificationClickHandler() {

            //            [msg={"display_type":"notification","extra":{"type":"1","target_id":"1000"},
//            "body":{"after_open":"go_app","ticker":"魅族前台测试777777","title":"魅族前台测试777777","play_sound":"true","play_lights":"false","play_vibrate":"false","text":"魅族前台测试7777777777"},"msg_id":"umbirpe160066819140810"}, action=10]
            override fun launchApp(context: Context, msg: UMessage) {
                printMsgExtra("launchApp", msg)
                super.launchApp(context, msg)
            }

            override fun openUrl(context: Context, msg: UMessage) {
                printMsgExtra("openUrl", msg)
                super.openUrl(context, msg)
            }

            override fun openActivity(context: Context, msg: UMessage) {
                printMsgExtra("openActivity", msg)
                super.openActivity(context, msg)
            }

            override fun dealWithCustomAction(context: Context, msg: UMessage) {
                printMsgExtra("dealWithCustomAction", msg)
                super.dealWithCustomAction(context, msg)
            }

        }
        /**
         *
         *  3.进入Activity时获取自定义参数
         *  Set<String> keySet = bun.keySet();
        for (String key : keySet) {
        String value = bun.getString(key);
        ...
        }
         */

        MeizuRegister.register(this, "134980", "1fe26502f4494064899c467afcb12317")

        MiPushRegistar.register(this, "2882303761518697476", "5741869745476")
    }

    private fun printMsgExtra(tag: String, msg: UMessage) {
        msg.extra.entries.forEach {
            Log.w(tag, "key=${it.key}---value = ${it.value}")
        }
    }

    companion object {

        @JvmStatic
        var CONTEXT: Context by Delegates.notNull()
    }

}