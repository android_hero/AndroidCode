package com.cherry.androidcode.module

import com.cherry.androidcode.entity.Engine
import com.cherry.androidcode.entity.GasEngine
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

//接口注入必须是个抽象类
@Module
@InstallIn(ActivityComponent::class)
abstract class EngineModule {

    //函数名随便，参数是具体实现类，返回值一定是接口
    @Binds
    abstract fun bindGasEngine(gasEngine: GasEngine): Engine

}