package com.cherry.androidcode.module

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.BINARY)//该注解在编译之后会得到保留，但是无法通过反射去访问这个注解
annotation class BindGasEngine
