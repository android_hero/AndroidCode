package com.cherry.androidcode.module

import android.app.Application
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.internal.managers.ApplicationComponentManager
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Singleton

//第三方依赖注入，不需要抽象
@Module
//Activity中包含的Fragment和View也可以使用，但是除了Activity、Fragment、View之外的其他地方就无法使用了
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Singleton
    @Provides
    fun initOkhttp(): OkHttpClient {
        return OkHttpClient.Builder().build()
    }

    @Singleton
    //参数由initOkhttp()注入提供
    @Provides
    fun initRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl("http://www.baidu.com")
            .build()
    }

}