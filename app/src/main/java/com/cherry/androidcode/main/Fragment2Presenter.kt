package com.cherry.androidcode.main

import android.annotation.SuppressLint
import com.cherry.androidcode.mvp1.MVPBasePresenter
import com.cherry.androidcode.recyclerView.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

/**
 * @author DongMS
 * @since 2019/7/11
 */
class Fragment2Presenter : MVPBasePresenter<Fragment2Contract.View>(), Fragment2Contract.Presenter {

    @SuppressLint("CheckResult")
    override fun loadCategory() {
        /////////test////////
        Observable.just(listOf(
                Fragment2.TabBean("Demo", DemoFragment.newInstance(DemoFragment.MAIN_FRAGMENT2_TAG)),
                Fragment2.TabBean("侧滑菜单", TestSwipeMenuFragment()),
                Fragment2.TabBean("多选", TestMultiSelectFragment()),
                Fragment2.TabBean("单选", TestSingeSelectFragment()),
                Fragment2.TabBean("妹子", TestGridLayoutFragment()),
                Fragment2.TabBean("一般的列表", TestNoRefreshLoadFragment()),
                Fragment2.TabBean("刷新的列表", TestNoLoadFragment()),
                Fragment2.TabBean("刷新加载更多的列表", TestRefreshLoadFragment()),
                Fragment2.TabBean("tabtest", StateFragment.newInstance("tabtest"))
        ))
                .delay(1, TimeUnit.SECONDS)//测试
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    view?.loadCategory(it)
                }.let { addSubscribe(it) }
        /////////test////////
    }
}