package com.cherry.androidcode.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentStatePagerAdapter
import com.cherry.androidcode.R
import com.cherry.androidcode.mvp1.MVPFragment
import com.parkingwang.android.view.SwStateToast
import kotlinx.android.synthetic.main.fragment_main1_fragment2.*

class Fragment2 : MVPFragment<Fragment2Contract.View, Fragment2Presenter>(), Fragment2Contract.View {

    override val contentLayout = R.layout.fragment_main1_fragment2

    override fun createPresenter() = Fragment2Presenter()

    override fun createView() = this

    override fun onFragmentFirstVisible() {
        //获取tab数据设置
        showLoading()
        mvpPresenter?.loadCategory()
    }

    override fun loadCategory(data: List<TabBean>) {
        dismissLoading()
        SwStateToast.success().show("请求成功")
        repeat(data.size) {
            tabLayout.addTab(tabLayout.newTab())
        }

        val fragmentList = data.map {
            it.fragment
        }

        val titles = data.map {
            it.title
        }

        /**
         * FragmentStatePagerAdapter   ：
         * 内部没有缓存fragment，每次都是新对象，可以自己手动维护fragment缓存。
         * 切换不同的Fragment的时候，我们会把前面的Fragment销毁，而我们系统在销毁前，
         * 会把我们的我们Fragment的Bundle在我们的onSaveInstanceState(Bundle)保存下来。
         * 等用户切换回来的时候，我们的Fragment就会根据我们的instance state恢复出来
         *
         * FragmentPagerAdapter ：
         *在切换的时候，不会销毁，而只是调用事务中的detach方法，
         * 会把Fragment的view销毁，而保留了以前的Fragment对象。所以通过这种方式创建的Fragment一直不会被销毁。
         *每次切换都会onCreateView，onViewCreated，多次回调影响效率
         *
         * 总结：用命名长的adapter，自己维护fragment缓存，保证每个fragment切换时view维持原来的状态
         */
        viewPager.adapter = object : FragmentStatePagerAdapter(childFragmentManager) {
            override fun getItem(position: Int) = fragmentList[position]

            override fun getCount() = fragmentList.size

            /**
             * 返回tab标题，忘了写tab标题显示不出
             */
            override fun getPageTitle(position: Int): CharSequence? {
                return titles[position]
            }

        }
        //设置缓存fragment，这样每次切换就不会销毁fragment了
        viewPager.offscreenPageLimit = fragmentList.size
        tabLayout.setupWithViewPager(viewPager)
    }

    companion object {
        fun newInstance(content: String): Fragment2 {
            val fragment = Fragment2()
            fragment.arguments = Bundle().apply {
                putString("content", content)
            }
            return fragment
        }
    }


    data class TabBean(val title: String, val fragment: Fragment)
}
