package com.cherry.androidcode.main

import android.util.Log
import com.cherry.androidcode.ApiUrl
import com.cherry.androidcode.entity.HttpResult
import com.cherry.androidcode.entity.MallHome
import com.cherry.androidcode.entity.MallHomeMultiple
import com.cherry.androidcode.mvp1.MVPBasePresenter
import com.cherry.androidcode.okgo.BaseObserver
import com.cherry.androidcode.okgo.JsonConvert
import com.cherry.androidcode.retrofit.RetrofitInstance
import com.cherry.androidcode.retrofit.api.MockyApi
import com.lzy.okgo.OkGo
import com.lzy.okrx2.adapter.ObservableBody
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

/**
 * @author DongMS
 * @since 2019/7/16
 */
class MainPresenter : MVPBasePresenter<MainContract.View>(), MainContract.Presenter {

    val scope = MainScope()

    override fun loadData() {
//        OkGo.get<HttpResult<MallHome>>(ApiUrl.MALL_HOME_URL)
//                .converter(object : JsonConvert<HttpResult<MallHome>>() {})
//                .adapt(ObservableBody<HttpResult<MallHome>>())
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(object : BaseObserver<MallHome>(this) {
//                    override fun onSuccess(data: MallHome) {
//                        view?.setMultipleItemView(arrayListOf<MallHomeMultiple<*>>().apply {
//                            add(MallHomeMultiple(data.adBannerData, MallHomeMultiple.TOP_AD))
////                            add(MallHomeMultiple(data.adData, MallHomeMultiple.CENTER_AD))
////                            add(MallHomeMultiple(data.bannerData, MallHomeMultiple.BOTTOM_AD))
//                            add(MallHomeMultiple(data.classData, MallHomeMultiple.CLASS_PRODUCT))
////                            add(MallHomeMultiple(data.hotProductData, MallHomeMultiple.HOT_PRODUCT))
////                            add(MallHomeMultiple(data.storeData, MallHomeMultiple.STORE))
//                            add(MallHomeMultiple(data.productData, MallHomeMultiple.NEW_PRODUCT))
//                        })
//                    }
//                })

        scope.launch {
            view?.showLoading()
            try {
                val data = RetrofitInstance.defaultInstance().create(MockyApi::class.java)
                        .mallHome(ApiUrl.MALL_HOME_URL).data
                    view?.dismissLoading()
                    view?.setMultipleItemView(arrayListOf<MallHomeMultiple<*>>().apply {
                        add(MallHomeMultiple(data.adBannerData, MallHomeMultiple.TOP_AD))
//                            add(MallHomeMultiple(data.adData, MallHomeMultiple.CENTER_AD))
//                            add(MallHomeMultiple(data.bannerData, MallHomeMultiple.BOTTOM_AD))
                        add(MallHomeMultiple(data.classData, MallHomeMultiple.CLASS_PRODUCT))
//                            add(MallHomeMultiple(data.hotProductData, MallHomeMultiple.HOT_PRODUCT))
//                            add(MallHomeMultiple(data.storeData, MallHomeMultiple.STORE))
                        add(MallHomeMultiple(data.productData, MallHomeMultiple.NEW_PRODUCT))
                    })
            } catch (exception: Exception) {
                exception.printStackTrace()
            }finally {
                view?.dismissLoading()
            }
        }

    }

    override fun testLoadMoreNewProduct(page: Int) {
        Log.e("加载更多", page.toString())
        OkGo.get<HttpResult<MallHome>>(ApiUrl.MALL_HOME_URL)
                .converter(object : JsonConvert<HttpResult<MallHome>>() {})
                .adapt(ObservableBody<HttpResult<MallHome>>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : BaseObserver<MallHome>(this) {
                    override fun onSuccess(data: MallHome) {
                        view?.onTestLoadMoreNewProduct(data.productData)
                    }
                })
    }

}