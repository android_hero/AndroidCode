package com.cherry.androidcode.main.provider

import android.util.Log
import android.view.Gravity
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.cherry.androidcode.R
import com.cherry.androidcode.entity.MallHome
import com.cherry.androidcode.entity.MallHomeMultiple
import com.cherry.androidcode.image.BannerImageLoader
import com.youth.banner.Banner
import com.youth.banner.loader.ImageLoader

/**
 * @author DongMS
 * @since 2019/7/16
 */
class MainTopAdProvider : AbsBaseItemProvider<MallHomeMultiple<*>>() {

    private val bannerImageLoader: ImageLoader

    init {
        bannerImageLoader = BannerImageLoader()
    }

    override val layoutId: Int = R.layout.holder_banner

    override val itemViewType: Int = MallHomeMultiple.TOP_AD

    override fun initView(helper: BaseViewHolder, data: MallHomeMultiple<*>, position: Int) {
        helper.getView<Banner>(R.id.banner).apply {
            setImageLoader(bannerImageLoader)
            setIndicatorGravity(Gravity.CENTER)
            setDelayTime(3000)
        }
    }

    override fun bindDataToViewHolder(helper: BaseViewHolder, data: MallHomeMultiple<*>, position: Int) {
        Log.e("-------", "有脏数据或者刷新")
        data.list?.map {
            it as MallHome.AdBannerData
            it.imgUrl
        }?.let { imgUrls ->
            helper.getView<Banner>(R.id.banner).apply {
                stopAutoPlay()
                setImages(imgUrls)
                start()
            }
        }
    }
}