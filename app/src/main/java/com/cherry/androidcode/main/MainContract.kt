package com.cherry.androidcode.main

import com.cherry.androidcode.entity.MallHome
import com.cherry.androidcode.entity.MallHomeMultiple
import com.cherry.androidcode.mvp1.BasePresenter
import com.cherry.androidcode.mvp1.BaseView

/**
 * @author DongMS
 * @since 2019/7/16
 */
interface MainContract {

    interface View : BaseView {
        fun setMultipleItemView(mallHome: MutableList<MallHomeMultiple<*>>?)

        fun onTestLoadMoreNewProduct(productData: MutableList<MallHome.ProductData>?)
    }

    interface Presenter : BasePresenter<View> {
        fun loadData()

        fun testLoadMoreNewProduct(page: Int)
    }


}