package com.cherry.androidcode.main

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import com.cherry.androidcode.R
import com.cherry.androidcode.base.LazyLoadFragment
import com.parkingwang.android.SwUtils
import kotlinx.android.synthetic.main.fragment_main1_state.*

/**
 * @author 董棉生(dongmiansheng @ parkingwang.com)
 * @since 2019/6/6
 */

class StateFragment : LazyLoadFragment() {

    override val contentLayout = R.layout.fragment_main1_state

    /**
     * 界面可见与否每次都会回调,rootView第一次是null，其他非null
     */
//    override fun onFragmentVisibleChange(isVisible: Boolean) {
//        //fragment如果有必要在每次界面可见刷新数据
//        if (isVisible) {
//            Log.e(this.javaClass.simpleName, "每次都会加载数据...")
//        }
//    }


    /**
     * 如果只需要在初次加载数据，后续不用加载，onFragmentVisibleChange回调中就不要去加载数据了
     * 仅仅第一次可见回调，但rootView是null
     */
    override fun onFragmentFirstVisible() {
        Log.e(this.javaClass.simpleName, "第一次加载数据...")

        //注意，view可能未创建
        Log.e(this.javaClass.simpleName, if (rootView == null) "view created" else "no view")
        //模拟加载数据
        activity?.window?.decorView?.postDelayed({
            Log.e(this.javaClass.simpleName, arguments?.getString("tab"))
            successStateView.showContent()
            successStateView.findViewById<TextView>(R.id.content).text = arguments?.getString("tab")
        }, 2000)
    }

    override fun initView(view: View) {
        super.initView(view)
        successStateView.showLoading()

        successState.setOnClickListener {
            successStateView.showLoading()
            SwUtils.runOnUiThreadDelayed({
                successStateView.showContent()
                successStateView.findViewById<TextView>(R.id.content).text = arguments?.getString("tab")
            }, 1000)
        }

        //重试View的id error_retry_view
        errorState.setOnClickListener {
            error()
        }

        //重试View的id empty_retry_view
        emptyState.setOnClickListener {
            empty()
        }


        //重试View的id no_network_retry_view
        networkState.setOnClickListener {
            noNetwork()
        }

        //重试
        emptyStateView.setOnRetryClickListener { empty() }
        //默认的布局重试用不了。。。
        errorStateView.setOnRetryClickListener { error() }
        networkStateView.setOnRetryClickListener { noNetwork() }

        //状态视图切换监听事件
//        successStateView.setOnViewStatusChangeListener { oldViewStatus, newViewStatus ->  }
    }

    private fun noNetwork() {
        networkStateView.showLoading()
        SwUtils.runOnUiThreadDelayed({
            networkStateView.showNoNetwork()
        }, 1000)
    }

    private fun empty() {
        emptyStateView.showLoading()
        SwUtils.runOnUiThreadDelayed({
            emptyStateView.showEmpty()
        }, 1000)
    }

    private fun error() {
        errorStateView.showLoading()
        SwUtils.runOnUiThreadDelayed({
            errorStateView.showError()
        }, 1000)
    }


    companion object {
        fun newInstance(tabTitle: String): StateFragment {
            val fragment = StateFragment()
            fragment.arguments = Bundle().apply {
                putString("tab", tabTitle)
            }
            return fragment
        }
    }


}
