package com.cherry.androidcode.main.provider

import androidx.recyclerview.widget.RecyclerView
import com.chad.library.adapter.base.provider.BaseItemProvider
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.cherry.androidcode.recyclerView.QuickViewHolder

/**
 * @author DongMS
 * @since 2019/9/20
 */
abstract class AbsBaseItemProvider<T> : BaseItemProvider<T>() {

    protected var isFirstInit = false

    final override fun convert(helper: BaseViewHolder, data: T) {
        var position = helper.adapterPosition
        if (position == RecyclerView.NO_POSITION) {
            return
        }
        position -= getAdapter()?.headerLayoutCount ?: 0
        if (!isFirstInit) {
            //初始化一些不变的配置
            initView(helper, data, position)
            isFirstInit = true
        }
        bindDataToViewHolder(helper, data, position)
    }

    protected abstract fun bindDataToViewHolder(helper: BaseViewHolder, data: T, position: Int)

    protected open fun initView(helper: BaseViewHolder, data: T, position: Int) {
    }

}