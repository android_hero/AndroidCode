package com.cherry.androidcode.main.provider

import android.graphics.Color
import android.util.Log
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.module.BaseLoadMoreModule
import com.chad.library.adapter.base.module.LoadMoreModule
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.cherry.androidcode.R
import com.cherry.androidcode.entity.MallHome
import com.cherry.androidcode.entity.MallHomeMultiple
import com.cherry.androidcode.main.MainFragment
import com.cherry.androidcode.main1.MainActivity
import com.cherry.androidcode.recyclerView.QuickViewHolder
import com.yanyusong.y_divideritemdecoration.Y_Divider
import com.yanyusong.y_divideritemdecoration.Y_DividerBuilder
import com.yanyusong.y_divideritemdecoration.Y_DividerItemDecoration

/**
 * @author DongMS
 * @since 2019/9/10
 */
class MainBottomNewProductProvider : AbsBaseItemProvider<MallHomeMultiple<*>>() {

    override val layoutId: Int = R.layout.layout_recycler_view

    override val itemViewType: Int = MallHomeMultiple.NEW_PRODUCT

    private lateinit var loadMoreModule: BaseLoadMoreModule


    class MainBottomNewProductAdapter : BaseQuickAdapter<MallHome.ProductData, QuickViewHolder>(R.layout.item_test_image_simple), LoadMoreModule {
        override fun convert(helper: QuickViewHolder, item: MallHome.ProductData) {
            helper.setImageWithGlide(R.id.iv, item.image)
                    .setText(R.id.tv_author, item.name)
                    .setText(R.id.tv_date, item.marketPrice)
        }
    }

    private val childRvAdapter by lazy {
        MainBottomNewProductAdapter()
    }

    override fun initView(helper: BaseViewHolder, item: MallHomeMultiple<*>, position: Int) {

        helper.getView<RecyclerView>(R.id.recyclerView).apply {
            layoutManager = GridLayoutManager(context, 2)
            addItemDecoration(object : Y_DividerItemDecoration(context) {
                override fun getDivider(itemPosition: Int): Y_Divider {
                    val data = childRvAdapter.data
                    val isNoLastItem = itemPosition != data.size - 1 || itemPosition != data.size - 2
                    return Y_DividerBuilder()
                            .setTopSideLine(isNoLastItem, Color.TRANSPARENT, 10f, 0f, 0f)
                            .setLeftSideLine(itemPosition % 2 != 0, Color.TRANSPARENT, 10f, 0f, 0f)
                            .create()
                }
            })
            loadMoreModule = childRvAdapter.loadMoreModule
            loadMoreModule.setOnLoadMoreListener {
                Log.e("加载更多", "-----")
                //            getAttachFragment()?.loadMoreNewProductList()
            }

            adapter = childRvAdapter
        }
    }

    override fun bindDataToViewHolder(helper: BaseViewHolder, data: MallHomeMultiple<*>, position: Int) {
        loadMoreModule.isEnableLoadMore = true
        childRvAdapter.setNewInstance(data.list?.map {
            it as MallHome.ProductData
        }?.toMutableList())
    }

    private fun getAttachFragment() = (context as? MainActivity)?.supportFragmentManager?.findFragmentById(R.id.fragment_container) as? MainFragment

    fun addMoreData(list: List<MallHome.ProductData>?) {
        loadMoreModule.isEnableLoadMore = true
        if (list.isNullOrEmpty()) {
            loadMoreModule.loadMoreEnd()
        } else {
            childRvAdapter.addData(list)
//            if (list.size < AppConstant.LOAD_PER_PAGE) {
//                childRvAdapter.loadMoreEnd()
//            } else {
//                childRvAdapter.loadMoreComplete()
//            }
            //这里就不进行加载更多的结束判断了
            loadMoreModule.loadMoreComplete()
        }
    }

    fun unableLoadMore() {
        loadMoreModule.isEnableLoadMore = false
    }

}