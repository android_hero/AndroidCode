package com.cherry.androidcode.main

import android.app.Activity
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import com.cherry.androidcode.R
import com.cherry.androidcode.activity.AppSettingActivity
import com.cherry.androidcode.base.LazyLoadFragment
import com.cherry.androidcode.helper.DrawableHelper
import com.cherry.androidcode.onresult.AvoidOnResult
import com.cherry.androidcode.util.dpToPx
import com.cherry.androidcode.util.setGone
import kotlinx.android.synthetic.main.layout_title_bar.*

/**
 * @author DongMS
 * @date 2019/7/3
 */
class MeFragment : LazyLoadFragment() {
    override val contentLayout = R.layout.fragment_me

    override fun initView(view: View) {
        super.initView(view)
        iv_back.setGone()
        tv_title.text = "我的"
        tv_right_menu.text = "设置"
        tv_right_menu.setDrawableWidthHeight(30f.dpToPx(), 30f.dpToPx(), false)
        tv_right_menu.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.ic_launcher, 0, 0, 0)
        DrawableHelper.normalDivider(toolbar)
        tv_right_menu.setOnClickListener {
            (this.activity as? Activity)?.let {
                AvoidOnResult(this).startForResult(it, AppSettingActivity::class.java).subscribe {
                    Log.e("----", "设置成功！")
                }
            }
        }
    }

    companion object {
        fun newInstance(): Fragment {
            return MeFragment()
        }
    }

}