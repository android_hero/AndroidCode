package com.cherry.androidcode.main

import com.chad.library.adapter.base.BaseProviderMultiAdapter
import com.cherry.androidcode.entity.MallHomeMultiple
import com.cherry.androidcode.main.provider.MainBottomNewProductProvider
import com.cherry.androidcode.main.provider.MainTopAdProvider
import com.cherry.androidcode.main.provider.MainTopClassProvider

/**
 * http://www.mocky.io/v2/5d2d72a62e00005700c57cfa
 * @author DongMS
 * @since 2019/7/16
 */
class MainRecyclerAdapter : BaseProviderMultiAdapter<MallHomeMultiple<*>>(null) {

    val mainBottomNewProductProvider: MainBottomNewProductProvider

    init {
        addItemProvider(MainTopAdProvider())
        addItemProvider(MainTopClassProvider())
        mainBottomNewProductProvider = MainBottomNewProductProvider()
        addItemProvider(mainBottomNewProductProvider)
    }

    override fun getItemType(data: List<MallHomeMultiple<*>>, position: Int): Int = data[position].multipleType

}