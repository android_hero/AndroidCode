package com.cherry.androidcode.main

import com.cherry.androidcode.mvp1.BasePresenter
import com.cherry.androidcode.mvp1.BaseView

/**
 * @author DongMS
 * @since 2019/7/11
 */
interface Fragment2Contract {

    interface View : BaseView {
        fun loadCategory(data: List<Fragment2.TabBean>)
    }

    interface Presenter : BasePresenter<View> {
        fun loadCategory()
    }

}