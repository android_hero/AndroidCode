# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
#------------------------------------默认的模板--------------------------------------#
########################混淆配置###############################
# 不使用大小写混合类名,混淆后的类名为小写

-dontusemixedcaseclassnames

# 混淆第三方库

-dontskipnonpubliclibraryclasses

# 混淆时记录日志,有助于排查错误

-verbose

# 代码混淆使用的算法.

-optimizations !code/simplification/arithmetic,!code/simplification/cast,!field/*,!class/merging/*

# 代码混淆压缩比,值在0-7之间,默认为5.

-optimizationpasses 5

# 优化时允许访问并修改有修饰符的类和类的成员

-allowaccessmodification

 #混淆时是否做预校验（可去掉加快混淆速度）
-dontpreverify

#-ignorewarning                   # 忽略警告
-dontoptimize                    # 优化不优化输入的类文件
-dontskipnonpubliclibraryclassmembers # 指定不去忽略非公共库的类成员

-printmapping proguardMapping.txt
-keepattributes *Annotation*,InnerClasses
-keepattributes Signature
-keepattributes SourceFile,LineNumberTable


##################### 不混淆 #####################
# 这些类不混淆

-keep public class * extends android.app.Activity

-keep public class * extends android.app.Application

-keep public class * extends android.app.Service

-keep public class * extends android.content.BroadcastReceiver

-keep public class * extends android.content.ContentProvider

-keep public class * extends android.app.backup.BackupAgent

-keep public class * extends android.preference.Preference

-keep public class * extends android.support.v4.app.Fragment

-keep public class * extends android.support.v4.app.DialogFragment

-keep public class * extends com.actionbarsherlock.app.SherlockListFragment

-keep public class * extends com.actionbarsherlock.app.SherlockFragment

-keep public class * extends com.actionbarsherlock.app.SherlockFragmentActivity

-keep public class * extends android.app.Fragment

-keep public class * extends android.app.backup.BackupAgentHelper

-keep public class com.android.vending.licensing.ILicensingService

# Native方法不混淆

-keepclasseswithmembernames class * {

native <methods>;

}

# 自定义组件不混淆

-keep public class * extends android.view.View {

public <init>(android.content.Context);

public <init>(android.content.Context, android.util.AttributeSet);

public <init>(android.content.Context, android.util.AttributeSet, int);

public void set*(...);

}

# 自定义控件类和类的成员不混淆(所有指定的类和类成员是要存在)

-keepclasseswithmembers class * {

public <init>(android.content.Context, android.util.AttributeSet);

}

# 同上

-keepclasseswithmembers class * {

public <init>(android.content.Context, android.util.AttributeSet, int);

}

# 自定义控件类不混淆

-keepclassmembers class * extends android.app.Activity {

public void *(android.view.View);

}

# 枚举类不被混淆

-keepclassmembers enum * {

public static **[] values();

public static ** valueOf(java.lang.String);

}

# android.os.Parcelable的子类不混淆

-keep class * implements android.os.Parcelable {

public static final android.os.Parcelable$Creator *;

}

# java.io.Serializable的子类不混淆

-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

# 资源类不混淆

-keepclassmembers class **.R$* {

public static <fields>;

}

#内部方法
-keepattributes EnclosingMethod

 # 保留第三方库android.support不被混淆

 -keep class android.support.** { *; }

 -keep interface android.support.** { *; }

 # WebView不被混淆

 -keepclassmembers class fqcn.of.javascript.interface.for.Webview {
    public *;
 }
 -keepclassmembers class * extends android.webkit.WebViewClient {
     public void *(android.webkit.WebView, java.lang.String, android.graphics.Bitmap);
     public boolean *(android.webkit.WebView, java.lang.String);
 }
 -keepclassmembers class * extends android.webkit.WebViewClient {
     public void *(android.webkit.WebView, jav.lang.String);
 }

#对WebView的简单说明下：经过实战检验,做腾讯QQ登录，如果引用他们提供的jar，若不加防止WebChromeClient混淆的代码，oauth认证无法回调，反编译基代码后可看到他们有用到WebChromeClient，加入此代码即可。

-keepclassmembernames class com.cgv.cn.movie.common.bean.** { *; }  #转换JSON的JavaBean，类成员名称保护，使其不被混淆

## java
-dontwarn javax.annotation.**
-dontwarn javax.inject.**

## databinding
-dontwarn android.databinding.**
-keep class android.databinding.** { *; }
-dontwarn com.android.databinding.**
-keep class com.android.databinding.** { *; }

 # 打包时忽略警告

 -dontwarn android.support.**

#------------------------------------默认的模板--------------------------------------#

-keep class com.cherry.androidcode.entity.** { *; } #实体类不参与混淆
-keep class com.cherry.androidcode.widget.** { *; } #自定义控件不参与混淆
-keep class com.cherry.androidcode.okgo.** { *; }

########################与js互相调用的类###############################

########################反射相关的类和方法###############################

########################不混淆的三方库###############################
#okhttp
-dontwarn okhttp3.**
-keep class okhttp3.**{*;}

#okio
-dontwarn okio.**
-keep class okio.**{*;}

#okgo
-keep class com.lzy.okgo.** { *; }

#gson
#-libraryjars libs/gson-2.2.4.jar   如果不混淆某个本地jar，可以使用-libraryjars libs/XXX
-keep class com.google.gson.** {*;}
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.stream.** { *; }
-keep class com.google.gson.examples.android.model.** { *; }
-keep class com.google.** {
    <fields>;
    <methods>;
}
-dontwarn com.google.gson.**


##httpmime/httpcore
-keep class org.apache.http.** {*;}
-keep class android.net.**{*;}
-keep class com.android.internal.http.multipart.**{*;}
-keep class org.apache.**{*;}

#BaseRecyclerViewAdapterHelper
#此资源库自带混淆规则，并且会自动导入，正常情况下无需手动导入。

#Glide
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}
# for DexGuard only
#-keepresourcexmlelements manifest/application/meta-data@value=GlideModule

# banner 的混淆代码
-keep class com.youth.banner.** {
    *;
 }

# Retrofit
-dontwarn retrofit2.**
-keep class retrofit2.** { *; }


# RxJava RxAndroid
-dontwarn sun.misc.**
-keepclassmembers class rx.internal.util.unsafe.*ArrayQueue*Field* {
    long producerIndex;
    long consumerIndex;
}
-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode producerNode;
}
-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueConsumerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode consumerNode;
}
-dontnote rx.internal.util.PlatformDependent

-keep class io.reactivex.Scheduler
-keep class io.reactivex.Observer
-keep class io.reactivex.disposables.Disposable
-keep class io.reactivex.Observable

# gsyvideoplayer
-keep class com.shuyu.gsyvideoplayer.video.** { *; }
-dontwarn com.shuyu.gsyvideoplayer.video.**
-keep class com.shuyu.gsyvideoplayer.video.base.** { *; }
-dontwarn com.shuyu.gsyvideoplayer.video.base.**
-keep class com.shuyu.gsyvideoplayer.utils.** { *; }
-dontwarn com.shuyu.gsyvideoplayer.utils.**
-keep class tv.danmaku.ijk.** { *; }
-dontwarn tv.danmaku.ijk.**

-keep public class * extends android.view.View{
    *** get*();
    void set*(***);
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

# BottomNavigationView
-keep public class com.google.android.material.bottomnavigation.BottomNavigationView { *; }
-keep public class com.google.android.material.bottomnavigation.BottomNavigationMenuView { *; }
-keep public class com.google.android.material.bottomnavigation.BottomNavigationPresenter { *; }
-keep public class com.google.android.material.bottomnavigation.BottomNavigationItemView { *; }


# 友盟推送
-dontwarn com.umeng.**
-dontwarn com.taobao.**
-dontwarn anet.channel.**
-dontwarn anetwork.channel.**
-dontwarn org.android.**
-dontwarn org.apache.thrift.**
-dontwarn com.xiaomi.**
-dontwarn com.huawei.**
-dontwarn com.meizu.**

-keepattributes *Annotation*

-keep class com.taobao.** {*;}
-keep class org.android.** {*;}
-keep class anet.channel.** {*;}
-keep class com.umeng.** {*;}
-keep class com.xiaomi.** {*;}
-keep class com.huawei.** {*;}
-keep class com.meizu.** {*;}
-keep class org.apache.thrift.** {*;}

-keep class com.alibaba.sdk.android.**{*;}
-keep class com.ut.**{*;}
-keep class com.ta.**{*;}

-keep public class **.R$*{
   public static final int *;
}

-keep class org.android.agoo.xiaomi.MiPushBroadcastReceiver {*;}
#可以防止一个误报的 warning 导致无法成功编译，如果编译使用的 Android 版本是 23。
-dontwarn com.xiaomi.push.**


